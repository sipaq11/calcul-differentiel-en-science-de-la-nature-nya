%%% Copyright (C) 2020 Julie Gendron, Simon Paquette et Jean-Nicolas Pépin
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\chapter{Applications}
%
%
\section{Extremums absolus}
%
%
\begin{bloc}{m}
Rappelons que pour trouver les \infobulle{extremums absolus}{defExtremumsAbsolus} d'une fonction continue sur un intervalle $[a, b]$, il suffit de trouver toutes les valeurs critiques de $f$ qui sont dans $]a, b[$, puis d'évaluer la fonction à chacune de ces valeurs critiques, en $x=a$ et en $x=b$ (\infobulle{théorème \ref{thmPositionValExt}}{thmPositionValExt}). La plus grande valeur de $f$ ainsi calculée est le maximum absolu et la plus petite, le minimum absolu. 

Pour trouver les extremums absolus lorsque la fonction n'est pas continue ou que l'intervalle n'est pas fermé, il faut déterminer si la fonction peut prendre des valeurs plus grandes ou plus petites que celles trouvées aux valeurs critiques et aux extrémités incluses de l'intervalle. Pour ce faire, il suffit d'étudier le comportement de la fonction autour des discontinuités et des extrémités non incluses de l'intervalle à l'aide de limites.
\end{bloc}%m
%
%
\begin{bloc}{b}

Pour trouver les \infobulle{extremums absolus}{defExtremumsAbsolus} d'une fonction $f$ : \pause
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]
\item Trouver le domaine de $f$ et identifier les discontinuités.
\item Trouver les valeurs critiques de $f$. 
\item Évaluer la fonction aux valeurs critiques ainsi qu'aux extrémités incluses de l'intervalle. 
\item Si la fonction comporte des discontinuités ou si l'intervalle n'est pas fermé, on étudie le comportement de la fonction aux extrémités non incluses de l'intervalle et aux discontinuités pour pouvoir conclure.
\end{liste}
\end{bloc}%b
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Déterminer les extremums absolus de la fonction $f(x) = e^x\sin(x)$ sur l'intervalle $[-\pi, \pi]$.}{

La fonction $f$ est continue sur $\mathds{R}$, car $\dom(f) = \mathds{R}$. Ainsi, le \infobulle{théorème \ref{thmValExetreme}}{thmValExetreme} nous confirme l'existence d'extremums absolus.   

Nous cherchons ensuite les valeurs critiques de $f$. 

Nous trouvons $f'(x) = e^x\paren*{\sin(x)+\cos(x)}$. 

Ainsi, $\dom(f') = \mathds{R}$, donc il n'y a pas de valeur de $x$ telle que $f'(x)~\nexists$.  

Ensuite, $f'(x) = 0 \Rightarrow e^x=0$ (ce qui est impossible) ou $\sin(x)+\cos(x)=0$.

Or, $\sin(x)+\cos(x)=0 \Rightarrow \sin(x)=-\cos(x) \Rightarrow x=-\dfrac{\pi}{4}$ ou $x=\dfrac{3\pi}{4}$.

Comme ces deux valeurs font partie du domaine de $f$, il s'agit de valeurs critiques.

Il faut évaluer la fonction à ces valeurs ainsi qu'aux bornes incluses de l'intervalle. 
\begin{alignat*}{3}
f(-\pi) &= 0 \hspace{1cm} &&  f\paren*{-\frac{\pi}{4}} &=-\dfrac{\sqrt{2}}{2e^{\frac{\pi}{4}}}\\[0.3cm]
f(\pi) &= 0 && f\paren*{\frac{3\pi}{4}} &=\dfrac{\sqrt{2}e^{\frac{3\pi}{4}}}{2} 
\end{alignat*}
On peut donc conclure que le maximum absolu de $f$ est $\dfrac{\sqrt{2}e^{\frac{3\pi}{4}}}{2}$ et que son minimum absolu est $-\dfrac{\sqrt{2}}{2e^{\frac{\pi}{4}}}$.
}
\end{exemple}

\begin{exemple}
\QS{Déterminer les extremums absolus de la fonction $f(x) =\tan(x^2)$ sur l'intervalle $\crochetso*{-\sqrt{\frac{\pi}{2}}, \sqrt{\frac{\pi}{2}}}$.}{

La fonction $f$ est continue sur $\crochetso*{-\sqrt{\frac{\pi}{2}}, \sqrt{\frac{\pi}{2}}}$. Par contre, comme l'intervalle n'est pas fermé, nous ne sommes pas assurés de l'existence d'extremums absolus.

Nous cherchons les valeurs critiques de $f$. 

Nous trouvons $f'(x) = 2x\sec^2(x^2)=\dfrac{2x}{\cos^2(x^2)}$.

Ainsi, $\dom(f') = \left]-\sqrt{\frac{\pi}{2}}, \sqrt{\frac{\pi}{2}}\right[$, donc il n'y a pas de valeur de $x$ telle que $f'(x)~\nexists$.  

Ensuite, $f'(x) = 0 \Rightarrow x=0$, donc c'est le seul endroit où on pourrait avoir un extremum absolu. De plus, $f(0)=0$.

Pour conclure, il faut étudier le comportement de la fonction aux bornes exclues du domaine.%
\[\dlim{x\to -\sqrt{\frac{\pi}{2}}^+}{\tan(x^2)}= \infty \text{~~~et~~~} \dlim{x\to \sqrt{\frac{\pi}{2}}^-}{\tan(x^2)}= \infty\]%
On peut donc conclure qu'il n'y a pas de maximum absolu, mais qu'il y a un minimum absolu de $0$ atteint en $x=0$.
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}
\begin{exercice}

\QR{Déterminer les extremums absolus de la fonction $f(x) = e^x\cos(x)$ sur $[-\pi, \pi[$.}{Le maximum absolu de $f$ est $\dfrac{\sqrt{2}e^{\frac{\pi}{4}}}{2}$ et il est atteint en $x=\frac{\pi}{4}$. 

La fonction n'a pas de minimum absolu sur cet intervalle. 
} 
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Trouver les abscisses des extremums absolus des fonctions suivantes sur les intervalles donnés.}
\debutSousQ
\QR{$f(x)=(2x+1)e^{-x^2}$ sur $]-2, 1[$}{Minimum absolu en $x={-1}$. Maximum absolu en $x=\frac{1}{2}$.}  
\QR{$f(x)=\ln(4-x^2)$ sur $]{-2}, 2[$}{Aucun minimum absolu. Maximum absolu en $x=0$.} 
\QR{$f(x)=200\cos(x)\sin(x)$  sur $\crochets*{0, \dfrac{\pi}{2}}$}{Minimum absolu en $x=0$ et en $x=\frac{\pi}{2}$. Maximum absolu en $x=\frac{\pi}{4}$.} 
\QR{$f(x)=\arctan(x)-\dfrac{x}{2}$  sur $\mathds{R}$}{Aucun extremum absolu.} 
\finSousQ
\end{secExercices}
\end{bloc}%eim
%
%
\section{Optimisation}
%
%
\begin{bloc}{m}
Dans ce chapitre, nous souhaitons résoudre des problèmes d'optimisation dans lesquels apparaissent des fonctions transcendantes. À la \autoref{chapOpti}, nous avons vu une \infobulle{démarche}{tabMethodeOpti} pour résoudre les problèmes d'optimisation. Revoyons avec un problème en contexte ces différentes étapes de résolution.

\begin{exemple} 
\QS{Déterminer la longueur minimale de l'hypoténuse du triangle rectangle déterminé par les points $A$, $B$ et $C$. Le point $P$ doit être sur l'hypoténuse.
\begin{center}
\includegraphics[scale=0.9]{Tikz/Images/chap_15_ex_opti_2_man.pdf}
\end{center}
}{

\textbf{Étape 1: Lire attentivement le problème. Relire au besoin.} 

Remarquons que la longueur minimale cherchée représente aussi la longueur maximale du segment de droite qu'il est possible de déplacer de la région $R_1$ à la région $R_2$.
\begin{center}
\includegraphics[scale=0.9]{Tikz/Images/chap_15_ex_opti_1_man.pdf}
\end{center}

\textbf{Étape 2: Nommer les variables. Utiliser un schéma, s'il y a lieu.}  

$L$: longueur totale de l'hypoténuse\\ 
$x$: distance entre les points $A$ et $P$ \\
$y$: distance entre les points $P$ et $B$  \\

\textbf{Étape 3: Établir une formule pour la quantité à optimiser.}  

Nous avons $L = x+y$. \dans{ordi}{\newpage}

\textbf{Étape 4: Au besoin, trouver une relation entre les variables afin que la fonction à optimiser ne dépende que d'une seule variable.}  

Nous pouvons écrire $x$ et $y$ en fonction de l'angle $\theta$, identifié sur la figure suivante. Nous ajoutons donc la variable $\theta$ à notre liste de variables de l'étape 2. 
\begin{center}
\includegraphics[scale=0.5]{Tikz/Images/chap_15_ex_opti_3_man.pdf}
\end{center}
Du triangle bleu, nous obtenons $\cos(\theta) = \dfrac{4}{x} \Rightarrow x = \dfrac{4}{\cos(\theta)} \Rightarrow x = 4\sec(\theta)$. 

Du triangle rouge, nous obtenons $\sin(\theta) = \dfrac{3}{y} \Rightarrow y = \dfrac{3}{\sin(\theta)} \Rightarrow y = 3\cosec(\theta)$. 

Ainsi, la fonction à optimiser est $L(\theta) = 4\sec(\theta) + 3\cosec(\theta)$. \\ 

\textbf{Étape 5: Déterminer le domaine de la fonction à optimiser, selon la variable choisie.}  

Puisque $\theta$ est un angle aigu (angle intérieur d'un triangle rectangle), nous trouvons $\dom(L) = \crochetso*{0, \frac{\pi}{2}}$. Remarquez qu'il n'est pas possible d'inclure les bornes, puisque la fonction sécante n'est pas définie en $\frac{\pi}{2}$ et que la fonction cosécante n'est pas définie en $0$.\\

\textbf{Étape 6: Utiliser le calcul différentiel pour trouver les extremums cherchés.}  

Nous cherchons les valeurs critiques de $L(\theta) = 4\sec(\theta) + 3\cosec(\theta)$. 

Nous avons $L'(\theta) = 4\sec(\theta)\tan(\theta) - 3\cosec(\theta)\cotan(\theta)$. 

Remarquons d'abord qu'il n'y a pas de valeur de $\theta$, dans le domaine de $L$, pour laquelle $L'(\theta)$ n'existe pas. 

Nous cherchons ensuite les valeurs de $\theta$ qui annulent la dérivée. 
\begin{alignat*}{3}
\Rightarrow \hspace*{1cm} && 4\sec(\theta)\tan(\theta) - 3\cosec(\theta)\cotan(\theta) &= 0 \\[0.3cm]
\Rightarrow \hspace*{1cm} && 4\sec(\theta)\tan(\theta) &= 3\cosec(\theta)\cotan(\theta) \\[0.3cm]
\Rightarrow \hspace*{1cm} && 4\dfrac{\sin(\theta)}{\cos^2(\theta)} &= 3\dfrac{\cos(\theta)}{\sin^2(\theta)} \\[0.3cm]
\Rightarrow \hspace*{1cm} && \tan^3(\theta) &= \dfrac{3}{4} \\[0.3cm]
\Rightarrow \hspace*{1cm} && \theta &= \arctan\paren*{\sqrt[3]{\dfrac{3}{4}}} \\[0.3cm]
\Rightarrow \hspace*{1cm} && \theta &\approx \num{0,7375} \ \imaCalcu{} 
\end{alignat*}
Comme cette valeur fait partie du domaine de $L$, il s'agit d'une valeur critique.

Nous calculons, à l'aide d'une calculatrice, $L(\num{0,7375}) \approx \num{9,8657}$.

De plus, $\dlim{\theta \to 0^+}{L(\theta)}=\infty$ et $\dlim{\theta \to \frac{\pi}{2}}{L(\theta)}=\infty$, alors cela confirme que le minimum est atteint en $\theta \approx \num{0,7375}$.\\ 

\textbf{Étape 7: Répondre à la question formulée dans le problème, en tenant compte du contexte. S'assurer que la réponse est plausible.} 

La plus petite hypoténuse du triangle rectangle de sommets $A$, $B$ et $C$ qui contient le point $P$ tel que décrit mesure environ \num{9,8657} mètres.
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{b}[Optimisation]
La \infobulle{marche à suivre}{tabMethodeOpti} pour résoudre les problèmes d'optimisation impliquant des fonctions transcendantes est la même que celle que nous avons vue dans la partie sur les polynômes et les fonctions algébriques.
\end{bloc}%b
%
%
\begin{bloc}{bn}
\begin{exercice}

\QR{Déterminer l'aire maximale du rectangle, situé dans le premier quadrant, inscrit sous la courbe $f(x) = e^{-x}$ et délimité par l'axe des abscisses et l'axe des ordonnées.}{Aire maximale = $\dfrac{1}{e}$ u$^2$, lorsque $x=1$.}
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{eim}
\begin{secExercices}

\QR{La marée pour une journée donnée peut être modélisée par la fonction $n(t)=\sin\paren*{\frac{t}{2}}+\sqrt{3}\cos\paren*{\frac{t}{2}}+2$ où $n$ représente le niveau de l'eau (en mètres) par rapport au niveau le plus bas et $t$ le temps écoulé à partir de minuit (en heures). Déterminer le moment au cours de cette journée où devraient avoir lieu les deux marées basses.}{Vers 7h20 et vers 19h54.}[12cm] 

\QR{La population d'une espèce $t$ années après son arrivée sur un territoire est donnée par la fonction $P(t)=\frac{\num{1200}}{1+3e^{-x+3}}$. Déterminer combien de temps après son arrivée la population de cette espèce augmentera à la vitesse maximale.}{On doit maximiser $P'(t)$. La population augmentera à la vitesse maximale $3-\ln\paren*{\frac{1}{3}}\approx \num{4,0986}$ ans après son arrivée.}[12cm] 

\QR{Déterminer le point $Q(x,y)$ de la courbe $f(x)=x^4\ln(x)$ tel que la pente de la droite joignant $Q$ au point $(0,0)$ soit minimale.}{La pente $\dfrac{y-0}{x-0}=\dfrac{x^4\ln(x)}{x}=x^3\ln(x)$ sera minimale en $\paren*{e^{-\frac{1}{3}}, \dfrac{-1}{3e^{\frac{4}{3}}}}$.}

\QR{On veut placer une caméra de surveillance sur un mur de 10 mètres de hauteur. Déterminer l'endroit où on doit la placer pour maximiser l'angle de vision $\theta$ de la caméra. \ \imaCalcu
\begin{center}
\includegraphics[width=\linewidth]{Tikz/Images/chap_15_exerc_opti_1_man.pdf}
\end{center}
}{$x$ : distance entre la caméra et le mur de \qty{5}{m} de haut. \newline $\theta(x) = \arctan\paren*{\frac{x}{5}} + \arctan\paren*{\frac{10-x}{2}}$ \newline $\dom(\theta) = [0, 10]$ \newline Il faut positionner la caméra à environ \qty{5,66}{m} du mur de \qty{5}{m} pour maximiser l'angle de vision de la caméra. \dans{e}{\columnbreak}}[12cm] 
\end{secExercices}
\end{bloc}%eim
%
%
\section{Taux liés}
%
%
\begin{bloc}{m}
Tout comme à la \autoref{secTauxLies}, nous cherchons maintenant à déterminer un taux de variation instantané à partir d'une équation qui lie des variables dépendantes d'une même variable indépendante.

\begin{exemple}
\QS{Supposons que $x$ et $y$ sont des fonctions de $t$ liées par l'équation $2x+e^{y^2} = y$. 

Sachant que $\displaystyle \frac{\dd x}{\dd t}=1$, évaluer $\displaystyle \frac{\dd y}{\dd t}$ lorsque $y=0$. 
}{

Il faut d'abord dériver les deux côtés de l'équation par rapport à la variable $t$.
\begin{align*}
2\frac{\dd x}{\dd t} + 2ye^{y^2}\frac{\dd y}{\dd t} &= \frac{\dd y}{\dd t}
\end{align*}
En remplaçant $y$ par 0 et $\displaystyle \frac{\dd x}{\dd t}$ par 1, nous obtenons
\begin{alignat*}{3}
&& 2\cdot 1 + 0 &= \left.\frac{\dd y}{\dd t}\right|_{y=0} \\[0.3cm]
\Rightarrow \hspace*{1cm} && \left.\frac{\dd y}{\dd t}\right|_{y=0} &= 2 \text{ .}
\end{alignat*}
}
\end{exemple}

La \infobulle{démarche}{tabMethodeTauxLies} pour la résolution de problèmes de taux liés en contexte est la même que celle vue dans la \autoref{secTauxLies}.

\begin{exemple}
\QS{Une femme court en ligne droite à une vitesse de \qty{4}{m/s}. Sur le sol, à 20 mètres du chemin sur lequel la femme se déplace, se trouve un projecteur qui éclaire et suit la femme. Déterminer la vitesse à laquelle le projecteur tourne lorsque la coureuse se trouve à 15 mètres du point du chemin le plus près du projecteur.}{

\textbf{Étape 1: Lire attentivement le problème. Relire au besoin.} \\

\textbf{Étape 2: Nommer les variables et déterminer tous les taux connus, ainsi que le taux cherché.} 

$x$: distance entre la coureuse et le point du chemin le plus près du projecteur (m)\\ 
$\theta$: angle que forment le faisceau du projecteur et la perpendiculaire au chemin (rad)\\
$t$: temps écoulé (s)

Nous cherchons $\left. \displaystyle \frac{\dd \theta}{\dd t}\right|_{x=15}$ et nous savons que $\displaystyle \frac{\dd x}{\dd t} = 4$. \\ 

\textbf{Étape 3: Tracer un schéma (pour les problèmes de nature géométrique).} 
\begin{center}
\includegraphics[scale=0.7]{Tikz/Images/chap_15_ex_TauxLie_1_man.pdf}
\end{center}

\textbf{Étape 4: Trouver le(s) lien(s) entre les variables.} 

Nous avons $\tan(\theta)=\dfrac{x}{20}  \Rightarrow x = 20\tan(\theta)$.  

\textbf{Étape 5:  Utiliser le calcul différentiel pour obtenir les taux de variation décrits à l'étape 2.}  

Nous dérivons de chaque côté par rapport à la variable $t$.
\begin{align*}
 & \frac{\dd x}{\dd t} = \ 20\sec^2(\theta) \, \frac{\dd \theta}{\dd t} \\[0.4cm]
\Rightarrow \hspace*{1cm}& \frac{\dd \theta}{\dd t}  = \ \dfrac{1}{20}\cos^2(\theta) \, \frac{\dd x}{\dd t} 
\end{align*}
Nous voulons évaluer cette dérivée quand $x=15$. Lorsqu'on complète le triangle rectangle, nous trouvons que l'hypoténuse vaut $25$. Ainsi, $\cos(\theta) = \frac{20}{25} = \frac{4}{5}$. Remarquez qu'il n'est pas nécessaire de trouver $\theta$, le cosinus nous suffit.
\begin{align*}
\left. \frac{\dd \theta}{\dd t}\right|_{x=15} =& \ \dfrac{1}{20}\paren*{\dfrac{4}{5}}^2\cdot 4 \\[0.4cm]
=& \ \dfrac{16}{125} 
\end{align*}

\textbf{Étape 6:  Conclure et interpréter le résultat en tenant compte du contexte.}  

Le projecteur tourne à une vitesse de \qty[parse-numbers=false]{\frac{16}{125}}{rad/s} lorsque la coureuse se trouve à 15 mètres du point du chemin le plus près du projecteur.
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{b}[Taux liés]

La \infobulle{marche à suivre}{tabMethodeTauxLies} pour la résolution de problèmes de taux liés en contexte impliquant des fonctions transcendantes est la même que celle vue dans la partie sur les polynômes et les fonctions algébriques.
\end{bloc}%b
%
%
\begin{bloc}{bn}
\begin{exercice}

\QR{Deux côtés d'un triangle mesurent respectivement quatre mètres et cinq mètres, et l'angle qu'ils forment augmente au rythme de \qty{0,06}{rad/s}. Trouver la vitesse à laquelle l'aire du triangle augmente quand l'angle entre ces deux côtés mesure $\frac{\pi}{3}$ radian.}{L'aire augmente à une vitesse de \qty{0,3}{m^2/s}.}
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{eim}
\begin{secExercices}
\QR{Une échelle de 6~m de longueur est appuyée contre un mur. Si le pied de l'échelle s'éloigne du mur à la vitesse de \qty{0,5}{m/s}, déterminer la vitesse à laquelle l'angle $\theta$ formé par l'échelle et le mur varie lorsque le pied de l'échelle est à \qty{3}{m} du mur.}{L'angle $\theta$ augmente au rythme de \qty[parse-numbers=false]{\dfrac{\sqrt{3}}{18}}{~rad/s}.}

\QR{Une caméra est posée au sol, à \qty{2}{km} du lieu où s'élève verticalement un hélicoptère à la vitesse de \qty{90}{km/h}. Déterminer la vitesse à laquelle l'angle d'élévation $\theta$ doit varier lorsque l'hélicoptère atteint une altitude de \qty{2}{km} afin que la caméra reste pointée sur l'hélicoptère.}{L'angle d'élévation $\theta$ doit augmenter au rythme de \qty{22,5}{rad/h}.}

\QR{Un avion grimpe à un angle de 30$\degres$ au-dessus de l'horizontale. Déterminer le rythme auquel l'appareil gagne de l'altitude s'il vole à \qty{800}{km/h}.}{L'appareil gagne de l'altitude à \qty{400}{km/h}.}

\QR{Soit l'équation suivante liant les variables $x$ et $y$ en fonction de $t$.%
\[-\arctan(y)+\dfrac{\pi x}{5-x}+ey=\ln(x)+e^{x^2}\]%
Déterminer $\dfrac{\textrm{d}y}{\textrm{d}t}$ sachant que $\dfrac{\textrm{d}x}{\textrm{d}t}=16e-8$ lorsque $(x, y)=(1, 1)$.}{On a $\dfrac{\textrm{d}y}{\textrm{d}t}=16+32e-5\pi$ lorsque $(x, y)=(1, 1)$.}
\end{secExercices}
\end{bloc}%eim
%
%
\section{Approximation linéaire}
%
%
\begin{bloc}{m}
Nous utiliserons la droite tangente ou la \infobulle{différentielle}{defDiff}, comme nous l'avons fait à la \autoref{secApproxLin}, pour approximer l'image de fonctions transcendantes.

\begin{exemple}
\QS{Approximer la valeur de $e^{0,1}$.}{

Si nous utilisons la \textbf{droite tangente} pour faire l'approximation : 

Soit la fonction $f(x)=e^x$. Nous savons que $e^0=1$. Nous pouvons donc utiliser la tangente à la courbe en $x=0$ pour approximer la valeur de $f(0,1)$.

Nous trouvons la dérivée pour obtenir la pente de la tangente. 

Nous trouvons $f'(x) = e^x$. Donc, la pente de la tangente en $x=0$ est $f'(0) = 1$. 

À l'aide du point de tangence $(0, 1)$, nous pouvons trouver l'ordonnée à l'origine de la droite tangente en $x=0$. 

$1 = 1\cdot 0 + b \Rightarrow b=1$. 

Nous avons donc $g(x) = x + 1$ comme équation de droite tangente. Ainsi, nous pouvons utiliser cette équation pour faire l'approximation.%
\[f(0,1) \approx g(0,1) = 0,1+1 = 1,1.\]%

Si nous utilisons la \textbf{différentielle} pour faire l'approximation : 

Soit la fonction $y=f(x) = e^x$. Nous savons que $e^0=1$ alors nous utilisons $x=0$ et $\dd x=0,1$.

Nous trouvons la dérivée pour obtenir la différentielle de $y$. 

Nous trouvons $f'(x) = e^x$. Donc, $\dd y = e^x \, \dd x$. 

Nous pouvons maintenant faire l'approximation.% 
\[f(0,1)=f(0+0,1) \approx f(0) + \dd y = f(0)+f'(0)\cdot 0,1=1+1\cdot 0,1 = 1,1.\]
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{b}[Approximation linéaire]
Pour faire une approximation linéaire, nous pouvons utiliser : 
\begin{liste}[beaOpt=<+- |alert@+>, itSep=0.3cm]
\item une droite tangente; 
\item la \infobulle{différentielle}{defDiff}.
\end{liste}
\end{bloc}%b
%
%
\begin{bloc}{bn}
\begin{exercice}

\QR{Approximer la valeur de $\sin(0,02)$ à l'aide du calcul différentiel.}{$\sin(0,02) \approx 0,02$}
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{b}{}
\begin{center}
{\Large Fin} 
\end{center}
\end{bloc}%b
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Faire une approximation linéaire des valeurs suivantes en utilisant chacune des méthodes étudiées.}
\debutSousQ
\QR{$\sin\paren*{\dfrac{\pi}{32}}$}{$\sin\paren*{\dfrac{\pi}{32}} \approx \frac{\pi}{32}$}  
\QR{$\ln(1,01)$}{$\ln(1,01) \approx 0,01$} 
\QR{$e^{{-0,02}}$}{$e^{{-0,02}} \approx 0,98$} 
\QR{$\arctan(1,1)$}{$\arctan(1,1) \approx \dfrac{\pi}{4}+\dfrac{1}{20}$} 
\finSousQ
\end{secExercices}
\end{bloc}%eim


