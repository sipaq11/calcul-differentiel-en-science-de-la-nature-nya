
%<*ici> 

\chapter{Limites et continuité}

\begin{manuel}
Dans la première partie nous avons étudié le concept de limite à l'aide de plusieurs graphiques. Parmi ceux-ci, certains représentaient des fonctions algébriques. Dans ce chapitre, nous allons nous concentrer sur le calcul de limites de façon algébrique. 
\end{manuel}


\section{Limites}

\begin{manuel}
\subsection{Formes déterminées et indéterminées}

\begin{ex}
{Évaluer $\dlim{x \to 2}{\frac{x-3}{x+5}}$. \\
}{\ \\

D'abord, le domaine de cette fonction est $\mathds{R}\setminus \{{-5}\}$ et comme toute fonction non définie par parties est continue sur son domaine, cette fonction est continue en $x=2$. Nous pouvons donc automatiquement utiliser le fait que
$\dlim{x \to a}{f(x)} = f(a)$, ce qui découle du point 3 de la \tip{définition de la continuité}{defContinuiteEnUnPoint}.

$$\dlim{x \to 2}{\frac{x-3}{x+5}} = \frac{2-3}{2+5} = -\frac{1}{7}.$$
}
\end{ex}
\end{manuel}


\begin{bframe}{Notation et signification}
\begin{center}
Que signifie $\dlim{x \to a}{f(x)}=b$ ?
\end{center}
\end{bframe}

\begin{bnframe}{Exemple \exe}
Évaluer $\dlim{x \to 4}{\frac{x-2}{\sqrt{x+5}}}$.  \ndc{\vspace*{3cm}}

\end{bnframe}


\begin{bframe}{Continuité}
\rappel Nous avons vu au \tip{théorème \ref{thmContinuite}}{thmContinuite} que toutes les fonctions non définies par parties sont continues sur leur domaine.  \\
\bea{\vspace*{0.5cm}}
De plus, la \tip{définition de la continuité en un point}{defContinuiteEnUnPoint} est toujours la même.
\end{bframe}



\begin{bmnframe}{Limites}
\bea{\justifying}
Les limites qui nous causeront plus de problèmes sont donc celles où $x$ tend vers $a$ et où $f$ possède une discontinuité en $x=a$. Ainsi que celles où $x$ tend vers $\pm \infty$.\\
\end{bmnframe}

\begin{manuel}
\rappel Les formes $\infty + k$, où $k \in \mathds{R}$, $\infty + \infty$, $\pm \infty \cdot \pm \infty$ et $\frac{k}{\pm \infty}$ sont des formes déterminées que nous avons déjà rencontrées. On se rappelle que dans le cas d'une forme déterminée, on peut évaluer la limite sans manipulations algébriques.

\ordi{\newpage}

\begin{ex}
{Évaluer $\dlim{x \to \infty}{\big(\sqrt{x-3}+7\big)}$. \\
}{
\begin{align*}
& \ \dlim{x \to \infty}{\big(\sqrt{x-3}+7\big)}   \hspace{2cm} & \text{F.D. } \infty + k \\[0.4cm]
=& \ \infty 
\end{align*}
}
\end{ex}  

\rem D'autres formes déterminées apparaîtront avec les fonctions algébriques. C'est le cas, entre autres, des formes $0/k$, $0/\infty$ et $\infty/0$.

\mob{\newpage}

\begin{ex}
{Évaluer $\dlim{x \to \infty}{\frac{x}{\frac{1}{x}}}$. \\
}{
\begin{align*}
& \ \dlim{x \to \infty}{\frac{x}{\frac{1}{x}}}   \hspace{2cm} & \text{F.D. } \dfrac{\infty}{0} \\[0.4cm]
=& \ \dlim{x \to \infty}{\left(x \cdot \frac{x}{1}\right)}   \\[0.4cm]
=& \ \dlim{x \to \infty}{x^2}   \\[0.4cm]
=& \ \infty 
\end{align*}
}
\end{ex} 


\ordi{\newpage}

Voici une situation que nous rencontrerons souvent avec les fonctions rationnelles.
\end{manuel}



\begin{bnframe}{Exemple \exe}
Évaluer les limites suivantes. \bea{\\[0.6cm]}

\ndc{\be} \bea{\be[<+- |alert@+>]  \setlength\itemsep{0.6cm}}
\item $\dlim{x \to \infty}{\frac{\frac{1}{x}}{x}}$  \ndc{\vfill}

\item $\dlim{x \to \infty}{\frac{{-2}}{\sqrt{x+5}}}$  \ndc{\vfill \newpage}
\ee
\end{bnframe}


\begin{bmntframe}{Forme $\frac{k}{0}$} 
\tool{g}{tabFormek0}{
\man{\begin{table}[!h] 
\caption{Forme $\frac{k}{0}$}  
\label{tabFormek0}} 
\mnt{\begin{mdframed}[style=encadre] 
\begin{center}{\bf Forme $\dfrac{k}{0}$} \end{center}}
\ \\
$\dlim{x \to a}{\frac{f(x)}{g(x)}}$ a une forme $\dfrac{k}{0}$ si $\dlim{x \to a}{f(x)}=k\neq 0$ et $\dlim{x \to a}{g(x)}=0$. \mnt{\\} \bea{\\[0.5cm] \pause}

Dans ce cas, la limite pourrait ne pas exister. Il faut évaluer la limite à gauche et la limite à droite pour conclure. 
\mnt{\end{mdframed}}
\man{\end{table}}
}%tool

\end{bmntframe}



\begin{manuel}
\begin{ex}
{Évaluer $\dlim{x \to {-2}}{\frac{x^2+x+1}{x+2}}$. \\
}{
$$ \dlim{x \to {-2}}{\frac{x^2+x+1}{x+2}}  \hspace{5.3cm}  \text{Forme } \dfrac{k}{0} $$

On évalue donc la limite à gauche et la limite à droite.
\begin{align*}
& \ \dlim{x \to {-2}^-}{\frac{x^2+x+1}{x+2}}  \hspace{2cm} &  \text{F.D. } \dfrac{3}{0^-} \\[0.4cm]
=& \ -\infty 
\end{align*}
car une constante positive divisée par un très petit nombre négatif donne un très grand nombre négatif.
\begin{align*}
& \ \dlim{x \to {-2}^+}{\frac{x^2+x+1}{x+2}}  \hspace{2cm} &  \text{F.D. } \dfrac{3}{0^+} \\[0.4cm]
=& \ \infty 
\end{align*}
car une constante positive divisée par un très petit nombre positif donne un très grand nombre positif.\\

Ainsi, $\dlim{x \to {-2}}{\frac{x^2+x+1}{x+2}}$ $\nexists$, mais nous avons une idée de la façon dont se comporte la fonction autour de $x={-2}$.
}
\end{ex} 

\ordi{\newpage}

\begin{ex}
{Évaluer $\dlim{x \to 5}{\frac{x}{x^2-10x+25}}$. \\
}{
$$ \dlim{x \to  5}{\frac{x}{x^2-10x+25}}  \hspace{4.2cm}  \text{Forme } \dfrac{k}{0}$$

\ \\
On évalue donc la limite à gauche et la limite à droite.
\begin{alignat*}{3}
& \ \dlim{x \to  5^-}{\frac{x}{x^2-10x+25}} && \hspace{4cm}   \text{Forme } && \dfrac{5}{0^?} \\[0.4cm]
=& \ \dlim{x \to  5^-}{\frac{x}{(x-5)^2}}  && \hspace{4cm}  \text{F.D. } && \dfrac{5}{0^+} \\[0.4cm]
=& \ \infty 
\end{alignat*}
Remarquons que dans l'évaluation de cette limite, il était difficile de savoir si le dénominateur allait tendre vers $0^+$ ou $0^-$. Le fait de factoriser le dénominateur élimine cette difficulté. De même,
\begin{align*}
& \ \dlim{x \to  5^+}{\frac{x}{(x-5)^2}}  \hspace{2.1cm} &  \text{F.D. } \dfrac{5}{0^+} \\[0.4cm]
=& \ \infty 
\end{align*}

Ainsi, $\dlim{x \to  5}{\frac{x}{(x-5)^2}}= \infty$. Attention, cette limite n'existe tout de même pas.
}
\end{ex}  

Voyons maintenant différentes formes indéterminées ainsi que la manipulation algébrique permettant de lever l'indétermination dans chacun des cas.

\newpage
\end{manuel}


\begin{bnframe}{Exemple \exe}
Évaluer les limites suivantes. \bea{\\[0.6cm]}

\ndc{\be} \bea{\be[<+- |alert@+>]  \setlength\itemsep{0.6cm}}
\item $\dlim{x \to 2}{\frac{3}{2-x}}$  \ndc{\vfill}

\item $\dlim{x \to 2}{\frac{3}{\sqrt{2-x}}}$  \ndc{\vfill \newpage}
\ee
\end{bnframe}


\begin{bmntframe}{Forme $\frac{0}{0}$} 
\tool{g}{tabForme0sur0}{
\man{
\begin{table}[!h] 
\caption{Forme indéterminée $\frac{0}{0}$}  
\label{tabForme0sur0}} 
\mnt{\begin{mdframed}[style=encadre] 
\begin{center}{\bf Forme indéterminée $\dfrac{0}{0}$} \end{center}}
\ \\
Voici quelques astuces permettant de lever l'indétermination dans le cas d'une forme $\frac{0}{0}$. \mnt{\\} \bea{\\[0.3cm]}

\begin{enumerate}  \bea{\setlength\itemsep{0.3cm}}
\item Dans le cas d'un quotient de polynômes: factoriser les polynômes et simplifier les facteurs communs.
\item Dans le cas où le numérateur ou le dénominateur contient une racine: multiplier le numérateur et le dénominateur par le conjugué du terme contenant la racine et simplifier.
\item Dans le cas où le numérateur ou le dénominateur est constitué de deux fractions: effectuer l'opération et simplifier. 
\end{enumerate}
\mnt{\end{mdframed}}
\man{\end{table}}
}%tool
\end{bmntframe}


\begin{manuel}
\begin{ex}
{Évaluer $\dlim{x \to {-3}}{\frac{x^2+3x}{x^2-9}}$. \\
}{
\begin{align*}
& \ \dlim{x \to {-3}}{\frac{x^2+3x}{x^2-9}}   \hspace{2cm} & \text{F.I. }\dfrac{0}{0} \\[0.4cm]
=& \ \dlim{x \to {-3}}{\frac{x(x+3)}{(x-3)(x+3)}}   \\[0.4cm]
=& \ \dlim{x \to {-3}}{\frac{x}{x-3}}   \\[0.4cm]
=& \ \frac{1}{2}
\end{align*}
}
\end{ex} 


\ordi{\newpage}


\begin{ex}
{Évaluer $\dlim{x \to 1}{\frac{x^3-x^2+x-1}{x^2-x}}$. \\
}{\ \\

Dans cet exemple de forme indéterminée $\frac{0}{0}$, on constate que la valeur $x=1$ est une racine à la fois du numérateur et du dénominateur puisqu'elle annule chacune des expressions. Ainsi, nous savons que $(x-1)$ est un facteur du numérateur et un facteur du dénominateur. On peut donc utiliser la division polynômiale (voir \ref{divisionPolynomes}) pour factoriser le numérateur et la mise en évidence pour factoriser le dénominateur.
\begin{align*}
& \ \dlim{x \to 1}{\frac{x^3-x^2+x-1}{x^2-x}}   \hspace{2cm} & \text{F.I. }\dfrac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 1}{\frac{(x-1)(x^2+1)}{x(x-1)}}   \\[0.4cm]
=& \ \dlim{x \to 1}{\frac{x^2+1}{x}} \\[0.4cm]
=& \ 2
\end{align*}
}
\end{ex}
 
 
\ordi{\newpage}

 
\begin{ex}
{Évaluer $\dlim{x \to 2}{\frac{1-\sqrt{x-1}}{x-2}}$. \\
}{
\begin{align*}
& \ \dlim{x \to 2}{\frac{1-\sqrt{x-1}}{x-2}}  \hspace{2cm} & \text{F.I. }\dfrac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 2}{\left(\frac{1-\sqrt{x-1}}{x-2}\cdot \frac{1+\sqrt{x-1}}{1+\sqrt{x-1}}\right)} \\[0.4cm]
=& \ \dlim{x \to 2}{\frac{1-(x-1)}{(x-2)(1+\sqrt{x-1})}}  \\[0.4cm]
=& \ \dlim{x \to 2}{\frac{-x+2}{(x-2)(1+\sqrt{x-1})}}  \\[0.4cm]
=& \ \dlim{x \to 2}{\frac{-(x-2)}{(x-2)(1+\sqrt{x-1})}}  \\[0.4cm]
=& \ \dlim{x \to 2}{\frac{-1}{(1+\sqrt{x-1})}}  \\[0.4cm]
=& \ -\frac{1}{2}
\end{align*}
Remarquons d'abord que pour lever l'indétermination de la forme $\frac{0}{0}$, il faut arriver à faire disparaître le terme qui cause problème, dans ce cas-ci $(x-2)$. Il faut donc garder cet objectif en tête. De plus, pour y arriver, nous avons ici dû effectuer une mise en évidence suite à l'utilisation de l'astuce.
}
\end{ex}


\ordi{\newpage}


\begin{ex}
{Évaluer $\dlim{x \to 3}{\displaystyle \frac{\frac{x+2}{6-x}-\frac{5}{x}}{x-3}}$. \\
}{
\begin{align*}
& \ \dlim{x \to 3}{\displaystyle \frac{\frac{x+2}{6-x}-\frac{5}{x}}{x-3}}   \hspace{2cm} & \text{F.I. }\dfrac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 3}{\left(\frac{x(x+2)-5(6-x)}{x(6-x)}\cdot \frac{1}{x-3}\right)}  \\[0.4cm]
=& \ \dlim{x \to 3}{\frac{x^2+7x-30}{x(6-x)(x-3)}} \hspace{2cm} & \text{F.I. }\dfrac{0}{0}  \\[0.4cm]
=& \ \dlim{x \to 3}{\frac{(x-3)(x+10)}{x(6-x)(x-3)}}   \\[0.4cm]
=& \ \dlim{x \to 3}{\frac{(x+10)}{x(6-x)}}   \\[0.4cm]
=& \ \frac{13}{9}
\end{align*}
}
\end{ex} 

Il peut arriver qu'on retrouve une forme indéterminée à l'intérieur d'une évaluation de limite. Il peut donc s'avérer utile d'utiliser les \tip{propriétés des limites}{thmLimiteProp}.

\ordi{\newpage}


\begin{ex}
{Évaluer  $\dlim{x \to 4}{\sqrt{\displaystyle \frac{x^2-4x}{x^2-3x-4}}}$\\
}{  
\begin{alignat*}{4}
& \ \dlim{x \to  4}{\sqrt{\displaystyle \frac{x^2-4x}{x^2-3x-4}}}   &&                                             &&   \text{Forme } \sqrt{\dfrac{0}{0}} \\[0.3cm]
=& \ \sqrt{ \dlim{x \to  4}{\displaystyle \frac{x^2-4x}{x^2-3x-4}} } \hspace{2cm} && \text{\tip{propriété 4 des limites}{thmLimiteProp}} \hspace{2cm}&& \text{F.I. } \dfrac{0}{0}\\[0.3cm]
=& \ \sqrt{ \dlim{x \to  4}{\displaystyle \frac{x(x-4)}{(x-4)(x+1)}} } \\[0.3cm]
=& \ \sqrt{ \dlim{x \to  4}{\displaystyle \frac{x}{(x+1)}} } \\[0.3cm]
=& \ \sqrt{\frac{4}{5}}  \\[0.3cm]
=& \ \frac{2}{\sqrt{5}}
\end{alignat*}
Notons qu'ici nous avons pu utiliser la propriété des limites puisque la fonction racine carrée est continue en $x=\frac{4}{5}$.
}
\end{ex}


\ordi{\newpage}


\begin{ex}
{Évaluer  $\dlim{x \to 3}{f(x)}$ où $f(x) =
\begin{fctpp}
$\displaystyle \frac{x^2-9}{x^2-6x+9}$ & si $x< 3$ \\
$10$ & si $x= 3$ \\
$\displaystyle \frac{3x-4}{3-x}$ & si $x > 3$ \\
\end{fctpp}$
}{\ \\

Comme il s'agit d'une fonction définie par parties et qu'on cherche la limite à la valeur charnière, il faut automatiquement évaluer les limites à gauche et à droite.
\begin{alignat*}{4}
 \dlim{x \to 3^-}{f(x)} =& \ \dlim{x \to 3^-}{\frac{x^2-9}{x^2-6x+9}} \hspace{2cm} && \text{F.I. } && \dfrac{0}{0}  \\[0.4cm]
=& \ \dlim{x \to 3^-}{\frac{(x-3)(x+3)}{(x-3)^2}}  \\[0.4cm]
=& \ \dlim{x \to 3^-}{\frac{(x+3)}{(x-3)}}   \hspace{2cm} && \text{F.D. } && \dfrac{6}{0^-}  \\[0.4cm]
=& \ -\infty \\[1cm]
\dlim{x \to 3^+}{f(x)} =& \ \dlim{x \to 3^+}{\frac{3x-4}{3-x}} \hspace{2cm} && \text{F.D. } && \dfrac{5}{0^-}  \\[0.4cm]
=& \ -\infty
\end{alignat*}
Ainsi, $\dlim{x \to 3}{f(x)}=-\infty$.
}
\end{ex}
\end{manuel}


\begin{bnframe}{Exemple \exe}
Évaluer les limites suivantes. \bea{\\[0.6cm]}

\ndc{\be} \bea{\be[<+- |alert@+>]  \setlength\itemsep{0.6cm}}
\item $\dlim{x \to 1}{\sqrt{\frac{x-1}{x^2-1}}}$  \ndc{\vfill}

\item $\dlim{x \to 5}{\frac{3-\sqrt{4+x}}{5-x}}$  \ndc{\vfill \newpage}
\ee
\end{bnframe}


\begin{bmntframe}{Forme indéterminée $\frac{\infty}{\infty}$} 
\tool{g}{tabFormeinfsurinf}{
\man{
\begin{table}[!h] 
\caption{Forme indéterminée $\frac{\infty}{\infty}$}  
\label{tabFormeinfsurinf}} 
\mnt{\begin{mdframed}[style=encadre] 
\begin{center}{\bf Forme indéterminée $\dfrac{\infty}{\infty}$}  \end{center}}
\ \\
Voici une astuce qui permet de lever l'indétermination dans le cas d'une forme $\frac{\infty}{\infty}$. \mnt{\\} \bea{\\[0.3cm]}
Pour lever cette indétermination, on peut mettre en évidence le terme de plus haut degré au numérateur et au dénominateur et simplifier l'expression.
\mnt{\end{mdframed}}
\man{\end{table}}
}%tool
\end{bmntframe}



\begin{manuel}
\newpage

\begin{ex}
{Évaluer $\dlim{x \to \infty}{\frac{3x^6+2x+1}{5x^6+4x^2}}$. \\
}{
\begin{alignat*}{4}
& \ \dlim{x \to \infty}{\frac{3x^6+2x+1}{5x^6+4x^2}}  \hspace{4cm} && && \text{F.I. } &&\dfrac{\infty}{\infty} \\[0.4cm]
=& \ \dlim{x \to\infty}{\displaystyle \frac{x^6(3+\displaystyle \frac{2}{x^5}+\frac{1}{x^6})}{x^6(5+\displaystyle \frac{4}{x^4})}}   \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3+\displaystyle \frac{2}{x^5}+\frac{1}{x^6}}{5+\displaystyle \frac{4}{x^4}}}   && \text{plusieurs } && \text{F.D. } && \dfrac{k}{\infty}  \\[0.4cm]
=& \ \frac{3+0+0}{5+0} \\[0.4cm]
=& \ \frac{3}{5}
\end{alignat*}
Remarquons qu'ici il n'est pas utile de mettre en évidence les coefficients qui multiplient les termes de plus haut degré.
}
\end{ex}

L'exemple précédent illustre bien que, dans le cas d'un quotient de polynômes, seulement les termes dominants déterminent le résultat d'une limite à l'infini. %%%Pour la suite des choses, il sera donc possible d'utiliser le résultat suivant.



%%%\begin{bmntframe}{Limite à l'infini d'un quotient de polynômes}
%%%\begin{thm}[Limite à l'infini d'un quotient de polynômes]{thmLimiteinfinirationnelle}
%%%Soit $p(x)=a_nx^n+a_{n-1}x^{n-1}+...+a_1x+a_0$ et \\ $q(x)=b_mx^m+b_{m-1}x^{m-1}+...+b_1x+b_0$, où $a_n$ et $b_m\neq 0$, deux polynômes. Alors on a
%%%$$\dlim{x \to \pm \infty}{\frac{p(x)}{q(x)}}=\dlim{x \to \pm \infty}{\frac{a_nx^n}{b_mx^m}}$$
%%%\end{thm}
%%%\end{bmntframe}


\ordi{\newpage
\vspace*{-0.3cm}}

\begin{ex}
{Évaluer $\dlim{x \to -\infty}{\frac{4x^6-16x^2+7}{1-x^4}}$. \\
}{
\begin{alignat*}{3}
& \ \dlim{x \to -\infty}{\frac{4x^6-16x^2+7}{1-x^4}} \hspace{4cm} && \text{F.I. }\dfrac{\infty}{\infty} \\[0.3cm]
=& \ \dlim{x \to -\infty}{\dfrac{4x^6\left(1-\frac{4}{x^4}+\frac{7}{4x^6}\right)}{-x^4\left(-\frac{1}{x^4}+1 \right)}} \hspace{2cm} \\[0.3cm]
=& \ \dlim{x \to -\infty}{\dfrac{-4x^2\left(1-\frac{4}{x^4}+\frac{7}{4x^6}\right)}{\left(-\frac{1}{x^4}+1 \right)}} \hspace{2cm} \\[0.3cm]
=& \ -\infty
\end{alignat*}

Notons qu'ici la forme indéterminée était plutôt de la forme $\dfrac{\infty - \infty}{-\infty}$. Cette astuce permet de lever toutes les indéterminations en même temps.
} 
\end{ex}

\begin{ex}
{La concentration $C$ d'un médicament dans le sang $t$ heures après son injection est donnée par la fonction $C(t)=\displaystyle \frac{0,3t}{t^2+4}$. Évaluer $\dlim{t \to \infty}{C(t)}$ et interpréter le résultat selon le contexte.\\
}{
\begin{alignat*}{3}
\dlim{t \to \infty}{C(t)} =& \ \dlim{x \to \infty}{\frac{0,3t}{t^2+4}} \hspace{4cm} && \text{F.I. }\dfrac{\infty}{\infty} \\[0.3cm]
=& \ \dlim{x \to \infty}{\frac{0,3t}{t^2\left(1+\frac{4}{t^2} \right)}}  &&  \\[0.3cm]
=& \ \dlim{x \to \infty}{\frac{0,3}{t\left(1+\frac{4}{t^2} \right)}}  &&  \\[0.25cm]
=& \ 0
\end{alignat*}
À très long terme, la concentration du médicament dans le sang sera nulle. Il n'y aura donc plus de trace du médicament dans le sang.
}
\end{ex}

\ordi{\newpage}

\rem Chaque fois que le degré du numérateur est supérieur au degré du dénominateur, le résultat est $\pm \infty$. Chaque fois que le degré du numérateur est inférieur au degré du dénominateur, le résultat est 0.

\begin{ex}
{Évaluer $\dlim{x \to -\infty}{\displaystyle \frac{3x+4}{\sqrt{4x^2-3x+1}}}$. \\
}{
\begin{align*}
& \ \dlim{x \to -\infty}{\displaystyle \frac{3x+4}{\sqrt{4x^2-3x+1}}}  \hspace{2cm} & \text{F.I. }\dfrac{\infty}{\infty} \\[0.4cm]
=& \ \dlim{x \to -\infty}{\displaystyle \frac{x\left(3+ \frac{4}{x}\right)}{\sqrt{x^2\left(4-\displaystyle\frac{3}{x}+\frac{1}{x^2}\right)}}}  \\[0.4cm]
=& \ \dlim{x \to -\infty}{\displaystyle \frac{x\left(3+\displaystyle\frac{4}{x}\right)}{\sqrt{x^2}\sqrt{\left(4-\displaystyle\frac{3}{x}+\frac{1}{x^2}\right)}}}  \\[0.4cm]
=& \ \dlim{x \to -\infty}{\displaystyle \frac{x\left(3+\displaystyle\frac{4}{x}\right)}{-x\sqrt{\left(4-\displaystyle\frac{3}{x}+\frac{1}{x^2}\right)}}}  \hspace{2cm} & \sqrt{x^2}=-x \text{ car }x<0 ~ (\text{tend vers }-\infty) \\[0.4cm]
=& \ \dlim{x \to -\infty}{\displaystyle \frac{-\left(3+\displaystyle\frac{4}{x}\right)}{\sqrt{\left(4-\displaystyle \frac{3}{x}+\frac{1}{x^2}\right)}}}  \\[0.4cm]
=& \ -\frac{3}{\sqrt{4}} \\[0.4cm]
=& \ -\frac{3}{2}
\end{align*}
}
\end{ex} 


\ordi{\newpage}
\end{manuel}

\begin{bnframe}{Exemple \exe}
Évaluer les limites suivantes. \bea{\\[0.6cm]}

\ndc{\be} \bea{\be[<+- |alert@+>]  \setlength\itemsep{0.6cm}}
\item $\dlim{x \to \infty}{\frac{x^2-1}{x^3-x}}$  \ndc{\vfill}

\item $\dlim{x \to -\infty}{\frac{\sqrt{x^2+x-1}}{5-x}}$  \ndc{\vfill \vfill \newpage}
\ee
\end{bnframe}



\begin{bmntframe}{Forme $\infty - \infty$} 
\tool{g}{tabFormeinfmoinsinf}{
\man{
\begin{table}[!h] 
\caption{Forme indéterminée $\infty - \infty$}  
\label{tabFormeinfmoinsinf}} 
\mnt{\begin{mdframed}[style=encadre] 
\begin{center}{\bf Forme indéterminée $\infty - \infty$}  \end{center}}
\ \\
Voici quelques astuces permettant de lever l'indétermination dans le cas d'une forme $\infty - \infty$. \mnt{\\} \bea{\\[0.3cm]}
\man{
Nous avons vu dans la première partie une première astuce qui consiste à effectuer une mise en évidence dans le cas d'un polynôme. Voici deux nouvelles astuces qui seront utiles pour les fonctions algébriques.} 
\begin{enumerate}  \bea{\setlength\itemsep{0.3cm}}
\bn{\item S'il s'agit d'un polynôme, mettre en évidence la plus grande puissance de $x$.}

\item Dans le cas où l'expression contient des radicaux: multiplier par une fraction constituée du conjugué au numérateur et au dénominateur et simplifier.

\item Dans le cas d'une somme ou d'une différence de fractions algébriques: mettre sur le même dénominateur, effectuer l'opération et simplifier.
\end{enumerate}  
\mnt{\end{mdframed}}
\man{\end{table}}
}%tool
\end{bmntframe}


\begin{manuel}

\begin{ex}
{Évaluer $\dlim{x \to \infty}{\big(\sqrt{x-2}-\sqrt{x}\big)}$. \\
}{
\begin{alignat*}{4}
& \ \dlim{x \to \infty}{\big(\sqrt{x-2}-\sqrt{x}\big)} \hspace{7cm} && \text{F.I. } && \infty - \infty \\[0.4cm]
=& \ \dlim{x \to \infty}{\left(\big(\sqrt{x-2}-\sqrt{x}\big)\cdot \frac{\sqrt{x-2}+\sqrt{x}}{\sqrt{x-2}+\sqrt{x}}\right)}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{x-2-x}{\sqrt{x-2}+\sqrt{x}}}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{-2}{\sqrt{x-2}+\sqrt{x}}}  && \text{F.D. } && \dfrac{k}{ \infty}  \\[0.4cm]
=& \ 0
\end{alignat*}
}
\end{ex}

\ordi{\newpage}

\begin{ex}
{Évaluer $\dlim{x \to 2^-}{\left(\frac{1}{2-x}-\frac{2x}{4-x^2}\right)}$. \\
}{
\begin{alignat*}{4}
& \ \dlim{x \to 2^-}{\left(\frac{1}{2-x}-\frac{2x}{(2-x)(2+x)}\right)} \hspace{4cm} && \text{F.I. } && \dfrac{1}{0^+}-\dfrac{4}{0^+}=\infty - \infty \\[0.4cm]
=& \ \dlim{x \to 2^-}{\left(\frac{1}{2-x}-\frac{2x}{(2-x)(2+x)}\right)} \\[0.4cm]
=& \ \dlim{x \to 2^-}{\frac{2+x-2x}{(2-x)(2+x)}}     \\[0.4cm]
=& \ \dlim{x \to 2^-}{\frac{2-x}{(2-x)(2+x)}} \\[0.4cm]
=& \ \dlim{x \to 2^-}{\frac{1}{(2+x)}} \\[0.4cm]
=& \frac{1}{4}
\end{alignat*}
}
\end{ex}

Il arrive qu'après avoir effectuer une manipulation algébrique pour levée une indétermination, on se retrouve devant une autre forme indéterminée. Dans ce cas, il suffit d'appliquer une nouvelle astuce jusqu'à ce qu'on obtienne une forme déterminée.

\ordi{\newpage}

\begin{ex}
{Évaluer $\dlim{x \to \infty}{\big(\sqrt{x^2+3x}-x\big)}$. \\
}{
\begin{alignat*}{4}
& \ \dlim{x \to \infty}{\big(\sqrt{x^2+3x}-x\big)} \hspace{6cm} && \text{F.I. } \infty - \infty \\[0.4cm]
=& \ \dlim{x \to \infty}{\left(\big(\sqrt{x^2+3x}-x\big)\cdot \frac{\sqrt{x^2+3x}+x}{\sqrt{x^2+3x}+x}\right)}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{x^2+3x-x^2}{\sqrt{x^2+3x}+x}}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3x}{\sqrt{x^2+3x}+x}}   && \text{F.I. } \dfrac{\infty}{\infty}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3x}{\sqrt{x^2}\sqrt{1+\frac{3}{x}}+x}}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3x}{x\sqrt{1+\frac{3}{x}}+x}} && \text{car } x>0 \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3x}{x\left(\sqrt{1+\frac{3}{x}}+1\right)}}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3}{\sqrt{1+\frac{3}{x}}+1}}  \\[0.4cm]
=& \ \frac{3}{2}
\end{alignat*}
}
\end{ex}

\end{manuel}


\begin{bnframe}{Exemple \exe}
Évaluer $\dlim{x \to 2}{\left(\frac{1}{x^2-4} - \dfrac{1}{x-2}\right)}$.  \ndc{\vspace*{4cm}}

\end{bnframe}


\begin{bnframe}{Exercice \exe}
Évaluer, si possible, les limites suivantes.\bea{\\[0.6cm]}

\ndc{\be[series=p2lim1]}  \bea{\be \setlength\itemsep{0.6cm}}
\begin{tabular}{m{8cm}m{3cm}}
\item $\dlim{x\to 3}{\frac{x+1}{x^2-1}}$ & \vspace*{0.2cm}\monocg{R}{$\frac{1}{2}$} \\

\item $\dlim{x\to \infty}{\dfrac{\frac{1}{x^2-1}}{x+1}}$ & \vspace*{0.6cm} \monocg{R}{$0$}\\

\item $\dlim{x\to\infty}{\big(\sqrt{x+1} - \sqrt{x}\big)}$ & \vspace*{0.4cm} \monocg{R}{$0$}
\end{tabular}
\bea{\asuivre}
\ee
\end{bnframe}
\ndc{\newpage}

\addtocounter{exeBeaNdc}{-1}

\begin{bnframe}{Exercice \exe}
\ndc{\be[resume=p2lim1]}  \bea{\be \setlength\itemsep{0.6cm}
\suite}

\begin{tabular}{m{8cm}m{3cm}}
\item $\dlim{x\to 1}{\dfrac{x+2}{x^2-2x+1}}$ & \vspace*{0.5cm} \monocg{R}{$\infty$} \\

\item $\dlim{x\to 1}{\dfrac{x+2}{x-1}}$ & \vspace*{0.7cm} \monocg{R}{$\nexists$} \\

\item $\dlim{x\to \infty}{\sqrt[3]{\dfrac{x^3+2x^2-1}{27x^3-x+7}}}$ & \vspace*{0.7cm} \monocg{R}{$\frac{1}{3}$}
\end{tabular}
\bea{\asuivre}
\ee
\end{bnframe}

\addtocounter{exeBeaNdc}{-1}

\begin{bnframe}{Exercice \exe}
\ndc{\be[resume=p2lim1]}  \bea{\be \setlength\itemsep{0.6cm}
\suite}
\begin{tabular}{m{8cm}m{3cm}}
\item $\dlim{x\to 2}{\dfrac{x^2-x-2}{x^3-4x^2+6x-4}}$ & \vspace*{0.5cm} \monocg{R}{$\frac{3}{2}$} \\
\item $\dlim{x\to 4}{\dfrac{x-4}{\frac{7-x}{8-x} - \frac{3}{x}}}$ & \vspace*{0.5cm} \monocg{R}{$8$}
\end{tabular}
\ee
\end{bnframe}



\begin{bframe}{Devoirs} 
\begin{center}
\bi 
\item Relire la section \ref{secLimP1}. \\

Faire les exercices \#1 à 6 de la section \thechap.\thesec.
\ei
\end{center}
\end{bframe}

\begin{nframe}{}
\begin{mdframed}
DEVOIR : Faire les exercices \#1 à 6 de la section \thechap.\thesec. 
\end{mdframed}
\end{nframe}
\ndc{\vspace*{-0.3cm}}

\begin{bframe}{Activité}
\begin{center}
Activité sur les limites
\end{center}
\end{bframe}


\ordi{\newpage}
\begin{manuel}
\subsection{Asymptotes horizontales et verticales}
\end{manuel}


\begin{bmntframe}{Définition}
\begin{newdef}[Asymptote horizontale]{asympHor}
Une fonction $f$ admet une \emph{asymptote horizontale} d'équation $y=b$ si $\dlim{x \to -\infty}{f(x)}=b$ \textbf{ou} $\dlim{x \to \infty}{f(x)}=b$, où $b \in \mathds{R}$.\index{Asymptote!horizontale}
\end{newdef}
\ndc{\vspace*{-0.3cm}}
\end{bmntframe}


\begin{bmntframe}{Définition}
\begin{newdef}[Asymptote verticale]{asympVer}
Une fonction $f$ admet une \emph{asymptote verticale} d'équation\report{}{\\}$x=a$ si $\dlim{x \to a^-}{f(x)}=\pm \infty$ \textbf{ou} $\dlim{x \to a^+}{f(x)}=\pm \infty$, où $a \in \mathds{R}$. \index{Asymptote!verticale}\\[0.4cm]

\rem Les valeurs de $x$ qui sont à risque de correspondre à des asymptotes verticales sont exclues du domaine. Dans le cas des fonctions définies par parties, les valeurs charnières sont aussi à risque de correspondre à des asymptotes verticales.
\end{newdef}
\end{bmntframe}


\begin{manuel}
\begin{ex}
{Trouver les asymptotes horizontales et verticales de la fonction $f(x)=\displaystyle \frac{1-x}{x^2-1}$.\\
}{\ \\

Pour la asymptotes horizontales, il faut toujours évaluer les deux limites suivantes.

\begin{alignat*}{4}
& \ \dlim{x \to -\infty}{\frac{1-x}{x^2-1}} \hspace{6cm} && \text{F.I. } && \dfrac{\infty}{\infty} \\[0.4cm]
=& \ \dlim{x \to -\infty}{\frac{x\left(\frac{1}{x}-1\right)}{x^2\left(1-\frac{1}{x^2}\right)}} \\[0.4cm]
=& \ \dlim{x \to -\infty}{\frac{\left(\frac{1}{x}-1\right)}{x\left(1-\frac{1}{x^2}\right)}} && \text{F.D. } && \dfrac{k}{-\infty} \\[0.4cm]
=& \ 0 \\[1cm]
& \ \dlim{x \to \infty}{\frac{1-x}{x^2-1}} && \text{F.I. } && \dfrac{-\infty}{\infty} \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{\left(\frac{1}{x}-1\right)}{x\left(1-\frac{1}{x^2}\right)}} && \text{F.D. } && \dfrac{k}{\infty} \\[0.4cm]
=& \ 0
\end{alignat*}



On conclut qu'il y a une asymptote horizontale d'équation $y=0$. Attention, une seule des deux limites devait donner une valeur réelle pour qu'on soit en présence d'une asymptote horizontale. Toutefois, il faut toujours évaluer les deux limites parce qu'on pourrait obtenir deux asymptotes horizontales différentes.\\

Pour les asymptotes verticales, on étudie d'abord le domaine de la fonction.\\
$\dom(f)=\mathds{R}\setminus \left\lbrace {-1},\,1 \right\rbrace$. Ainsi, $x={-1}$ et $x=1$ sont des candidats potentiels.

\begin{align*}
& \ \dlim{x \to {-1}^-}{\frac{1-x}{x^2-1}} \hspace{2cm} & \text{F.D. }\dfrac{2}{0^+} \\[0.4cm]
=& \ \dlim{x \to {-1}^-}{\frac{1-x}{(x-1)(x+1)}} \hspace{2cm} & \text{F.D. }\dfrac{2}{0^+} \\[0.4cm]
=& \ \infty
\end{align*}

On peut tout de suite conclure qu'il y a une asymptote verticale d'équation $x={-1}$. Lorsque nous serons rendus au tracé de graphique, nous nous intéresserons aussi à la limite quand $x$ tend vers ${-1}$ par la droite pour connaître le comportement de la fonction autour de l'asymptote. Vérifions maintenant la présence d'une deuxième asymptote.

\begin{align*}
& \ \dlim{x \to 1^-}{\frac{1-x}{x^2-1}} \hspace{3cm} & \text{F.I. }\dfrac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 1^-}{\frac{-(-1+x)}{(x-1)(x+1)}} \\[0.4cm]
=& \ \dlim{x \to 1^-}{\frac{-1}{x+1}}  \\[0.4cm]
=& \ -\frac{1}{2}
\end{align*}

\begin{align*}
& \ \dlim{x \to 1^+}{\frac{1-x}{x^2-1}} \hspace{3cm} & \text{F.I. }\frac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 1^+}{\frac{-(-1+x)}{(x-1)(x+1)}} \\[0.4cm]
=& \ \dlim{x \to 1^+}{\frac{-1}{x+1}}  \\[0.4cm]
=& \ -\frac{1}{2}
\end{align*}
On conclut ici qu'il n'y a pas d'asymptote verticale en $x=1$.

}
\end{ex}
\end{manuel}


\begin{bframe}{Ressource additionnelle}
\begin{center}
WolframAlpha
\end{center}
\end{bframe}


\begin{bframe}{Devoirs} 
\begin{center}
Faire l'exercice \#7 de la section \thechap.\thesec.
\end{center}
\end{bframe}

\begin{nframe}{}
\begin{mdframed}
DEVOIR : Faire l'exercice \#7 de la section \thechap.\thesec. 
\end{mdframed}
\end{nframe}


\begin{manuel}
\mob{\newpage}
\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?time_continue=491&v=Q_6djie5Bp8}{Forme c/0.}

\ \\

\href{https://www.youtube.com/watch?time_continue=438&v=vWmQAq-aKnw}{Forme indéterminée 0/0 (fonction rationnelle). }

\ \\


\href{https://www.youtube.com/watch?time_continue=375&v=TOCT107ofRI}{Forme indéterminée 0/0 (fonction rationnelle avec division euclidienne).}

\ \\


\href{https://www.youtube.com/watch?v=VbGCw7z-0O4}{Forme indéterminée 0/0 (racine). }

\ \\



\href{https://www.youtube.com/watch?v=SHsQ_NM8Fyo}{Forme indéterminée 0/0 (fraction complexe). }

\ \\


\href{https://www.youtube.com/watch?v=D0OIHmmirT8}{Forme infini/infini  (fonction rationnelle). }

\ \\


\href{https://www.youtube.com/watch?v=e8IYj18iyio}{Forme infini/infini (avec racine carrée).}

\ \\



\href{https://www.youtube.com/watch?v=r_hKgKgClGM&index=3&list=PLR4dIO3FEk8R8AkkeWu0pfmq2imwDbSKD}{Limite infini-infini (avec fractions). }

\ \\


\href{https://www.youtube.com/watch?time_continue=180&v=GdMYVzKcy_M}{Asymptotes verticales. }

\ \\

\href{https://www.youtube.com/watch?time_continue=432&v=ig-NDCYeoOA}{Asymptotes horizontales. }

\ \\

\href{https://www.wolframalpha.com}{WolframAlpha}

Voici deux exemples d'écriture de limites dans un langage que WolframAlpha comprend. \\

Pour calculer $\dlim{x \to \infty}{3x^2}$, on écrit \og limit 3x\^{}2, x=infinity \fg{}. \\

Pour calculer $\dlim{x \to 2^+}{3x^2}$, on écrit \og limit 3x\^{}2, x=2 right \fg{}. \\
\end{manuel}





%%%%% exercices limites partie 2
\begin{sectionEx}

\Q{Évaluer les limites.}

\debutSousQ
\Q{$\dlim{x \to {-1}^+}{\dfrac{-2}{x+1}}$ \hfill}  \rep{p}{$-\infty$ donc $\nexists$}
\Q{$\dlim{x \to {-1}^-}{\dfrac{-2}{x+1}}$ \hfill}  \rep{p}{$\infty$ donc $\nexists$} 
\Q{$\dlim{x \to {-1}}{\dfrac{-2}{x+1}}$ \hfill}  \rep{p}{$\nexists$}
\Q{$\dlim{x \to {-2}}{\dfrac{x+2}{x^2+5x+6}}$ \hfill}  \rep{p}{1}
\Q{$\dlim{x \to {-3}}{\dfrac{9-x^2}{x+3}}$ \hfill}  \rep{p}{6}
\Q{$\dlim{x \to 4}{\sqrt{2x+8}}$ \hfill}  \rep{p}{4}
\Q{$\dlim{x \to -\infty}{\dfrac{x}{|x|}}$ \hfill}  \rep{p}{${-1}$} 
\Q{$\dlim{x \to 16}{\dfrac{16-x}{\sqrt{x}-4}}$ \hfill}  \rep{p}{${-8}$}
\Q{$\dlim{x \to 1}{\dfrac{2x^3-10x^2-2x+10}{x-1}}$ \hfill}  \rep{p}{${-16}$} 
\Q{$\dlim{x \to 2}{\dfrac{4-x^2}{-x^3-3x^2+4x+12}}$ \hfill}  \rep{p}{$\dfrac{1}{5}$} 
\Q{$\dlim{x \to 1^+}{\dfrac{x-1}{x^3-2x^2+x}}$ \hfill}  \rep{p}{$\infty$ donc $\nexists$} 
\Q{$\dlim{x \to 0}{\left(\dfrac{1}{x}-\dfrac{1}{x^2}\right)}$ \hfill}  \rep{p}{$-\infty$ donc $\nexists$} 
\Q{$\dlim{x \to 1}{\left(\dfrac{\sqrt{x}}{x-1}+\dfrac{1-x}{x^2-2x+1}\right)}$ \hfill}  \rep{p}{$\dfrac{1}{2}$}
\Q{$\dlim{x \to 5}{\dfrac{x^2-3x-10}{x^2-10x+25}}$ \hfill}  \rep{p}{$\nexists$}
\Q{$\dlim{x \to 1}{\dfrac{x^3+x^2-5x+3}{x^3-3x+2}}$ \hfill}  \rep{p}{$\dfrac{4}{3}$} 
\Q{$\dlim{x \to 4^+}{\dfrac{3-x}{x^2-2x-8}}$ \hfill}  \rep{p}{$-\infty$ donc $\nexists$} 
\Q{$\dlim{x \to 0^-}{\left(\dfrac{1}{x}+\dfrac{1}{x^2}\right)}$ \hfill}  \rep{p}{$\infty$ donc $\nexists$} 
\Q{$\dlim{x \to 0}{\dfrac{\sqrt{x+4}-2}{x}}$ \hfill}  \rep{p}{$\dfrac{1}{4}$} 
\finSousQ

\Q{Évaluer les limites.}

\debutSousQ
\Q{$\dlim{x \to \infty}{\dfrac{x^2+3x}{x^2+1}}$ \hfill}  \rep{p}{$1$} 
\Q{$\dlim{x \to -\infty}{\dfrac{3x^2+2x-1}{-x^3+3x^2+1000}}$ \hfill}  \rep{p}{$0$} 
\Q{$\dlim{x \to \infty}{\dfrac{5x^3-2x^2}{1-3x}}$ \hfill}  \rep{p}{$-\infty$ donc $\p\nexists$} 
\Q{$\dlim{x \to \infty}{\sqrt[5]{\dfrac{32x^2-4x+1}{x^2+x+12}}}$ \hfill}  \rep{p}{$2$} 
\Q{$\dlim{x \to -\infty}{\dfrac{\sqrt{x^2+2}}{3x-6}}$ \hfill}  \rep{p}{$-\dfrac{1}{3}$} 
\Q{$\dlim{x \to \infty}{\left(\sqrt{x^6+8}-x^3\right)}$ \hfill}  \rep{p}{$0$} 
\Q{$\dlim{x \to \infty}{\big(-x^4+x^2-12\big)}$ \hfill}  \rep{p}{$-\infty$ donc $\nexists$} 
\Q{$\dlim{x \to -\infty}{\sqrt{4-x}}$ \hfill}  \rep{p}{$\infty$ donc $\nexists$} 
\Q{$\dlim{x \to \infty}{\dfrac{2-x}{\sqrt{7+9x^2}}}$ \hfill}  \rep{p}{$-\dfrac{1}{3}$} 
\finSousQ

\Q{Soit
$$f(x) =
\begin{fctpp}
$\dfrac{1}{x+2}$ & si $x < {-2}$ \\
$x^2-5$ & si ${-2} < x < 3$ \\
$\sqrt{x+13}$ & si $ x > 3$ \\
\end{fctpp}.$$
Évaluer les limites suivantes.}

\debutSousQ
\Q{$\dlim{x \to {-2}^-}{f(x)}$ \hfill}  \rep{p}{$-\infty$ donc $\nexists$.} 
\Q{$\dlim{x \to {-2}^+}{f(x)}$ \hfill}  \rep{p}{${-1}$} 
\Q{$\dlim{x \to {-2}}{f(x)}$ \hfill}  \rep{p}{$\nexists$} 
\Q{$\dlim{x \to 0}{f(x)}$ \hfill}  \rep{p}{${-5}$}  
\Q{$\dlim{x \to 3^-}{f(x)}$ \hfill}  \rep{p}{$4$}  
\Q{$\dlim{x \to 3^+}{f(x)}$ \hfill}  \rep{p}{$4$}  
\Q{$\dlim{x \to \infty}{f(x)}$ \hfill}  \rep{p}{$\infty$ donc $\nexists$.}  
\Q{$\dlim{x \to -\infty}{f(x)}$ \hfill}  \rep{p}{$0$}   
\finSousQ

\Q{Sachant que $\dlim{x \to a}{f(x)}=3$, $\dlim{x \to a}{g(x)}={-1}$ et $\dlim{x \to a}{h(x)}=0$, évaluer si possible les limites suivantes.}

\debutSousQ
\Q{$\dlim{x \to a}{\big(f(x)+3g(x)\big)}$ \hfill}  \rep{p}{$0$} 
\Q{$\dlim{x \to a}{\big(h(x)-4g(x)+1\big)}$ \hfill}  \rep{p}{$5$} 
\Q{$\dlim{x \to a}{f(x)g(x)}$ \hfill}  \rep{p}{${-3}$} 
\Q{$\dlim{x \to a}{(g(x))^2}$ \hfill}  \rep{p}{$1$} 
\Q{$\dlim{x \to a}{\sqrt[3]{5+f(x)}}$ \hfill}  \rep{p}{$2$} 
\Q{$\dlim{x \to a}{\dfrac{3}{g(x)}}$ \hfill}  \rep{p}{${-3}$} 
\Q{$\dlim{x \to a}{\dfrac{6f(x)}{f(x)+3g(x)}}$ \hfill}  \rep{p}{$\nexists$} 
\finSousQ

\Q{Sachant que $$\dfrac{2x^7+x^5-4x+10}{12+x^4+x^7} \leq g(x) \leq \dfrac{\sqrt{4x^2+2x+3}}{6+x}\textrm{,}$$ évaluer $\dlim{x \to \infty}{g(x)}$. \hspace*{0.1cm} \hfill} \rep{p}{$2$}

\Q{Sachant que $$\dfrac{10x^2-35x+30}{x^2+x-6} \leq h(x) \leq \dfrac{x-2}{\sqrt{x^2-3}-1}+\dfrac{1}{2}\textrm{,}$$ évaluer $\dlim{x \to 2}{h(x)}$. \hspace*{0.1cm} \hfill} \rep{p}{$1$}
\man{\columnbreak}
\Q{Identifier, s'il y en a, les asymptotes horizontales et verticales des fonctions suivantes.}
\debutSousQ
\Q{$f(x)=(x+1)^2(2x-x^2)$ \hfill}  \rep{p}{Il n'y a pas d'asymptote.}
\Q{$f(x)=\dfrac{2x-6}{4-x}$ \hfill}  \rep{m}{Asymptote horizontale $y={-2}$. \\ Asymptote verticale $x=4$.} 
\Q{$f(x)=\dfrac{3x^2-8}{x^2-4}$ \hfill}  \rep{m}{Asymptote horizontale $y=3$. Asymptotes verticales $x=2$ et $x={-2}$.} 
\Q{$f(x)=\dfrac{3(x+1)^2}{(x-1)^2}$ \hfill}  \rep{m}{Asymptote horizontale $y=3$. Asymptote verticale $x=1$.} 
\Q{$f(x)=\sqrt[3]{\dfrac{8x+5}{x-8}}$ \hfill}  \rep{m}{Asymptote horizontale $y=2$. Asymptote verticale $x=8$.} 
\Q{$f(x)=\dfrac{\sqrt{x^2+2}}{3x-6}$ \hfill}  \rep{m}{Asymptotes horizontales $y={-1}/3$\\ dans la partie négative et $y=1/3$ dans la partie positive. Asymptote verticale $x=2$.} 
\finSousQ

\end{sectionEx}




\section{Continuité}

\begin{bmnframe}{Continuité}
\rappel Nous avons vu au \tip{théorème \ref{thmContinuite}}{thmContinuite} que toutes les fonctions non définies par parties sont continues sur leur domaine.\bea{\\ \vspace*{0.5cm}} De plus, la \tip{définition de la continuité en un point}{defContinuiteEnUnPoint} est toujours la même.\\
\end{bmnframe}


\begin{manuel}
\begin{ex}
{Déterminer le plus grand ensemble sur lequel la fonction $f(x)=\displaystyle\frac{x+2}{x^2+5x}$ est continue.\\
}{\ \\

Il suffit de trouver le domaine de la fonction $f$.
Ici, la seule opération problème à considérer est la division par 0.
$$x^2+5x=0 \Rightarrow x(x+5)=0 \Rightarrow x=0 \text{ ou } x={-5}$$
On obtient donc $\dom (f) = \mathds{R} \setminus \left\lbrace{-5},\,0\right\rbrace$.
Ainsi, la fonction $f$ est continue $\forall x \in \mathds{R} \setminus \left\lbrace{-5},\,0\right\rbrace$.
}

\end{ex}

\begin{ex}
{Déterminer le plus grand ensemble sur lequel la fonction $g(x)=\displaystyle \frac{\sqrt{x}}{1+x^2}$ est continue.\\
}{\ \\

Ici il y a deux opérations problèmes à considérer. D'abord, il y aura une division par zéro si
$1+x^2=0 \Rightarrow x^2={-1}$, ce qui est impossible. De plus, comme il y a une racine paire, on doit avoir $x\geq 0$. La fonction $g$ est donc continue sur $\left[0,\, \infty\right[$.
}
\end{ex}


\ordi{\newpage}
\end{manuel}

\begin{bmnframe}{Continuité des fonctions définies par parties}
\bea{\justifying}
Lorsque la fonction est définie par parties, il faut étudier la continuité de chaque branche de la fonction et la continuité à chaque valeur charnière.
\end{bmnframe}

\begin{manuel}
\begin{ex}
{Déterminer le plus grand ensemble sur lequel la fonction suivante est continue.
$$f(x) =
\begin{fctpp}
$\sqrt{9-x^2}$ & si $x\leq 2$ \\
$\displaystyle \frac{x^2-4}{x-2}$ & si $x > 2$ \\
\end{fctpp}$$
}{\ \\

Il faut d'abord étudier la continuité sur chaque branche.

Le domaine de la fonction $\sqrt{9-x^2}$ est $\left[{-3},\, 3\right]$, donc la fonction $f$ est continue sur $\left[{-3},\, 2\right[$.\\
Le domaine de la fonction $\displaystyle \frac{x^2-4}{x-2}$ est $\mathds{R} \setminus \left\lbrace 2\right\rbrace$, donc la fonction $f$ est continue sur $\left]2,\, \infty \right[$.

On étudie ensuite la continuité à la valeur charnière $x=2$.

\be[label=\arabic*)] 
\item $f(2) = \sqrt{9-2^2}=\sqrt{5}$ donc $2 \in \dom(f)$. 

%%%%Aligner à gauche
\item 
\begin{alignat*}{4}
\dlim{x \to 2^-}{f(x)} =& \dlim{x \to 2^-}{\sqrt{9-x^2}} = \sqrt{5}  \\[1cm]
\dlim{x \to 2^+}{f(x)} =& \ \dlim{x \to 2^+}{\frac{x^2-4}{x-2}} \hspace{4cm} && \text{F.I. } && 0/0 \\[0.4cm]
=& \ \dlim{x \to 2^+}{\frac{(x-2)(x+2)}{x-2}} \\[0.4cm]
=& \ \dlim{x \to 2^+}{(x+2)} \\[0.4cm]
=& \ 4 
\end{alignat*}
\ee
Ainsi la limite n'existe pas. \\

La fonction $f$ est donc discontinue en $x=2$. On en conclut qu'elle est continue sur $\left[{-3},\, \infty\right[ \setminus \left\lbrace 2\right\rbrace$
}
\end{ex}
\end{manuel}



\begin{bnframe}{Exercice \exe}
Déterminer le plus grand ensemble sur lequel les fonctions suivantes sont continues. \bea{\\[0.5cm]}

\be  \bea{\setlength\itemsep{0.6cm}}
\begin{tabular}{m{7cm}m{4cm}}
\item  $f(x) = \dfrac{\sqrt{x-2}}{x-5}$ &  \monocg{R}{$[2,\, \infty[ \, \setminus \, \{5\}$} \\  \ndc{\vspace*{3cm}}

\item $f(x) = \begin{fctpp}
$3x+3$ & si $x < 0$ \\
$12$ & si $x=0$ \\
$\sqrt{9-x}$ & si $0< x \leq 5$ \\
$\dfrac{3}{x-10}$ & si $x > 5$ \\
\end{fctpp}$ &  \monocg{R}{$\mathds{R}\setminus \{0, \, 5,\, 10\}$} \\ 
\end{tabular}
\ee
\ndc{\vfill }
\end{bnframe}




\devoirs
\ndc{\newpage}



%%%% exercices continuité algébrique 
\begin{sectionEx}

\Q{Déterminer pour quelles valeurs de $x$ la fonction $f$ est continue.}

\debutSousQ
\Q{
$f(x) =
\begin{fctpp}
$\sqrt{x^2-5}$ & si $x \geq 3$ \\
$x-1$ & si $1 < x < 3$ \\
$\dfrac{1}{x}$ & si $ x \leq 1$ \\
\end{fctpp}$ \hspace*{0.01cm} \hfill}  \rep{p}{$\p\mathrm{R}\p\setminus\p\{0,\, 1\}$}

\Q{$f(x)=\sqrt{\dfrac{(x+1)(x-2)}{({-3}-x)}}$ \hfill}  \rep{p}{$]-\infty,\, {-3}[ \p\cup [{-1},\, 2] $} 
\Q{$f(x)=\dfrac{-2x^2+x+1}{x^2-3x+2}$ \hfill}  \rep{p}{$\mathrm{R}\setminus\{1,\,2\}$} 
\Q{$f(x)=\dfrac{5}{x}+\dfrac{2x}{x+4}$ \hfill}  \rep{p}{$\mathrm{R}\setminus\{{-4},\,0\}$}  
\finSousQ

\man{\renewcommand{\arraystretch}{1.5}}

\Q{Soit la fonction $f$ suivante
$$f(x) =
\begin{fctpp}
$\sqrt[3]{x^2+4}+x$ & si $x \leq 2$ \\
&\\
$\dfrac{2k^2x+3}{x-1}$ & si $2 < x < 3$ \\
&\\
$\dfrac{\sqrt{x^2-5}+2x+1}{2}$ & si $x \geq 3$ \\
\end{fctpp}$$
où $k$ est une constante.}
\debutSousQ
\Q{Trouver une valeur de $k$ telle que la fonction $f$ soit continue en $x=2$. \hfill} \rep{p}{$k=\pm \frac{1}{2}$}
\Q{Déterminer si $f$ est alors continue en $x=3$. \\ \hspace*{0.3cm} \hfill} \rep{p}{Non}
\Q{Déterminer la valeur qu'il faut donner à $k$ pour que la fonction $f$ soit continue en $x=3$. \\ \hspace*{0.3cm} \hfill} \rep{p}{$k=\pm 1$}
\finSousQ

\end{sectionEx}


%</ici>