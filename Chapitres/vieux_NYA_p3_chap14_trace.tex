

\chapter{Tracé de graphiques de fonctions transcendantes}

\section{Croissance, décroissance et extremums relatifs}

\begin{manuel}
Nous avons vu que pour déterminer la \tip{croissance et la décroissance}{defCroissanceDecroissance} d'une fonction, il faut étudier le signe de la dérivée première de la fonction. Le tout est résumé au \tip{théorème \ref{thmCroissanceDecroissance}}{thmCroissanceDecroissance}. Puisque ce théorème ne dépend pas du type de fonction, il s'applique aussi pour les fonctions transcendantes. 

De plus, nous avons vu que les valeurs de $x$ où la fonction peut potentiellement passer de croissante à décroissante, ou vice versa, sont les \tip{valeurs critiques}{defValeurCritique} de la fonction. À ces valeurs critiques, on peut, ou non, retrouver des \tip{extremums relatifs}{defExtremumsRelatifs}. Pour le vérifier, nous pouvons utiliser le \tip{test de la dérivée première}{thmCritereDeriveePremiere} ou le \tip{test de la dérivée seconde}{thmCritereDeriveeSeconde}. 

\begin{ex}
\QS{Déterminer les intervalles de croissance et de décroissance ainsi que les extremums relatifs de la fonction $f(x) =\dfrac{2}{e^{x^2}}+3$.}{

Nous avons $\dom(f)=\mathds{R}$ puisque $e^{x^2}$ n'égale jamais zéro. 

Pour trouver les valeurs critiques de $f$, nous trouvons sa dérivée. 

Pour ce faire, on peut d'abord réécrire la fonction de la façon suivante: $f(x) = 2e^{-x^2}+3$.

Nous obtenons $f'(x) = -\dfrac{4x}{e^{x^2}}$. 

Comme $\dom(f')=\mathds{R}$, il n'y a aucune valeur de $x$ telle que $f'(x) ~\nexists$. 

De plus, $f'(x)=0 \Rightarrow 4x=0 \Rightarrow x=0$. Comme cette valeur fait partie du domaine de $f$, il s'agit d'une valeur critique, et donc potentiellement de l'abscisse d'un extremum relatif. 

\newpage

On construit ensuite le tableau de signes en tenant compte du domaine et de la valeur critique trouvée.
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{2cm}|C{2cm}|C{1cm}|C{2cm}|}
\hline
$x$ & & $0$ &  \\ \hline
$f'(x)$ & $+$ & $0$ & $-$ \\ \hline
$f(x)$ & $\nearrow$ & max $5$ & $ \searrow$  \\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
Donc, $f$ est croissante sur $]-\infty,\, 0]$ et décroissante sur $[0,\, \infty[$. De plus $f$ possède un maximum relatif de 5, atteint lorsque $x=0$. 
}
\end{ex}

\begin{ex}
\QS{Trouver les extremums relatifs de la fonction $f(\theta)=\cos^2{(2\theta)}$ sur l'intervalle $]0,\frac{\pi}{2}[$ en utilisant le \tip{test de la dérivée seconde}{thmCritereDeriveeSeconde}.}{

Nous avons $\dom(f) = \mathds{R}$. Ici, nous nous limiterons à l'intervalle $\crochetso*{0,\frac{\pi}{2}}$.

Nous trouvons $f'(\theta) = -4\cos(2\theta)\sin(2\theta)$. 

On peut simplifier cette expression en utilisant une \tip{identité trigonométrique}{propAutresIdentitesTrigo}.

Ainsi, $f'(\theta)={-2}\sin(4\theta)$.

Ensuite, en dérivant $f'$, nous obtenons $f''(\theta) ={-8}\cos(4\theta)$. 

On a $f'(\theta)=0$ si $\sin(4\theta)=0\Rightarrow 4\theta = 0$, $\pi$ ou $2\pi \Rightarrow \theta =0$, $\dfrac{\pi}{4}$ ou $\dfrac{\pi}{2}$. Comme $0$ et $\dfrac{\pi}{2}$ ne font pas partie de l'intervalle étudié, $\theta=\dfrac{\pi}{4}$ est la seule valeur critique où $f'(\theta)=0$.

De plus, $\dom(f')=\mathds{R}$ donc il n'y a aucune valeur $\theta$ dans l'intervalle telle que $f'(\theta)~\nexists$.

Ainsi, sur cet intervalle, le seul candidat est $\theta=\dfrac{\pi}{4}$.

Comme $f''\left(\dfrac{\pi}{4}\right)=8 > 0$, la fonction $f$ admet un minimum relatif en $x=\dfrac{\pi}{4}$, selon le \tip{test de la dérivée seconde}{thmCritereDeriveeSeconde}. Ce minimum est $f\left(\dfrac{\pi}{4}\right)=0$.
}
\end{ex}
\end{manuel}



\begin{bframe}{Croissance et décroissance}

Pour trouver les intervalles de \tip{croissance et de décroissance}{defCroissanceDecroissance} d'une fonction $f$, nous utilisons le \tip{théorème \ref{thmCroissanceDecroissance}}{thmCroissanceDecroissance}.
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]
\item Trouver le domaine de $f$. 

\item Trouver les \tip{valeurs critiques}{defValeurCritique} de $f$. 

Attention au domaine de $f$ et $f'$. 

\item Faire un tableau de signes de la dérivée première. 
\end{liste}
\end{bframe}



\begin{bnframe}{Exercice \exe}
\Q{Déterminer les intervalles de croissance et de décroissance, ainsi que les extremums relatifs, des fonctions suivantes.}
\debutSousQ[itSep=0.3cm]
\QR{$f(x) = \ln{\left(\sqrt{x^2+4}\right)}$}{La fonction $f$ est croissante sur $[0,\, \infty[$ et décroissante sur $]-\infty,\, 0]$. Elle possède un minimum relatif de $\ln(2)$ atteint en $x=0$.} 
\QR{$f(x) = -\arctan(x^2-1)$}{La fonction $f$ est croissante sur $]-\infty,\, 0]$ et décroissante sur $[0,\, \infty[$. Elle possède un maximum relatif de $\frac{\pi}{4}$ atteint en $x=0$.} 
\finSousQ
\end{bnframe}




\devoirs


\begin{manuel}
\newpage

\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?v=O82FRyA5fzY&list=PLQhcwa4wrwRpsC-Y-NwaFxb3ydas0bI96&index=3}{Trouver les extremums relatifs (fonction exponentielle)}

\href{https://www.youtube.com/watch?v=O82FRyA5fzY&list=PLQhcwa4wrwRpsC-Y-NwaFxb3ydas0bI96&index=3}{Trouver les valeurs critiques, les asymptotes et les extremums relatifs d'une fonction trigonométrique (attention, la notation utilisée pour la limite n'est pas rigoureuse) }
\end{manuel}



\begin{sectionEx}

\Q{Déterminer les intervalles de croissance et de décroissance des fonctions suivantes, ainsi que les abscisses des extremums relatifs.}
\debutSousQ
\QR{$f(x)=\ln\paren*{\dfrac{x^2}{x^2-1}}$}{Croissante sur $]-\infty,\,-1[$. Décroissante sur $]1,\, \infty[$. Aucun extremum relatif.}  
\QR{$f(x)=\arcsin(x^2)$}{Croissante sur $[0,\,1]$. Décroissante sur $[-1,\, 0]$. Minimum relatif en $x=0$. Maximum relatif en $x=-1$ et en $x=1$.} 
\QR{$f(x)=\sin(x^2)$ sur $[-\sqrt{\pi},\,\sqrt{\pi}]$}{Croissante sur $\crochets*{-\sqrt{\pi},\,-\sqrt{\frac{\pi}{2}}}$ et sur $\crochets*{0,\,\sqrt{\frac{\pi}{2}}}$. Décroissante sur $\crochets*{-\sqrt{\frac{\pi}{2}},\,0}$ et sur $\crochets*{\sqrt{\frac{\pi}{2}},\,\sqrt{\pi}}$. Minimum relatif en $x=0$, $x=-\sqrt{\pi}$ et en $x=\sqrt{\pi}$. Maximum relatif en $x=-\sqrt{\frac{\pi}{2}}$ et en $x=\sqrt{\frac{\pi}{2}}$. } 
\QR{$f(x)=e^x+\dfrac{e^x}{x}$}{Croissante sur $\crochets*{-\infty,\,\frac{-1-\sqrt{5}}{2}}$ et sur $\crochetsfo*{\frac{\sqrt{5}-1}{2},\,\infty}$. Décroissante sur $\crochets*{\frac{-1-\sqrt{5}}{2},\,\frac{\sqrt{5}-1}{2}}\setminus\{0\}$. Minimum relatif en $x=\frac{\sqrt{5}-1}{2}$. Maximum relatif en $x=\frac{-1-\sqrt{5}}{2}$.} 
\finSousQ

\end{sectionEx}


\section{Concavité et points d'inflexion}   \label{secConcaviteTrans}

\begin{manuel}
Pour déterminer la concavité d'une fonction $f$, il faut étudier le signe de sa dérivée seconde. Le tout est résumé au \tip{théorème \ref{thmConcavite}}{thmConcavite}. Puisque ce théorème ne dépend pas du type de fonction, il s'applique aussi pour les fonctions transcendantes. 

Les \tip{points d'inflexion}{defPointInflexion} sont les endroits où la fonction change de concavité et ils se trouvent potentiellement aux endroits où la dérivée seconde s'annule ou n'existe pas. 

Nous utiliserons donc le tableau de signes de la dérivée seconde pour déterminer les intervalles de concavité vers le haut et vers le bas et identifier les valeurs où on retrouve des points d'inflexion.
\end{manuel}


\begin{bframe}{Concavité et points d'inflexion}
Pour déterminer les intervalles de concavité vers le haut et vers le bas d'une fonction $f$ (\tip{théorème \ref{thmConcavite}}{thmConcavite}), ainsi que pour trouver les \tip{points d'inflexion}{defPointInflexion} de $f$ : 
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]
\item Trouver le domaine de $f$. 
\item Trouver les endroits où la dérivée seconde s'annule ou n'existe pas. 

Attention au domaine de $f$ et $f''$. 
\item Faire un tableau de signes de la dérivée seconde. 
\end{liste}
\end{bframe}


\begin{manuel}
\begin{ex}
\QS{Déterminer les intervalles de concavité vers le haut et de concavité vers le bas ainsi que les points d'inflexion de la fonction $f(x)=\ln(x^2+1)$.}{

Nous avons $\dom(f) = \mathds{R}$ puisque $x^2+1$ est toujours positif. 

Nous trouvons les deux premières dérivées. 

Nous obtenons $f'(x) = \dfrac{2x}{x^2+1}$ et $f''(x) = \dfrac{{-2}(x-1)(x+1)}{(x^2+1)^2}$. 

Nous déterminons les valeurs de $x$ pour lesquelles la deuxième dérivée s'annule ou n'existe pas. Il s'agit des endroits où la concavité peut changer. 

Nous trouvons $\dom(f'') = \mathds{R}$, donc il n'y a aucune valeur de $x$ telle que $f''(x)~\nexists$.

De plus, $f''(x)=0 \Rightarrow x={-1}$ ou $x=1$. Comme ces deux valeurs font partie du domaine de $f$, il s'agit possiblement d'abscisses de points d'inflexion, ce que nous confirmerons avec le tableau de signes.
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{2cm}|C{2cm}|C{1cm}|C{2cm}|C{1cm}|C{2cm}|}
\hline
$x$ & &  ${-1}$  &  & 1 & \\ \hline
$f''(x)$ & $-$ & $0$ & $+$ & 0 & $-$   \\ \hline
$f(x)$ & $\cap$ & pt inf $\ln(2)$ &  $\cup$ &  pt inf $\ln(2)$ & $\cap$  \\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
La fonction est donc concave vers le haut sur $[{-1},\, 1]$ et concave vers le bas sur $]-\infty,\, {-1}]$ et sur $[1,\,\infty[$. De plus, cette fonction admet deux points d'inflexion $({-1},\,\ln(2))$ et $(1,\,\ln(2))$.
}
\end{ex}
\end{manuel}


\begin{bnframe}{Exercice \exe}

\QR{Déterminer les intervalles de concavité vers le haut et vers le bas, ainsi que les points d'inflexion, de la fonction $f(x) = e^x-e^{-x}$.}{La fonction $f$ est concave vers le haut sur $[0,\, \infty[$. Elle est concave vers le bas sur $]-\infty,\, 0]$. Elle possède un point d'inflexion au point $(0,\,0)$.} 
\end{bnframe}





\devoirs


\begin{manuel}
\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?v=SxHQjnQFHwM&list=PLQhcwa4wrwRpsC-Y-NwaFxb3ydas0bI96&index=5}{Trouver les points d'inflexion (fonction logarithmique) }
\end{manuel}


\begin{sectionEx}
\Q{Déterminer les intervalles sur lesquels les fonctions suivantes sont concaves vers le haut et vers le bas, ainsi que les abscisses des points d'inflexion.}
\debutSousQ
\QR{$f(x)=xe^x$}{Concave vers le bas sur $]-\infty,\,{-2}]$. Concave vers le haut sur $[{-2},\,\infty[$. Point d'inflexion en $x={-2}$.}  
\QR{$f(x)=2x+\arcsin(1-x)$}{Concave vers le bas sur $[1,\,2]$. Concave vers le haut sur $[0,\,1]$. Point d'inflexion en $x=1$.} 
\QR{$f(x)=\ln\left(\dfrac{e^{2x+1}}{(e^x-1)^2}\right)$}{Concave vers le haut sur $\mathds{R}\setminus\{0\}$. Aucun point d'inflexion.} 
\QR{$f(x)=8\sin(x)-\tan(x)$ où $x \, \in \crochetso*{\dfrac{-\pi}{2},\,\dfrac{\pi}{2}}$ \hfill}  \rep{m}{Concave vers le bas sur $\crochetsfo*{0,\,\frac{\pi}{2}}$. Concave vers le haut sur $\left]-\frac{\pi}{2},\,0\right]$. Point d'inflexion en $x=0$.} 
\finSousQ
\end{sectionEx}




\section{Tracé de fonctions transcendantes}
\begin{bmn}
Pour le tracé de fonctions transcendantes, nous utiliserons la \tip{démarche}{tabMethodeTraceFct} vue dans les sections précédentes.
\end{bmn}





\begin{manuel}
\begin{ex}
\QS{Faire l'étude complète de la fonction $f(x) = \sqrt{x} \ \ln(x)$ sachant que $\dlim{x \to 0^+}{\sqrt{x} \ln{(x)}}=0$.}{

\textbf{Étape 1: Trouver le domaine de la fonction.} 

Nous obtenons $\dom(f) = ]0 ,\, \infty[$.\\

\textbf{Étape 2: Déterminer si la fonction possède des asymptotes horizontales et verticales.} 

La seule valeur de $x$ possible où il peut y avoir une asymptote verticale est $x=0$, à cause du logarithme. Or, $\dlim{x \to 0^+}{\sqrt{x}\ln{(x)}}=0$, donc il n'y a pas d'asymptote verticale.

Ici, nous n'évaluons pas la limite quand $x$ tend vers 0 par la gauche à cause du domaine.

On étudie ensuite le comportement à l'infini de la fonction.

$\dlim{x \to \infty}{\sqrt{x} \ln{(x)}}=\infty$

Il n'y a donc pas d'asymptote horizontale lorsque $x$ tend vers l'infini. 

Nous n'avons pas à vérifier l'existence d'asymptote horizontale à gauche à cause du domaine.\\

\textbf{Étape 3: Trouver les points critiques de la fonction (pour trouver les lieux potentiels des extremums relatifs).}  

Nous trouvons $f'(x) = \dfrac{\ln(x) + 2}{2\sqrt{x}}$. 

Nous avons, $\dom(f') = ]0 ,\, \infty[$. Il n'y a donc pas de valeur de $x$ élément du domaine de $f$ telles que $f'(x)~\nexists$.  

Ensuite, $f'(x) = 0 \Rightarrow \ln{(x)}+2 = 0 \Rightarrow \ln(x)={-2} \Rightarrow x=e^{-2}$. Comme cette valeur fait partie du domaine de $f$, il s'agit d'une valeur critique de $f$.  \ordi{\newpage}

\textbf{Étape 4: Trouver les endroits du domaine de $f$ où la dérivée seconde s'annule ou n'existe pas (pour trouver les lieux potentiels des points d'inflexion).}  

Nous trouvons $f''(x) = -\dfrac{\ln(x)}{4\sqrt{x^3}}$. 

Nous avons  $\dom(f'') = ]0 ,\, \infty[$. Il n'y a donc pas de valeur de $x$ élément du domaine de $f$ telles que $f''(x)~\nexists$. 

De plus, $f''(x) = 0 \Rightarrow -\ln(x)=0 \Rightarrow x=1$. Puisque $x=1$ fait partie du domaine de $f$, il s'agit potentiellement de l'abscisse d'un point d'inflexion. \\ 

\textbf{Étape 5: Faire un tableau de signes.}  
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{1.5cm}|C{1cm}|C{1.2cm}|C{1cm}|C{1.2cm}|C{1cm}|C{1.3cm}|}
\hline
$x$        & $0$ &   & $e^{-2}$ &   & $1$ &        \\ \hline
$f'(x)$ & $\nexists$  & $-$  & $0$  & $+$ & $+$  & $+$   \\ \hline
$f''(x)$   & $\nexists$  & $+$ & $+$  & $+$  & $0$ & $-$   \\ \hline
$f(x)$ & $\nexists$  &  \fdh  & min $-\frac{2}{e}$ & \fch    & pt inf $0$  & \fcb     \\ \hline  
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}


\textbf{Étape 6: Déterminer les zéros et l'ordonnée à l'origine, si nécessaire.}  

La fonction passe par le point $(1,\,0)$. \\
 
\textbf{Étape 7: Tracer le graphique.} 
\begin{center}
\renewcommand{\minX}[0]{-0.5} 
\renewcommand{\maxX}[0]{1}
\renewcommand{\minY}[0]{-1}
\renewcommand{\maxY}[0]{0.5}
\begin{tik}[p3TraceEx1Man44]

\begin{axis}
[
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$f(x)$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={\minY,-0.5, 0.5},
    xtick={-0.5, 0.5, 1},
    ymin=\minY - 0.1,
    ymax=\maxY + 0.1,
    xmin=\minX - 0.1,
    xmax=\maxX + 0.1,
    axis line style={thick},
    height=9cm,
    width=13cm
]

\addplot [very thick, draw=blue, smooth, samples=150, domain=0:1.1] {sqrt(x)*ln(x)};

\node[rectangle, anchor=north, draw] (x) at (0.5, -0.7) {$(e^{-2},\, -\frac{2}{e})$};


%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(0.1353, -0.7356)
};

\draw (x) -- (0.1353, -0.7356);

\node[rectangle, anchor=north, draw] (y) at (0.25, 0.35) {$(0,\,0)$};

%%rond vide
\addplot[color = blue, fill = white, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(0, 0)
};

\draw (y) -- (0,0);

\end{axis}
\end{tik}
\end{center}
}
\end{ex}
\ordi{\newpage}



\begin{ex}
\QS{Faire l'étude complète de la fonction $f(x) = 2x+\arccos(x)$.}{

\textbf{Étape 1: Trouver le domaine de la fonction.} 

Nous obtenons $\dom(f) = [{-1},\, 1]$. \\

\textbf{Étape 2: Déterminer si la fonction possède des asymptotes horizontales et verticales.} 

Il n'y a aucun candidat possible pour les asymptotes verticales. Il n'y a donc pas d'asymptote verticale. 

De plus, à cause du domaine, il n'y a pas d'asymptote horizontale.\\

\textbf{Étape 3: Trouver les points critiques de la fonction (pour trouver les lieux potentiels des extremums relatifs). } 

Nous trouvons $f'(x) = 2-\dfrac{1}{\sqrt{1-x^2}}$. 

Nous avons $\dom(f') = ]{-1} ,\, 1[$, donc $x={-1}$ et $x=1$ sont des valeurs critiques car la dérivée n'existe pas pour ces valeurs.  

Ensuite, $f'(x) = 0 \Rightarrow \dfrac{1}{\sqrt{1-x^2}} = 2 \Rightarrow \sqrt{1-x^2}=\dfrac{1}{2} \Rightarrow 1-x^2=\dfrac{1}{4} \Rightarrow x^2=\dfrac{3}{4} \Rightarrow x= \pm \dfrac{\sqrt{3}}{2}$. 

Comme ces valeurs font partie du domaine de $f$, il s'agit de valeurs critiques de $f$.  \\

\textbf{Étape 4: Trouver les endroits du domaine de $f$ où la dérivée seconde s'annule ou n'existe pas (pour trouver les lieux potentiels des points d'inflexion).}  

Nous trouvons $f''(x) = -\dfrac{x}{\sqrt{(1-x^2)^3}}$. 

Nous avons  $\dom(f'') = ]-1 ,\, 1[$. Donc $f''(-1)$ et $f''(1) ~\nexists$, mais $x={-1}$ et $x=1$ ne peuvent pas être les abscisses de points d'inflexion puisqu'il s'agit des bornes du domaine. 

De plus, $f''(x) = 0 \Rightarrow x=0$. Puisque $x=0$ fait partie du domaine de $f$, il s'agit potentiellement de l'abscisse d'un point d'inflexion. 

\newpage

\textbf{Étape 5: Faire un tableau de signes.} 
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{1.2cm}|C{1.3cm}|C{1cm}|C{1.8cm}|C{1cm}|C{1.3cm}|C{1cm}|C{1.3cm}|C{1cm}|C{1.2cm}|}
\hline
$x$        & ${-1}$ &   & $-\frac{\sqrt{3}}{2}$ &   & $0$ &  &   $\frac{\sqrt{3}}{2}$ &  &  $1$    \\ \hline
$f'(x)$ & $\nexists$  & $-$  & $0$  & $+$ & $+$  & $+$ & $0$ & $-$ & $\nexists$  \\ \hline
$f''(x)$   & $\nexists$  & $+$ & $+$  & $+$  & $0$ & $-$ & $-$ & $-$ & $\nexists$  \\ \hline
$f(x)$ & max rel $\pi-2$  &  \fdh  & min rel $-\sqrt{3}+\frac{5\pi}{6}$ & \fch    & pt inf $\frac{\pi}{2}$  & \fcb & max rel $\sqrt{3}+\frac{\pi}{6}$ & \fdb & min rel \ \ \ $2$     \\ \hline  
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}

\textbf{Étape 6: Déterminer les zéros et l'ordonnée à l'origine, si nécessaire.}  

Pas nécessaire pour cet exemple. \newpage
 
\textbf{Étape 7: Tracer le graphique.}
\begin{center}
\renewcommand{\minX}[0]{-1} 
\renewcommand{\maxX}[0]{1}
\renewcommand{\minY}[0]{-0.5}
\renewcommand{\maxY}[0]{2.7}
\begin{tik}[p3TraceEx2Man]

\begin{axis}
[
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$y$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={
         0, 0.7854, 1.5708, 2.3562
    },
    yticklabels={
         0, $\frac{\pi}{4}$, $\frac{\pi}{2}$, $\frac{3\pi}{4}$
    },
    xtick={
        -1, -0.5, 0, 0.5, 1
    },
    xticklabels={
        ${-1}$, -$\frac{1}{2}$, 0, $\frac{1}{2}$, $1$
    },
    ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.5,
    xmax=\maxX + 0.5,
    axis line style={thick},
    height=9cm,
    width=14cm
]

\addplot [very thick, smooth, draw=blue, samples=200, domain=-1:1] {2*x+rad(acos(x))};

\node[rectangle, anchor=south, draw] (a) at (-1.2, 1.7) {$({-1},\,\pi-2)$};

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(-1,1.1416)
};
\draw (a) -- (-1,1.1416);


\node[rectangle, anchor=south, draw] (b) at (-1, 0.2) {$\left(-\frac{\sqrt{3}}{2},\,\frac{5\pi}{6}-\sqrt{3}\right)$};


\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(-0.866, 0.8859)
};
\draw (b) -- (-0.866, 0.8859);

\node[rectangle, anchor=south, draw] (c) at (0.3, 0.7) {$\left(0,\,\frac{\pi}{2}\right)$};


\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(0, 1.5708)
};
\draw (c) -- (0, 1.5708);


\node[rectangle, anchor=south, draw] (d) at (0.5, 2.4) {$\left(\frac{\sqrt{3}}{2},\,\frac{\pi}{6}+\sqrt{3}\right)$};

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(0.866, 2.2556)
};
\draw (d) -- (0.866, 2.2556);


\node[rectangle, anchor=south, draw] (e) at (1.2, 1) {$\left(1,\,2\right)$};

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(1, 2)
};
\draw (e) -- (1, 2);

\end{axis}
\end{tik}
\end{center}
}
\end{ex}
\end{manuel}


\begin{bnframe}{Exercice \exe}
\QR{Faire l'étude complète de la fonction $f(x) = \dfrac{e^x}{x}$.}{
\begin{center}
\renewcommand{\minX}[0]{-3} 
\renewcommand{\maxX}[0]{3}
\renewcommand{\minY}[0]{-8}
\renewcommand{\maxY}[0]{8}
\begin{tik}[p3TraceEx4BN]

\begin{axis}
[
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$f(x)$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={\minY,-6,...,\maxY},
    xtick={\minX,...,\maxX},
    restrict y to domain=-35:9,
    ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.2,
    xmax=\maxX + 0.2,
    axis line style={thick},
    height=6.5cm,
    width=10cm
]

\addplot [very thick, draw = blue, smooth, domain=-3.2:0] {e^x/x-0.2};

\addplot [very thick, draw = blue, smooth, domain=0:3.2] {e^x/x};

%%%% asymptote
\addplot [very thick, dashed, draw=red] 
coordinates {
		(0,-8.2)
		(0,8.2)
};

\node[red, above] at (0.39,-0.1) {$x=0$};

\node[red, above] at (-2.8,-0.1) {$y=0$};

\draw[red, very thick, dashed] (-3.2, 0) -- (-2,0);

\node[rectangle, anchor=south, draw] (e) at (1.5, 6) {$(1,\, e)$};
 
%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(1, 2.71)
}; 

\draw (e) -- (1, 2.71);

\end{axis}
\end{tik}
\end{center}
} 
\end{bnframe}


\begin{bnframe}{Exercice \exe}
\QR{Faire l'étude complète de la fonction $f(x) = \sin(x)+\cos(x)$ sur $[0, \ \pi]$.}{
\begin{center}
\renewcommand{\minX}[0]{-0.2} 
\renewcommand{\maxX}[0]{3.5}
\renewcommand{\minY}[0]{-1.2}
\renewcommand{\maxY}[0]{2.2}
\begin{tik}[p3TraceEx5BN]

\begin{axis}
[
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$y$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={
                 -1, 1, 1.5708
            },
            yticklabels={
                 -1, 1,  $\sqrt{2}$ 
            },
            xtick={
                 -3.14159, -1.5708, 0.7071, 1.5708, 2.3562, 3.14159
            },
            xticklabels={
                 $-\pi$, $-\frac{\pi}{2}$, $\frac{\pi}{4}$, $\frac{\pi}{2}$, $\frac{3\pi}{4}$, $\pi$
            },
            ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.2,
    xmax=\maxX + 0.2,
    axis line style={thick},
    height=6.5cm,
    width=10cm
]
    
\addplot [smooth, very thick, draw = blue, samples=100, domain=0:3.1416] {sin(deg(x)) + cos(deg(x))};
        
\node[rectangle, anchor=south, draw] (e) at (0.5, 0.2) {$(0,\, 1)$};       

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(0, 1)
}; 

\draw (e) -- (0, 1);


\node[rectangle, anchor=south, draw] (c) at (3.3, 0.2) {$(\pi,\, {-1})$};       

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(3.1416, -1)
}; 

\draw (c) -- (3.1416, -1);


\node[rectangle, anchor=south, draw] (a) at (2.6, 1) {$\left(\frac{3\pi}{4},\, 0\right)$};  

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(2.3562, 0)
}; 

\draw (a) -- (2.3562, 0);



\node[rectangle, anchor=south, draw] (b) at (1.3, 1.7) {$\left(\frac{\pi}{4},\, \sqrt{2}\right)$};

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(0.7854, 1.4142)
};        
        
\draw (b) -- (0.7854, 1.4142);

        
\end{axis}
\end{tik}
\end{center}
} 
\end{bnframe}


\devoirs



\begin{sectionEx}

\Q{Faire l'étude complète des fonctions suivantes pour en tracer le graphe.}
\debutSousQ
\QR{$f(x)=\arctan(e^x)$}{%mettre valign=t comme paramètre du includegraphics
\begin{center}
\renewcommand{\minX}[0]{-5} 
\renewcommand{\maxX}[0]{5}
\renewcommand{\minY}[0]{0}
\renewcommand{\maxY}[0]{2}
\begin{tikzpicture}

\begin{axis}
[
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$f(x)$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={\minY,...,\maxY},
    xtick={-5, -3, ..., 5},
    ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.2,
    xmax=\maxX + 0.2,
    axis line style={thick},
    width=8cm,  
]


\addplot [very thick, smooth, draw=blue, domain=-6:6] {(rad(atan(e^x))-pi/4)/1.07 + pi/4};
\addplot [very thick, dashed, draw=red, domain=-6:6] {0};
\addplot [very thick, dashed, draw=red, domain=-6:6] {pi/2};

\node[red, above] at (3.8,1.55) {$y=\pi/2$};


\node[rectangle, anchor=south, draw] (a) at (3, 0.3) {$\left(0,\,\frac{\pi}{4}\right)$};

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(0,0.7854)
};
\draw (a) -- (0,0.7854);

\end{axis}
\end{tikzpicture}
\end{center}
}

\QR{$f(x)=e^{\frac{1}{x}}$}{%mettre valign=t comme paramètre du includegraphics
\begin{center}
\renewcommand{\minX}[0]{-2} 
\renewcommand{\maxX}[0]{3}
\renewcommand{\minY}[0]{0}
\renewcommand{\maxY}[0]{3}
\begin{tikzpicture}

\begin{axis}
[
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$f(x)$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={\minY,...,\maxY},
    xtick={-2,-1,1, 2, 3},
    ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.2,
    xmax=\maxX + 0.2,
    axis line style={thick},
    width=8cm,    
]

\addplot [very thick, smooth, draw=blue, samples=100, domain=-2.2:-0.01] {exp(1/x)};
\addplot [very thick, smooth, draw=blue, samples=100, domain=0.2:3.2] {exp(1/x)};
\addplot [very thick, dashed, draw=red, domain=-2.2:3.2] {1};
\addplot [very thick, dashed, draw=red] 
coordinates {
		(0,-0.2)
		(0,3.2)
};

\node[red, above] at (2.7,0.95) {$y=1$};

\node[red] at (-0.6,2.5) {$x=0$};

\node[rectangle, anchor=south, draw] (a) at (1.2, 0.2) {$\left(-\frac{1}{2},\, e^{-2}\right)$};

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(-0.5,0.1353)
};
\draw (a) -- (-0.5,0.1353);


%%%rond vide
\addplot[color = blue, fill = white, mark = *, mark size=3pt, only marks, thick]
coordinates {
		(0,0)
};

\end{axis}
\end{tikzpicture}
\end{center}
}

\QR{$f(x)=\dfrac{\cos(x)}{2+\sin(x)}$ sur $[0,\,2\pi]$}{%mettre valign=t comme paramètre du includegraphics
\begin{center}
\renewcommand{\minX}[0]{0} 
\renewcommand{\maxX}[0]{7}
\renewcommand{\minY}[0]{-1}
\renewcommand{\maxY}[0]{1}
\begin{tikzpicture}

\begin{axis}
[
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$f(x)$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={\minY,...,\maxY},
    xtick={
                1.5708, 3.14159, 4.7123889, 6.28318
            },
            xticklabels={
                $\frac{\pi}{2}$, $\pi$, $\frac{3\pi}{2}$, $2\pi$
            },
    ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.2,
    xmax=\maxX + 0.2,
    axis line style={thick},
    width=8cm,   
]

\addplot [very thick, smooth, draw=blue, samples=100, domain=0:7] {cos(deg(x))/(2+(sin(deg(x))))};


\node[rectangle, anchor=south, draw] (a) at (1.52, 0.8) {$\left(\frac{\pi}{2},\, 0\right)$};

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(1.5708,0)
};
\draw (a) -- (1.5708,0);

\node[rectangle, anchor=south, draw] (b) at (5.6, -0.8) {$\left(\frac{3\pi}{2},\, 0\right)$};

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(4.7124,0)
};
\draw (b) -- (4.7124,0);

\node[rectangle, anchor=south, draw] (c) at (1.5, -0.8) {$\left(\frac{7\pi}{6},\, -\frac{1}{\sqrt{3}}\right)$};

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(3.6652,-0.5774)
};
\draw (c) -- (3.6652,-0.5774);

\node[rectangle, anchor=south, draw] (d) at (5, 0.8) {$\left(\frac{11\pi}{6},\, \frac{1}{\sqrt{3}}\right)$};

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(5.7586,0.5774)
};
\draw (d) -- (5.7586,0.5774);

\end{axis}
\end{tikzpicture}
\end{center}
}

\QR{$f(x)=x + \ln(x^2)$ \newline sachant que $\dlim{x \to -\infty}{f(x)}=-\infty$}{%mettre valign=t comme paramètre du includegraphics
\begin{center}
\renewcommand{\minX}[0]{-5} 
\renewcommand{\maxX}[0]{2}
\renewcommand{\minY}[0]{-6}
\renewcommand{\maxY}[0]{3}
\begin{tikzpicture}

\begin{axis}
[
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$f(x)$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={\minY,-4,...,\maxY},
    xtick={-4, -2, 2},
    ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.2,
    xmax=\maxX + 0.2,
    axis line style={thick},
    width=8cm,   
]

\addplot [very thick, smooth, draw=blue, samples=100, domain=-5.2:-.01] {x+ln(x^2)};
\addplot [very thick, smooth, draw=blue, samples=100, domain=0.01:2.2] {x+ln(x^2)};
\addplot [very thick, dashed, draw=red] 
coordinates {
		(0,-6.2)
		(0,3.2)
};

\node[red, right] at (0,3) {$x=0$};


\node[rectangle, anchor=south, draw] (d) at (-3, 1.8) {$\left({-2},\, \ln(4)-2\right)$};

%%%rond plein
\addplot[color = blue, fill = blue, mark = *, mark size=2pt, only marks, thick]
coordinates {
		(-2, -0.6137)
};
\draw (d) -- (-2, -0.6137);

\end{axis}
\end{tikzpicture}
\end{center}
\man{\columnbreak}
}
\finSousQ
\end{sectionEx}

