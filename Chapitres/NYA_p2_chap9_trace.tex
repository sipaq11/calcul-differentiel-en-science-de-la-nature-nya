%%% Copyright (C) 2020 Julie Gendron, Simon Paquette et Jean-Nicolas Pépin
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\chapter{Représentation graphique des fonctions algébriques}
%
%
\section{Croissance et décroissance}
%
%
\begin{bloc}{m}
À la \autoref{refCroissancePoly} de la \autoref{partPoly}, nous avons vu que pour déterminer la \infobulle{croissance et la décroissance}{defCroissanceDecroissance} d'une fonction, il faut étudier le signe de la dérivée première de la fonction. Le tout est résumé au \infobulle{théorème \ref{thmCroissanceDecroissance}}{thmCroissanceDecroissance}. Puisque ce théorème ne dépend pas du type de fonction, nous pouvons l'utiliser pour les fonctions algébriques. 

Nous avons aussi vu que les valeurs de $x$ où la fonction peut potentiellement passer de croissante à décroissante, ou vice versa, sont les \infobulle{valeurs critiques}{defValeurCritique} de la fonction. 

Ainsi, pour déterminer les intervalles de croissance et de décroissance d'une fonction $f$, il suffit de trouver les valeurs critiques de $f$ et de faire un tableau de signes de la dérivée première. 

Il faudra porter une attention particulière au domaine de la fonction et à celui de sa dérivée lors de la recherche des valeurs critiques. Ces étapes étaient faciles dans la partie \ref{partPoly} puisqu'il s'agissait exclusivement de polynômes. 

\begin{exemple}
\QS{Déterminer les intervalles de croissance et de décroissance de la fonction $f(x) = \dfrac{x+1}{x+2}$. 
}{

Nous avons $\dom(f)=\mathds{R}\setminus \{{-2}\}$. 

Pour trouver les valeurs critiques de $f$, nous trouvons sa dérivée. 

Nous obtenons $f'(x) = \dfrac{1}{(x+2)^2}$.  \newpage

Ensuite, $f'(x) \ \nexists \Rightarrow x={-2}$. Puisque ${-2} \not\in \dom(f)$, il ne s'agit pas d'une valeur critique. 

Nous cherchons ensuite les valeurs de $x$ telles que $f'(x)=0$. Il n'y en a pas car, pour qu'une fraction égale zéro, il faut que son numérateur égale $0$ et $1$ n'égale jamais $0$. 

La fonction $f$ n'a donc pas de valeur critique. Nous construisons tout de même le tableau de signes. Nous nous souvenons que les valeurs de $x$ utilisées dans le tableau de signes sont les valeurs critiques et les valeurs qui ne font pas partie du domaine de $f$. 
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{2cm}|C{2cm}|C{1cm}|C{2cm}|}
\hline
$x$ & & ${-2}$ &  \\ \hline
$f'(x)$ & $+$ & $\nexists$ & $+$ \\ \hline
$f(x)$ & $\nearrow$ & $\nexists$ & $ \nearrow$  \\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
Donc, $f$ est croissante sur $]{{-\infty}}, {-2}[$ et sur $]{-2}, \infty[$.
}
\end{exemple}

\dans{mob}{\newpage}
\begin{exemple}
\QS{Déterminer les intervalles de croissance et de décroissance de la fonction\newline $f(x) = \dfrac{x^2-3x+2}{x^2+1}$. 
}{

Comme nous l'avons vu dans l'exemple précédent, il est important de déterminer le domaine de $f$. 

Nous avons $\dom(f) = \mathds{R}$ car le polynôme au dénominateur n'a pas de zéro. 

Nous obtenons $f'(x) = \dfrac{3x^2-2x-3}{(x^2+1)^2}$. 

Ensuite, nous obtenons $\dom(f') = \mathds{R}$. Donc, la dérivée existe partout. 

$\dfrac{3x^2-2x-3}{(x^2+1)^2} = 0 \Rightarrow 3x^2-2x-3 = 0 \Rightarrow x= \dfrac{1-\sqrt{10}}{3}$ ou $x=\dfrac{1+\sqrt{10}}{3}$. \newpage

Nous faisons le tableau de signes. 
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{2cm}|C{2cm}|C{2cm}|C{2cm}|C{2cm}|C{2cm}|}
\hline
$x$ & & $\frac{1-\sqrt{10}}{3}$ &  & $\frac{1+\sqrt{10}}{3}$ &  \\ \hline
$f'(x)$ & $+$ & $0$ & $-$ & $0$ & $+$ \\ \hline
$f(x)$ & $\nearrow$ &  & $\searrow$ &  & $\nearrow$ \\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
Nous concluons que la fonction $f$ est croissante sur $\crochetsof*{{-\infty}, \frac{1-\sqrt{10}}{3}}$ et sur $\crochetsfo*{\frac{1+\sqrt{10}}{3}, \infty}$. 

De plus, $f$ est décroissante sur $\crochets*{\frac{1-\sqrt{10}}{3}, \frac{1+\sqrt{10}}{3}}$.
}
\end{exemple}

\begin{exemple}
\QS{Déterminer les intervalles de croissance et de décroissance de la fonction $f(x)=\sqrt[3]{x^2-4}$. 
}{

Nous avons $\dom(f) = \mathds{R}$. 

Nous devons étudier le signe de $f'$.

Nous trouvons $f'(x)=\displaystyle \frac{1}{3}(x^2-4)^{-\frac{2}{3}}\cdot 2x = \displaystyle \frac{2x}{3(x^2-4)^{\frac{2}{3}}}$.

Ensuite, $f'(x)=0$ si $x=0$ et $f'(x)~ \nexists$ si $x^2-4=0 \Rightarrow x={-2}$ ou $x=2$. Comme $\dom(f) = \mathds{R}$, $x=0$, $x={-2}$ et $x=2$ sont des valeurs critiques de $f$.
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{2cm}|C{1.5cm}|C{1cm}|C{1.5cm}|C{1cm}|C{1.5cm}|C{1cm}|C{1.5cm}|}
\hline
$x$  & & ${-2}$ & &  0 & & 2 &  \\ \hline
$2x$  & $-$ & $-$ & $-$ & $0$ & $+$ & $+$ & $+$  \\ \hline
$3(x^2-4)^{\frac{2}{3}}$  & $+$ & $0$ & $+$ & $+$ & $+$ & $0$ & $+$  \\ \hline
$f'(x)$ & $-$ & $\nexists$ & $-$ & $0$ & $+$ & $\nexists$ & $+$  \\ \hline
$f(x)$ & $\searrow$ &  & $\searrow$ &   & $\nearrow$ &  & $\nearrow$  \\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
Nous trouvons que la fonction $f$ est décroissante sur l'intervalle $]{-\infty}, 0]$ et qu'elle est croissante sur l'intervalle $[0, \infty[$.
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}[\dans{b}{Croissance et décroissance}]

Pour trouver les intervalles de \infobulle{croissance et de décroissance}{defCroissanceDecroissance} d'une fonction $f$, nous utilisons le \infobulle{théorème \ref{thmCroissanceDecroissance}}{thmCroissanceDecroissance}.\dans{b}{\\[0.7cm]\pause}
\begin{liste}[itSep=\dans{b}{0.3cm}\dans{n}{0.1cm}, beaOpt=<+- |alert@+>]
\item Trouver le domaine de $f$. 
\item Trouver les \infobulle{valeurs critiques}{defValeurCritique} de $f$. 
Attention au domaine de $f$ et $f'$. 
\item Faire un tableau de signes de la dérivée première. 
\end{liste}
\end{bloc}%bn
%
%
\subsection{Extremums relatifs}
%
%
\begin{bloc}{m}
Nous avons déjà vu que les \infobulle{extremums relatifs}{defExtremumsRelatifs} d'une fonction $f$ se trouvent potentiellement aux\newline \infobulle{valeurs critiques}{defValeurCritique} de $f$. Pour distinguer les valeurs critiques de $f$ où il y a des extremums relatifs de celles où il n'y en a pas, on peut utiliser le \infobulle{test de la dérivée première}{thmCritereDeriveePremiere}. Il suffit donc de faire un tableau de signes de la dérivée première. 

Une autre option serait d'utiliser le \infobulle{test de la dérivée seconde}{thmCritereDeriveeSeconde}, comme nous le ferons à la \autoref{secConcaviteAlg}.

\begin{exemple}
\QS{Déterminer les extremums relatifs de la fonction $f(x) =3x^{\frac{4}{3}}- 12x^{\frac{1}{3}}$. 
}{

Nous avons $\dom(f) = \mathds{R}$.

Nous trouvons $f'(x) = \dfrac{4(x-1)}{x^{\frac{2}{3}}}$. 

Ensuite, $f'(x) = 0 \Rightarrow x = 1$ et $f'(0) \ \nexists$. Comme $1 \in \dom(f)$ et $0 \in \dom(f)$, $x=1$ et $x=0$ sont deux valeurs critiques de $f$. 
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{2cm}|C{2cm}|C{1cm}|C{2cm}|C{1.5cm}|C{2cm}|}
\hline
$x$ & & 0 & &  1 &   \\ \hline
$4(x-1)$ & $-$ & $-$ & $-$ & $0$ & $+$   \\ \hline
$x^{\frac{2}{3}}$ & $+$ & $0$ & $+$ & $+$ & $+$   \\ \hline
$f'(x)$ & $-$ & $\nexists$ & $-$ & $0$ & $+$   \\ \hline
$f(x)$ & $\searrow$ & $0$ & $\searrow$ &  min. rel. $-9$ & $\nearrow$   \\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
Le minimum relatif de $f$ est ${-9}$ et il est atteint en $x=1$.
}
\end{exemple}

\begin{exemple}
\QS{Déterminer les extremums relatifs de la fonction $f(x) = \dfrac{x^2}{x^4+16}$.}{

Comme $x^4\geq 0$, $\dom(f) = \mathds{R}$. 

Nous trouvons $f'(x) = -\dfrac{2x(x^4-16)}{(x^4+16)^2}$. 

Ensuite, $\dom(f') = \mathds{R}$ et $f'(x) = 0 \Rightarrow x=0$, $x={-2}$ ou $x=2$. Puisque $\dom(f) = \mathds{R}$, ces trois valeurs de $x$ sont des valeurs critiques. 
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{2cm}|C{1.5cm}|C{1.5cm}|C{1.5cm}|C{1.5cm}|C{1.5cm}|C{1.5cm}|C{1.5cm}|}
\hline
$x$ & & ${-2}$ & &  0 & & 2 &  \\ \hline
$-2x$ & $+$ & $+$ & $+$ & $0$ & $-$ & $-$ & $-$  \\ \hline
$x^2-16$ & $+$ & $0$ & $-$ & $-$ & $-$ & $0$ & $+$  \\ \hline
$(x^2+16)^2$ & $+$ & $+$ & $+$ & $+$ & $+$ & $+$ & $+$  \\ \hline
$f'(x)$ & $+$ & $0$ & $-$ & $0$ & $+$ & $0$ & $-$  \\ \hline
$f(x)$ & $\nearrow$ & max. rel. $\frac{1}{8}$ & $\searrow$ & min. rel. $0$ & $\nearrow$ & max. rel. $\frac{1}{8}$ & $\searrow$  \\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
La fonction a un maximum relatif de $\frac{1}{8}$ et il est atteint en $x={-2}$ et en $x=2$. 

La fonction a un minimum relatif de $0$ et il est atteint en $x=0$.  
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}[\dans{b}{Extremums relatifs}]

Pour trouver les \infobulle{extremums relatifs}{defExtremumsRelatifs} d'une fonction $f$ :\dans{b}{\\[0.7cm]\pause} 

\begin{liste}[itSep=\dans{b}{0.3cm}\dans{n}{0.1cm}, beaOpt=<+- |alert@+>]
\item Trouver le domaine de $f$. 
\item Trouver les \infobulle{valeurs critiques}{defValeurCritique} de $f$. 
Attention au domaine de $f$ et $f'$. 
\item Utiliser le \infobulle{test de la dérivée première}{thmCritereDeriveePremiere} ou le\dans{b}{\\} \infobulle{test de la dérivée seconde}{thmCritereDeriveeSeconde}. 
\end{liste}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exercice}

\Q{Déterminer les intervalles de croissance et de décroissance des fonctions suivantes.}
\debutSousQ[itSep=0.3cm]
\QR{$f(x) = (x^{2/3}-1)^2$}{La fonction $f$ est croissante sur $[{-1}, 0]$ et sur $[1, \infty[$. Elle est décroissante sur $]{-\infty}, {-1}]$ et sur $[0, 1]$.} 
\QR{$f(x) = \dfrac{x^2+1}{x+1}$}{La fonction $f$ est croissante sur $]{-\infty}, {-1}-\sqrt{2}]$ et sur $[{-1}+\sqrt{2}, \infty[$. Elle est décroissante sur $[{-1}-\sqrt{2}, {-1}[$ et sur $]{-1}, {-1}+\sqrt{2}]$.} 
\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exercice}

\Q{Déterminer les abscisses des extremums relatifs des fonctions suivantes.}
\debutSousQ[itSep=0.3cm]
\QR{$f(x) = \dfrac{x+1}{x^2+3}$}{La fonction $f$ a un minimum relatif en $x=-3$ et un maximum relatif en $x=1$.}  
\QR{$f(x) = \sqrt[3]{x^2+2x-3}$}{La fonction $f$ a un minimum relatif en $x={-1}$.}  
\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Déterminer les intervalles où $f$ est croissante, les intervalles où $f$ est décroissante ainsi que l'abscisse de tous les points critiques de $f$.}
\debutSousQ
\QR{$f(x)=\sqrt{3x^2-4x+2}$}{Croissante sur $\crochetsfo*{\frac{2}{3}, \infty}$. \newline Décroissante sur $\crochetsof*{{-\infty}, \frac{2}{3}}$. \newline Point critique au point d'abscisse \newline $x = \frac{2}{3}$.}
\QR{$f(x)=\sqrt[3]{x^2+x+1}$}{Décroissante sur $\crochetsof*{{-\infty}, -\frac{1}{2}}$. \newline Croissante sur $\crochetsfo*{-\frac{1}{2}, \infty}$. \newline Point critique au point d'abscisse \newline $x=-\frac{1}{2}$.}
\QR{$f(x)=(x^{\frac{2}{3}}-1)^2$}{Croissante sur $[{-1}, 0]$ et sur $[1, \infty[$. \newline Décroissante sur $]{-\infty}, {-1}]$ et sur $[0, 1]$. \newline Points critiques aux points d'abscisse \newline $x={-1}$, $x=0$ et $x=1$.}
\QR{$f(x)=\dfrac{x}{x^2+2}$}{Croissante sur $[-\sqrt{2}, \sqrt{2}]$. \newline Décroissante sur $]{-\infty}, -\sqrt{2}]$ et sur $[\sqrt{2}, \infty[$. \newline Points critiques aux points d'abscisse \newline $x=-\sqrt{2}$ et $x=\sqrt{2}$.}
\QR{$f(x)=\dfrac{x-2}{(x^2-x+1)^2}$}{Croissante sur $\crochets*{\frac{3-\sqrt{5}}{2}, \frac{3+\sqrt{5}}{2}}$. \newline Décroissante sur $\crochetsof*{{-\infty}, \frac{3-\sqrt{5}}{2}}$ et sur $\crochetsfo*{\frac{3+\sqrt{5}}{2}, \infty}$. \newline Points critiques aux points d'abscisse \newline $x=\frac{3-\sqrt{5}}{2}$ et $x=\frac{3+\sqrt{5}}{2}$.}
\finSousQ

\Q{Utiliser la dérivée donnée pour trouver l'abscisse des extremums relatifs de $f$ en procédant par la méthode de votre choix. Supposer que dans tous les cas, la fonction $f$ est continue en tout point du domaine.} 
\debutSousQ
\QR{$f'(x)=x^2(9-x^2)$}{Minimum relatif au point d'abscisse $x={-3}$. \newline Maximum relatif au point d'abscisse $x=3$.}
\QR{$f'(x)=\dfrac{\sqrt{x+2}-1}{3-x}$}{Minimum relatif au point d'abscisse $x={-1}$. \newline Maximum relatif au point d'abscisse $x=3$.}
\QR{$f'(x)=\dfrac{x+5}{\sqrt[3]{x^2-4}}$}{Minimum relatif aux points d'abscisse \dans{em}{\newline}$x={-5}$ et $x=2$. \newline Maximum relatif au point d'abscisse $x={-2}$.}
\finSousQ

\Q{Trouver l'abscisse des extremums relatifs de $f$ en procédant par la méthode de votre choix.}
\debutSousQ
\QR{$f(x)=\dfrac{8(\sqrt{x}-1)}{x}$}{Aucun minimum relatif. \newline Maximum relatif au point d'abscisse $x=4$.}
\QR{$f(x)=\dfrac{(x+2)^3}{(3x-1)^2}$}{Minimum relatif au point d'abscisse $x=5$. \newline Aucun maximum relatif.}
\QR{$f(x)=4x^{\frac{1}{3}}-x^{\frac{4}{3}}$.}{Aucun minimum relatif. \newline Maximum relatif au point d'abscisse $x=1$.}
\QR{$f(x)=\sqrt{4x^2-1}$}{Minimum relatif aux points d'abscisse \dans{em}{\newline}$x=-\frac{1}{2}$ et $x=\frac{1}{2}$. \newline Aucun maximum relatif.\dans{e}{\newpage}}
\finSousQ
\end{secExercices}
\end{bloc}%eim
%
%
\section{Concavité}   \label{secConcaviteAlg}
%
%
\begin{bloc}{m}
À la \autoref{secConcavitePoly} de la \autoref{partPoly}, nous avons vu que pour déterminer la concavité d'une fonction $f$, il faut étudier le signe de sa dérivée seconde. Le tout est résumé au \infobulle{théorème \ref{thmConcavite}}{thmConcavite}. Puisque ce théorème ne dépend pas du type de fonction, nous pouvons l'utiliser pour les fonctions algébriques. 

Nous avons aussi vu que les \infobulle{points d'inflexion}{defPointInflexion} sont les endroits où la fonction change de concavité et qu'ils se trouvent potentiellement aux endroits où la dérivée seconde s'annule ou n'existe pas. 

Donc, pour déterminer les intervalles de concavité vers le haut et vers le bas d'une fonction $f$ ainsi que ses points d'inflexion, il faut d'abord trouver les valeurs de $x$ pour lesquelles la dérivée seconde s'annule ou n'existe pas. Ensuite, il faut faire un tableau de signes de la dérivée seconde pour distinguer les valeurs qui sont des points d'inflexion de celles qui n'en sont pas.

Ici aussi, il faudra porter une attention particulière au domaine de la fonction et à celui de sa dérivée seconde lors de la recherche des valeurs. 
\end{bloc}%m
%
%
\begin{bloc}{bn}[\dans{b}{Concavité et points d'inflexion}]

Pour déterminer les intervalles de concavité vers le haut et vers le bas d'une fonction $f$ (\infobulle{théorème \ref{thmConcavite}}{thmConcavite}), ainsi que pour trouver les \infobulle{points d'inflexion}{defPointInflexion} de $f$ : \dans{b}{\\[0.7cm]\pause}
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]
\item Trouver le domaine de $f$. 
\item Trouver les endroits où la dérivée seconde s'annule ou n'existe pas. 
Attention au domaine de $f$ et $f''$. 
\item Faire un tableau de signes de la dérivée seconde. 
\end{liste}
\end{bloc}%bn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Déterminer les intervalles de concavité vers le haut et de concavité vers le bas de la fonction $f(x)=2x + 3x^{\frac{1}{3}}$.
}{

Nous avons $\dom(f) = \mathds{R}$. 

Nous cherchons les deux premières dérivées. 

Nous obtenons $f'(x) = 2+\frac{1}{x^{\frac{2}{3}}}$ et $f''(x) = -\frac{2}{3x^{\frac{5}{3}}}$. 

Nous déterminons les valeurs de $x$ pour lesquelles la deuxième dérivée s'annule ou n'existe pas. Il s'agit des endroits où la concavité peut changer. 

Nous concluons que $\dom(f'') = \mathds{R}\setminus \{0\}$. Puisque $0 \in \dom(f)$, $(0, 0)$ peut être un point d'inflexion. De plus, la dérivée seconde ne s'annule jamais.
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{2cm}|C{2cm}|C{1cm}|C{2cm}|}
\hline
$x$ & &  0  & \\ \hline
$f''(x)$ & $+$ & $\nexists$ & $-$   \\ \hline
$f(x)$ & $\cup$ & $0$ &  $\cap$   \\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
La fonction est donc concave vers le haut sur $]{-\infty}, 0]$ et concave vers le bas sur $[0, \infty[$. 
}
\end{exemple}
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Déterminer les points d'inflexion de la fonction $f(x)=\dfrac{x^2-1}{x^2+1}$.
}{

Nous savons que $\dom(f) = \mathds{R}$. 

Nous cherchons les deux premières dérivées. 

Nous obtenons $f'(x) = \dfrac{4x}{(x^2+1)^2}$ et $f''(x) =\dfrac{4-12x^2}{(x^2+1)^3}$. 

Nous déterminons les valeurs de $x$ pour lesquelles la deuxième dérivée s'annule ou n'existe pas. Il s'agit des endroits où la concavité peut changer. 

Nous obtenons $\dom(f'') = \mathds{R}$. 

Ensuite, $f''(x) = 0 \Rightarrow x = \pm \frac{1}{\sqrt{3}}$. Comme ces deux valeurs font partie du domaine de $f$, il s'agit potentiellement des abscisses d'un point d'inflexion. 

Puisque $x^2+1 > 0$, on remarque que $(x^2+1)^3 >0$. Ainsi, le signe de la dérivée seconde dépend seulement de son numérateur.
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{2cm}|C{2cm}|C{1.5cm}|C{2cm}|C{1.5cm}|C{2cm}|}
\hline
$x$ & &  $-\frac{1}{\sqrt{3}}$  &  & $\frac{1}{\sqrt{3}}$ & \\ \hline
$f''(x)$ & $-$ & 0 & + & 0 & $-$ \\ \hline
$f(x)$ & $\cap$ & pt. inf. $ -\frac{1}{2}$ &  $\cup$  & pt. inf. $-\frac{1}{2}$ & $\cap$ \\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
Les points d'inflexion de $f$ sont $(-\frac{1}{\sqrt{3}}, -\frac{1}{2})$ et $(\frac{1}{\sqrt{3}}, -\frac{1}{2})$.
}
\end{exemple}

L'étude de la concavité d'une fonction nous a permis d'obtenir le \infobulle{test de la dérivée seconde}{thmCritereDeriveeSeconde}. Ce test nous est utile pour déterminer les extremums relatifs d'une fonction.
\dans{ordi}{\newpage}

\begin{exemple}
\QS{
Déterminer les extremums relatifs de la fonction $f(x) = \dfrac{x}{(x+2)^2}$.
}{

Nous trouvons d'abord le domaine de $f$, c'est-à-dire $\dom(f) = \mathds{R}\setminus \{-2\}$. 

Ensuite, nous dérivons $f$ pour obtenir $f'(x) = \dfrac{2-x}{(x+2)^3}$. 

Pour trouver les valeurs critiques, nous cherchons les valeurs de $x$ pour lesquelles la dérivée n'existe pas. $\dom(f') = \mathds{R}\setminus \{-2\}$. Puisque $x={-2}$ ne fait pas partie du domaine de $f$, il ne s'agit pas d'une valeur critique. 

De plus, nous déterminons les valeurs de $x$ pour lesquelles la dérivée s'annule. 

$f'(x) = 0 \Rightarrow x=2$. Puisque $2 \in \dom(f)$, $x=2$ est une valeur critique de $f$. 

Finalement, il faut déterminer si $f$ possède un extremum en $x=2$. Nous allons utiliser le test de la dérivée seconde. Il suffit de trouver la dérivée seconde pour ensuite l'évaluer en $x=2$.

$f''(x) = \dfrac{2(x-4)}{(2+x)^4} \Rightarrow f''(2) = -\dfrac{1}{64}$. 

Puisque $f''(2) < 0$, nous concluons que $f$ a un maximum relatif en $x=2$ et que ce maximum est $\frac{1}{8}$. La fonction n'a pas de minimum relatif. 
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}[allowframebreaks]{bn}
\begin{exercice}

\Q{Déterminer les intervalles de concavité vers le haut et vers le bas, ainsi que tous les points d'inflexion, des fonctions suivantes.} 

\debutSousQ[itSep=0.3cm]
\QR{$f(x) = \dfrac{x}{1-x^2}$}{La fonction $f$ est concave vers le haut sur $]{-\infty}, {-1}[$ et sur $[0, 1[$. Elle est concave vers le bas sur $]{-1}, 0]$ et sur $]1, \infty[$. La fonction $f$ a un point d'inflexion en $(0, 0)$.}  
\framebreak
\QR{$f(x) = \dfrac{x^2-1}{\sqrt{x}}$}{La fonction $f$ est concave vers le haut sur $[1, \infty[$. Elle est concave vers le bas sur $]0, 1]$. La fonction a un point d'inflexion en $(1, 0)$.}  
\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Déterminer les intervalles où $f$ est concave vers le haut, les intervalles où $f$ est concave vers le bas, ainsi que l'abscisse des points d'inflexion.}
\debutSousQ
\QR{$f(x)=\sqrt[3]{x^2-9}$}{Concave vers le bas sur $]{-\infty}, {-3}]$ et sur $[3, \infty[$. \newline Concave vers le haut sur $[{-3}, 3]$. \newline Points d'inflexion aux points d'abscisse\newline $x={-3}$ et $x=3$.}
\QR{$f(x)=\dfrac{6x}{x^2+9}$}{Concave vers le bas sur $]{-\infty}, -3\sqrt{3}]$ et sur $[0, 3\sqrt{3}]$. \newline Concave vers le haut sur $[-3\sqrt{3}, 0]$ et sur $[3\sqrt{3}, \infty[$. \newline Points d'inflexion aux points d'abscisse\newline $x=-3\sqrt{3}$, $x=0$ et $x=3\sqrt{3}$.}
\QR{$f(x)={-4}x\sqrt{x+2}$}{Concave vers le bas sur $[{-2}, \infty[$. \newline Aucun point d'inflexion.}
\QR{$f(x)=\dfrac{x^2+1}{x+1}$}{Concave vers le bas sur $]{-\infty}, {-1}[$. \newline Concave vers le haut sur $]{-1}, \infty[$. \newline Aucun point d'inflexion.}
\finSousQ
\end{secExercices}
\end{bloc}%eim
%
%
\section{Tracé des fonctions algébriques}
%
%
\begin{bloc}{bmn}[\dans{b}{Étapes pour trouver une représentation graphique}]
La \infobulle{démarche}{tabMethodeTraceFct} pour le tracé de fonction a été donnée à la \dans{m}{\autoref{secTracePoly}}\dans{bn}{section 5.3} de la \dans{m}{\autoref{partPoly}}\dans{bn}{partie I}. Elle est la même pour les fonctions algébriques.
\end{bloc}%bmn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Trouver la représentation graphique de la fonction $f(x) = x\sqrt{2-x^2}$. 
}{

\textbf{Étape 1: Trouver le domaine de la fonction.} 

Nous obtenons $\dom(f) = [-\sqrt{2}, \sqrt{2}]$. \\ 

\textbf{Étape 2: Déterminer si la fonction possède des asymptotes horizontales et verticales.}  

Puisque le domaine est fini, $f$ ne peut pas avoir d'asymptote horizontale. 

Comme il n'y a pas de discontinuité sur le domaine, $f$ ne peut pas avoir d'asymptote verticale. \\ 

\textbf{Étape 3: Trouver les points critiques de la fonction (pour trouver les lieux potentiels des extremums relatifs).}  

Nous trouvons $f'(x) = -\dfrac{2(x^2-1)}{\sqrt{2-x^2}}$. 

Nous avons $\dom(f') = ]-\sqrt{2}, \sqrt{2}[$. Comme elles font partie du domaine de $f$, $-\sqrt{2}$ et $\sqrt{2}$ sont donc des valeurs critiques de $f$.  

De plus, $f'(x) = 0 \Rightarrow x = \pm 1$. Comme elles font partie du domaine de $f$, $-1$ et $1$ sont donc des valeurs critiques de $f$.  \\ 

\textbf{Étape 4: Trouver les endroits du domaine de $f$ où la dérivée seconde s'annule ou n'existe pas (pour trouver les lieux potentiels des points d'inflexion).}  

Nous trouvons $f''(x) = \dfrac{2x(x^2-3)}{(2-x^2)^{\frac{2}{3}}}$. 

Puisqu'en $x=\sqrt{2}$ il y a une division par zéro, $f''(x) \ \nexists \Rightarrow x= \pm \sqrt{2}$. Comme ces valeurs sont aux extrémités du domaine, elles ne sont pas les abscisses d'un point d'inflexion.

Ensuite $f''(x) = 0 \Rightarrow x=0$, $x=\pm \sqrt{3}$. Puisque $x=\sqrt{3}$ ou $x=-\sqrt{3}$ ne font pas partie du domaine de $f$, on ne considère pas ces valeurs.\\ \dans{ordi}{\newpage}

\textbf{Étape 5: Faire un tableau de signes.}  
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{1.4cm}|C{0.9cm}|C{1.1cm}|C{1.5cm}|C{1.1cm}|C{1.2cm}|C{1.1cm}|C{1.5cm}|C{1.2cm}|C{0.8cm}|}
\hline
$x$     &   $-\sqrt{2}$ &  & ${-1}$ &   & $0$ &   & $1$ &    &  $\sqrt{2}$    \\ \hline
$f'(x)$ & $\nexists$ & $-$  & $0$ & $+$  & $+$  & $+$  & $0$ & $-$ & $\nexists$  \\ \hline
$f''(x)$ & $\nexists$ & $+$ & $+$  & $+$ & $0$  & $-$  & $-$ & $-$ & $\nexists$  \\ \hline
$f(x)$ & $0$ & \fdh & min. rel. $-1$ & \fch   &  pt. inf. $0$ & \fcb   &  max. rel. $1$ &  \fdb  & $0$    \\ \hline  
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}

\textbf{Étape 6: Déterminer les zéros et l'ordonnée à l'origine, si nécessaire.}  

Pas nécessaire pour cet exemple. \\ 
 
\textbf{Étape 7: Tracer le graphique.} 
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_9_ex_trace_1_man.pdf}
\end{center}
}
\end{exemple}
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Faire l'étude complète de la fonction $f(x) = \dfrac{x^2}{x^2+3}$. 
}{

\textbf{Étape 1: Trouver le domaine de la fonction.} 

Nous obtenons $\dom(f) = \mathds{R}$. \\ 

\textbf{Étape 2: Déterminer si la fonction possède des asymptotes horizontales et verticales.} 

Puisqu'il n'y a pas de discontinuité sur le domaine, $f$ ne peut pas avoir d'asymptote verticale.
\begin{alignat*}{3}
& \ \dlim{x \to \infty}{\dfrac{x^2}{x^2+3}} && \hspace{4cm}   \text{F.I. } && \dfrac{\infty}{\infty} \\[0.4cm]
=& \ \dlim{x \to \infty}{\dfrac{x^2}{x^2(1+\frac{3}{x^2})}} \\[0.4cm]
=& \ \dlim{x \to \infty}{\dfrac{1}{1+\frac{3}{x^2}}}   \\[0.4cm]
=& \ 1
\end{alignat*}

Il y a donc une asymptote horizontale d'équation $y=1$ lorsque $x$ tend vers l'infini. 
\begin{alignat*}{3}
& \ \dlim{x \to {-\infty}}{\dfrac{x^2}{x^2+3}} && \hspace{4cm}   \text{F.I. } && \dfrac{\infty}{\infty} \\[0.4cm]
=& \ \dlim{x \to {-\infty}}{\dfrac{x^2}{x^2(1+\frac{3}{x^2})}} \\[0.4cm]
=& \ \dlim{x \to {-\infty}}{\dfrac{1}{1+\frac{3}{x^2}}}   \\[0.4cm]
=& \ 1
\end{alignat*}

Il y a donc une asymptote horizontale d'équation $y=1$ lorsque $x$ tend vers moins l'infini. \newpage

\textbf{Étape 3: Trouver les points critiques de la fonction (pour trouver les lieux potentiels des extremums relatifs).}  

Nous trouvons $f'(x) = \dfrac{6x}{(x^2+3)^2}$. 

La dérivée première existe toujours car $\dom(f') = \mathds{R}$.  

Ensuite, $f'(x) = 0 \Rightarrow x = 0$. Comme $x=0$ fait partie du domaine de $f$, c'est donc une valeur critique de $f$.  \\ 

\textbf{Étape 4: Trouver les endroits du domaine de $f$ où la dérivée seconde s'annule ou n'existe pas (pour trouver les lieux potentiels des points d'inflexion).}  

Nous trouvons $f''(x) = -\dfrac{18(x^2-1)}{(x^2+3)^3}$. 

La dérivée seconde existe toujours car $\dom(f'') = \mathds{R}$. 

De plus, $f''(x) = 0 \Rightarrow x=\pm 1$. Puisque $x = 1$ et $x={-1}$ font partie du domaine de $f$, il y a peut-être un point d'inflexion à ces endroits. \\ 

\textbf{Étape 5: Faire un tableau de signes.}  
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{1.5cm}|C{1.2cm}|C{1.2cm}|C{1.2cm}|C{1.4cm}|C{1.2cm}|C{1.2cm}|C{1.3cm}|}
\hline
$x$      &  & $-1$ &   & $0$ &   & $1$ &        \\ \hline
$f'(x)$ & $-$ & $-$  & $-$ & $0$  & $+$  & $+$  & $+$   \\ \hline
$f''(x)$  & $-$ & $0$  & $+$ & $+$  & $+$  & $0$ & $-$   \\ \hline
$f(x)$ &  \fdb & pt. inf. $\frac{1}{4}$  & \fdh   & min. rel. $0$& \fch   & pt. inf. $\frac{1}{4}$  &  \fcb    \\ \hline  
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}

\textbf{Étape 6: Déterminer les zéros et l'ordonnée à l'origine, si nécessaire.}  

Pas nécessaire pour cet exemple. \newpage
 
\textbf{Étape 7: Tracer le graphique.} 
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_9_ex_trace_2_man.pdf}
\end{center}
}
\end{exemple}
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Faire l'étude complète de la fonction $f(x) = \dfrac{x^3}{x-2}$. 
}{

\textbf{Étape 1: Trouver le domaine de la fonction.} 

Nous obtenons $\dom(f) = \mathds{R}\setminus \{2\}$. \\ 

\textbf{Étape 2: Déterminer si la fonction possède des asymptotes horizontales et verticales.} 

La seule valeur de $x$ possible où il peut y avoir une asymptote verticale est $x=2$ car cette valeur est exclue du domaine.
\begin{alignat*}{3}
& \ \dlim{x \to 2^-}{\dfrac{x^3}{x-2}} && \hspace{4cm}   \text{F.D. } && \dfrac{8}{0^-} \\[0.4cm]
=& \ {-\infty} \\
\intertext{Il y a donc une asymptote verticale d'équation $x=2$. Pour faciliter le tracé du graphe, nous étudions aussi le comportement de la fonction lorsque $x$ se rapproche de $2$ par la droite.}
& \ \dlim{x \to 2^+}{\dfrac{x^3}{x-2}} && \hspace{4cm}   \text{F.D. } && \dfrac{8}{0^+} \\[0.4cm]
=& \ \infty \\
\intertext{Nous étudions ensuite le comportement à l'infini de la fonction.}
& \ \dlim{x \to \infty}{\dfrac{x^3}{x-2}} && \hspace{4cm}   \text{F.I. } && \dfrac{\infty}{\infty} \\[0.4cm]
=& \ \dlim{x \to \infty}{\dfrac{x^3}{x(1+\frac{2}{x})}} \\[0.4cm]
=& \ \dlim{x \to \infty}{\dfrac{x^2}{1+\frac{2}{x}}}   \\[0.4cm]
=& \ \infty \\
\intertext{Il n'y a donc pas d'asymptote horizontale lorsque $x$ tend vers l'infini.} 
& \ \dlim{x \to {-\infty}}{\dfrac{x^3}{x-2}} && \hspace{4cm}   \text{F.I. } && \dfrac{{-\infty}}{{-\infty}} \\[0.4cm]
=& \ \dlim{x \to {-\infty}}{\dfrac{x^3}{x(1+\frac{2}{x})}} \\[0.4cm]
=& \ \dlim{x \to {-\infty}}{\dfrac{x^2}{1+\frac{2}{x}}}   \\[0.4cm]
=& \ \infty
\end{alignat*}

Il n'y a donc pas d'asymptote horizontale lorsque $x$ tend vers moins l'infini.  \\ 

\textbf{Étape 3: Trouver les points critiques de la fonction (pour trouver les lieux potentiels des extremums relatifs).}  

Nous trouvons $f'(x) = \dfrac{2x^2(x-3)}{(x-2)^2}$. 

Nous avons $\dom(f') = \mathds{R}\setminus \{2\}$. Puisque $x=2$ ne fait pas partie du domaine de $f$, il ne s'agit pas d'une valeur critique.  

Ensuite, $f'(x) = 0 \Rightarrow x = 0$ ou $x=3$. Comme ces deux valeurs font partie du domaine de $f$, il s'agit de valeurs critiques de $f$.  \\

\textbf{Étape 4:} Trouver les endroits du domaine de $f$ où la dérivée seconde s'annule ou n'existe pas (pour trouver les lieux potentiels des points d'inflexion).  

Nous trouvons $f''(x) = \dfrac{2x(x^2-6x+12)}{(x-2)^3}$. 

Nous avons  $\dom(f'') = \mathds{R}\setminus \{2\}$. Comme $x=2$ ne fait pas partie du domaine de $f$, il n'y a pas de point d'inflexion d'abscisse $x=2$.

De plus, $f''(x) = 0 \Rightarrow x=0$. Puisque $x=0$ fait partie du domaine de $f$, il s'agit potentiellement de l'abscisse d'un point d'inflexion. \dans{mob}{\\}
\dans{ordi}{\newpage}

\textbf{Étape 5: Faire un tableau de signes.}  
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{1.5cm}|C{1.2cm}|C{1.2cm}|C{1.2cm}|C{1cm}|C{1.2cm}|C{1.4cm}|C{1.3cm}|}
\hline
$x$      &  & $0$ &   & $2$ &   & $3$ &        \\ \hline
$f'(x)$ & $-$ & $0$  & $-$  & $\nexists$  & $-$ &  $0$  & $+$   \\ \hline
$f''(x)$  & $+$ & $0$  & $-$ & $\nexists$  & $+$  & $+$ & $+$   \\ \hline
$f(x)$ &  \fdh & pt. inf. $0$  & \fdb   & $\nexists$ & \fdh   & min. rel. $27$  &  \fch    \\ \hline  
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}

\textbf{Étape 6: Déterminer les zéros et l'ordonnée à l'origine, si nécessaire.}  

Pas nécessaire pour cet exemple. \newpage
 
\textbf{Étape 7: Tracer le graphique.} 
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_9_ex_trace_3_man.pdf}
\end{center}
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}

\begin{exercice}

\QR{Faire l'étude complète de la fonction $f(x) = \dfrac{1}{x^2-9}$.}{
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_9_exerc_trace_1_bn.pdf}
\end{center}
} 
\end{exercice}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exercice}

\QR{Faire l'étude complète de la fonction $f(x) = 1+\dfrac{1}{x}+\dfrac{1}{x^2}$.}{
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_9_exerc_trace_2_bn.pdf}
\end{center}
}  
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{m}
\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?v=7qJSiDpUdS0&index=6&list=PLQhcwa4wrwRpsC-Y-NwaFxb3ydas0bI96}{Étude complète de fonction rationnelle.} 

\href{https://www.youtube.com/watch?v=LiBkYTGBmZk&index=7&list=PLQhcwa4wrwRpsC-Y-NwaFxb3ydas0bI96
}{Étude complète d'une autre fonction rationnelle.} 

\href{https://www.wolframalpha.com}{\textit{WolframAlpha}}
Pour tout savoir (ou presque) sur une fonction, il suffit d'écrire celle-ci.
\end{bloc}%m
%
%
\begin{bloc}{eim}
\begin{secExercices}

\Q{Faire l'étude complète des fonctions suivantes.}
\debutSousQ
\QR{$f(x)=\dfrac{x}{x^2+4}$}{
\includegraphics[scale=1, valign=t]{Tikz/Images/chap_9_ex_sol_trace_1_man.pdf}
}

\QR{$f(x)=\dfrac{x^2}{x^2-1}$}{
\includegraphics[scale=1, valign=t]{Tikz/Images/chap_9_ex_sol_trace_2_man.pdf}
}

\QR{$f(x)=\dfrac{6}{x^2}-\dfrac{6}{x}$}{
\includegraphics[scale=1, valign=t]{Tikz/Images/chap_9_ex_sol_trace_3_man.pdf}
}

\QR{$f(x)=\dfrac{8}{x^2+4}$}{
\includegraphics[scale=1, valign=t]{Tikz/Images/chap_9_ex_sol_trace_4_man.pdf}
}

\QR{$f(x)=(x^2-1)^{\frac{2}{3}}$}{
\includegraphics[scale=1, valign=t]{Tikz/Images/chap_9_ex_sol_trace_5_man.pdf}
}

\QR{$f(x)=x^{\frac{1}{3}}+x^{\frac{4}{3}}$}{
\includegraphics[scale=1, valign=t]{Tikz/Images/chap_9_ex_sol_trace_6_man.pdf}
}
\finSousQ
\end{secExercices}
\end{bloc}%eim

