%%% Copyright (C) 2020 Julie Gendron, Simon Paquette et Jean-Nicolas Pépin
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\chapter{Limites et continuité}
%
%
\begin{bloc}{m}
Dans la première partie, nous avons étudié le concept de limite à l'aide de plusieurs graphiques. Parmi ceux-ci, certains représentaient des fonctions algébriques. Dans ce chapitre, nous allons nous concentrer sur le calcul de limites de façon algébrique. 
\end{bloc}%m
%
%
\section{Limites}
%
%
\subsection{Formes déterminées et indéterminées}
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Évaluer $\dlim{x \to 2}{\frac{x-3}{x+5}}$.}{

D'abord, le domaine de cette fonction est $\mathds{R}\setminus \{{-5}\}$ et comme toute fonction non définie par parties est continue sur son domaine, cette fonction est continue en $x=2$. Nous pouvons donc automatiquement utiliser le fait que $\dlim{x \to a}{f(x)} = f(a)$, ce qui découle du point 3 de la \infobulle{définition de la continuité}{defContinuiteEnUnPoint}.%
\[\dlim{x \to 2}{\frac{x-3}{x+5}} = \frac{2-3}{2+5} = -\frac{1}{7}.\]
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{b}[Notation et signification]
\begin{center}
Que signifie $\dlim{x \to a}{f(x)}=b$?
\end{center}
\end{bloc}%b
%
%
\begin{bloc}{b}[Propriétés]
Nous avons vu les propriétés des limites au \infobulle{théorème \ref{thmLimiteProp}}{thmLimiteProp}.
\end{bloc}%b
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Évaluer $\dlim{x \to 4}{\frac{x-2}{\sqrt{x+5}}}$.}{$\dfrac{2}{3}$}[\vspace*{3cm}]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{b}[Continuité]
Nous avons vu au \infobulle{théorème \ref{thmContinuite}}{thmContinuite} que toutes les fonctions non définies par parties sont continues sur leur domaine.  

De plus, la \infobulle{définition de la continuité en un point}{defContinuiteEnUnPoint} est toujours la même.
\end{bloc}%b
%
%
\begin{bloc}{bmn}
Les limites qui nous causeront plus de problèmes sont donc celles où $x$ tend vers $a$ et où $f$ possède une discontinuité en $x=a$, ainsi que celles où $x$ tend vers $\pm \infty$.
\end{bloc}%bmn
%
%
\begin{bloc}{m}

Les formes $\infty + k$, où $k \in \mathds{R}$, $\infty + \infty$, $\pm \infty \cdot \pm \infty$ et $\frac{k}{\pm \infty}$ sont des formes déterminées que nous avons déjà rencontrées. On se souvient que dans le cas d'une forme déterminée, on peut évaluer la limite sans manipulations algébriques.
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Évaluer $\dlim{x \to \infty}{\paren*{\sqrt{x-3}+7}}$. 
}{
\setlength{\belowdisplayskip}{0pt}%
\begin{align*}
& \ \dlim{x \to \infty}{\paren*{\sqrt{x-3}+7}}   \hspace{2cm} & \text{F.D. } \infty + k \\[0.4cm]
=& \ \infty 
\end{align*}
}
\end{exemple}  

D'autres formes déterminées apparaîtront avec les fonctions algébriques. C'est le cas, entre autres, des formes $0/k$, $0/\infty$ et $\infty/0$.
\dans{mob}{\newpage}

\begin{exemple}
\QS{Évaluer $\dlim{x \to \infty}{\frac{x}{\frac{1}{x}}}$. 
}{
\setlength{\belowdisplayskip}{0pt}%
\begin{align*}
& \ \dlim{x \to \infty}{\frac{x}{\frac{1}{x}}}   \hspace{2cm} & \text{F.D. } \dfrac{\infty}{0} \\[0.4cm]
=& \ \dlim{x \to \infty}{\paren*{x \cdot \frac{x}{1}}}   \\[0.4cm]
=& \ \dlim{x \to \infty}{x^2}   \\[0.4cm]
=& \ \infty 
\end{align*}
}
\end{exemple} 
\dans{ordi}{\newpage}

Voici une situation que nous rencontrerons souvent avec les fonctions rationnelles.
\end{bloc}%m
%

\begin{bloc}{bn}
\begin{exemple}

\Q{Évaluer les limites suivantes.}
\debutSousQ[beaOpt=<+- |alert@+>, itSep=0.3cm]
\QR{$\dlim{x \to \infty}{\frac{\frac{1}{x}}{x}}$}{$0$}[\vfill]
\QR{$\dlim{x \to \infty}{\frac{{-2}}{\sqrt{x+5}}}$}{$0$}[\vfill \newpage]
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\creerInfobulle[12cm]{
\dans{mn}{\begin{table}[!h] 
\caption{Forme $\frac{k}{0}$}  
\label{tabFormek0}} 
\begin{cadre}[\dans{mn}{13cm}\dans{bi}{11cm}]{g}
\begin{center} \textbf{Forme $\dfrac{k}{0}$}\end{center}

La limite $\dlim{x \to a}{\frac{f(x)}{g(x)}}$ a une forme $\dfrac{k}{0}$ si $\dlim{x \to a}{f(x)}=k\neq 0$\dans{b}{\\} et $\dlim{x \to a}{g(x)}=0$. \pause

Dans ce cas, la limite n'existe pas. Il faut évaluer la limite à gauche et la limite à droite pour bien comprendre le comportement de la fonction. 
\end{cadre}
\dans{mn}{\end{table}}
}{tabFormek0}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Évaluer $\dlim{x \to {-2}}{\frac{x^2+x+1}{x+2}}$. 
}{
{\setlength{\belowdisplayskip}{0pt}
\begin{align*}
& \ \dlim{x \to {-2}}{\frac{x^2+x+1}{x+2}}  \hspace{3cm}  & &\text{Forme } \dfrac{k}{0} \\ \vspace*{0.15cm}
\intertext{On évalue donc la limite à gauche et la limite à droite.}
& \ \dlim{x \to {-2}^-}{\frac{x^2+x+1}{x+2}}  &  &\text{F.D.} \dfrac{3}{0^-} \\[0.4cm]
=& \ {-\infty} \\
\shortintertext{Car une constante positive divisée par un très petit nombre négatif donne un très grand nombre négatif.}\vspace*{0.2cm}
& \ \dlim{x \to {-2}^+}{\frac{x^2+x+1}{x+2}}  &  &\text{F.D. } \dfrac{3}{0^+} \\[0.4cm]
=& \ \infty \\
\shortintertext{Car une constante positive divisée par un très petit nombre positif donne un très grand nombre positif.}
\end{align*}
Ainsi, $\dlim{x \to {-2}}{\frac{x^2+x+1}{x+2}}$ $\nexists$, mais nous avons une idée de la façon dont se comporte la fonction autour de $x={-2}$.
}
}
\end{exemple} 
\dans{ordi}{\newpage}
\begin{exemple}
\QS{Évaluer $\dlim{x \to 5}{\frac{x}{x^2-10x+25}}$. 
}{
{\setlength{\belowdisplayskip}{0pt}
\begin{align*}
& \ \dlim{x \to  5}{\frac{x}{x^2-10x+25}}  \hspace{4cm} & & \text{Forme } \dfrac{k}{0}\\ \vspace*{0.15cm}
\intertext{On évalue donc la limite à gauche et la limite à droite.}
& \ \dlim{x \to  5^-}{\frac{x}{x^2-10x+25}}  \hspace{4cm}  & &\text{Forme } \dfrac{5}{0^?} \\[0.4cm]
=& \ \dlim{x \to  5^-}{\frac{x}{(x-5)^2}}  \hspace{4cm}  & &\text{F.D. } \dfrac{5}{0^+} \\[0.4cm]
=& \ \infty \\[0.2cm]
\intertext{Remarquons que dans l'évaluation de cette limite, il était difficile de savoir si le dénominateur allait tendre vers $0^+$ ou $0^-$. Le fait de factoriser le dénominateur élimine cette difficulté. De même,}
& \ \dlim{x \to  5^+}{\frac{x}{(x-5)^2}}  \hspace{4cm} & & \text{F.D. } \dfrac{5}{0^+} \\[0.4cm]
=& \ \infty 
\end{align*}

Ainsi, $\dlim{x \to  5}{\frac{x}{(x-5)^2}}= \infty$. Attention, nous disons tout de même que cette limite n'existe pas.}
}
\end{exemple}  

Voyons maintenant différentes formes indéterminées, ainsi que la manipulation algébrique permettant de lever l'indétermination dans chacun des cas.
\newpage
\end{bloc}%m
%
%
\begin{bloc}{bn}
\begin{exemple}

\Q{Évaluer les limites suivantes.}
\debutSousQ[beaOpt=<+- |alert@+>, itSep=0.3cm] 
\QR{$\dlim{x \to 2}{\frac{3}{2-x}}$}{$\nexists$}[\vfill]
\QR{$\dlim{x \to 2}{\frac{3}{\sqrt{2-x}}}$}{$\infty$}[\vfill \newpage]
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\creerInfobulle[12cm]{
\dans{mn}{
\begin{table}[!h] 
\caption{Forme indéterminée $\frac{0}{0}$}  
\label{tabForme0sur0}} 
\begin{cadre}[\dans{mn}{13cm}\dans{bi}{11cm}]{g}
\begin{center}\textbf{Forme indéterminée $\dfrac{0}{0}$}\end{center}

Voici quelques astuces permettant de lever l'indétermination dans le cas d'une forme $\frac{0}{0}$.
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]
\item Dans le cas d'un quotient de polynômes: factoriser les polynômes et simplifier les facteurs communs.
\item Dans le cas où le numérateur ou le dénominateur contient une racine: multiplier le numérateur et le dénominateur par le conjugué du terme contenant la racine et simplifier.
\item Dans le cas où le numérateur ou le dénominateur est constitué de deux fractions: effectuer l'opération et simplifier. 
\end{liste}
\end{cadre}
\dans{mn}{\end{table}}
}{tabForme0sur0}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Évaluer $\dlim{x \to {-3}}{\frac{x^2+3x}{x^2-9}}$. 
}{
\setlength{\belowdisplayskip}{0pt}%
\begin{align*}
& \ \dlim{x \to {-3}}{\frac{x^2+3x}{x^2-9}}   \hspace{2cm} & \text{F.I. }\dfrac{0}{0} \\[0.4cm]
=& \ \dlim{x \to {-3}}{\frac{x(x+3)}{(x-3)(x+3)}}   \\[0.4cm]
=& \ \dlim{x \to {-3}}{\frac{x}{x-3}}   \\[0.4cm]
=& \ \frac{1}{2}
\end{align*}
}
\end{exemple} 
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Évaluer $\dlim{x \to 1}{\frac{x^3-x^2+x-1}{x^2-x}}$. 
}{

Dans cet exemple de forme indéterminée $\frac{0}{0}$, on constate que la valeur $x=1$ est une racine à la fois du numérateur et du dénominateur puisqu'elle annule chacune des expressions. Ainsi, nous savons que $(x-1)$ est un facteur du numérateur et un facteur du dénominateur. On peut donc utiliser la division polynômiale (voir \ref{divisionPolynomes}) pour factoriser le numérateur et la mise en évidence pour factoriser le dénominateur.
\setlength{\belowdisplayskip}{0pt}%
\begin{align*}
& \ \dlim{x \to 1}{\frac{x^3-x^2+x-1}{x^2-x}}   \hspace{2cm} & \text{F.I. }\dfrac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 1}{\frac{(x-1)(x^2+1)}{x(x-1)}}   \\[0.4cm]
=& \ \dlim{x \to 1}{\frac{x^2+1}{x}} \\[0.4cm]
=& \ 2
\end{align*}
}
\end{exemple}

\begin{exemple}
\QS{Évaluer $\dlim{x \to 2}{\frac{1-\sqrt{x-1}}{x-2}}$. 
}{
\begingroup
\allowdisplaybreaks
\begin{align*}
& \ \dlim{x \to 2}{\frac{1-\sqrt{x-1}}{x-2}}  \hspace{2cm} & \text{F.I. }\dfrac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 2}{\paren*{\frac{1-\sqrt{x-1}}{x-2}\cdot \frac{1+\sqrt{x-1}}{1+\sqrt{x-1}}}} \\[0.4cm]
=& \ \dlim{x \to 2}{\frac{1-(x-1)}{(x-2)(1+\sqrt{x-1})}}  \\[0.4cm]
=& \ \dlim{x \to 2}{\frac{-x+2}{(x-2)(1+\sqrt{x-1})}}  \\[0.4cm]
=& \ \dlim{x \to 2}{\frac{-(x-2)}{(x-2)(1+\sqrt{x-1})}}  \\[0.4cm]
=& \ \dlim{x \to 2}{\frac{-1}{(1+\sqrt{x-1})}}  \\[0.4cm]
=& \ -\frac{1}{2}
\end{align*}
\endgroup
Remarquons d'abord que pour lever l'indétermination de la forme $\frac{0}{0}$, il faut arriver à faire disparaître le terme qui cause problème, dans ce cas-ci $(x-2)$. Il faut donc garder cet objectif en tête. De plus, pour y arriver, nous avons ici dû effectuer une mise en évidence à la suite de l'utilisation de l'astuce.
}
\end{exemple}
\dans{mob}{\newpage}
\begin{exemple}
\QS{Évaluer $\dlim{x \to 3}{\displaystyle \frac{\frac{x+2}{6-x}-\frac{5}{x}}{x-3}}$. 
}{
\setlength{\belowdisplayskip}{0pt}%
\begin{align*}
& \ \dlim{x \to 3}{\displaystyle \frac{\frac{x+2}{6-x}-\frac{5}{x}}{x-3}}   \hspace{2cm} & \text{F.I. }\dfrac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 3}{\paren*{\frac{x(x+2)-5(6-x)}{x(6-x)}\cdot \frac{1}{x-3}}}  \\[0.4cm]
=& \ \dlim{x \to 3}{\frac{x^2+7x-30}{x(6-x)(x-3)}} \hspace{2cm} & \text{F.I. }\dfrac{0}{0}  \\[0.4cm]
=& \ \dlim{x \to 3}{\frac{(x-3)(x+10)}{x(6-x)(x-3)}}   \\[0.4cm]
=& \ \dlim{x \to 3}{\frac{(x+10)}{x(6-x)}}   \\[0.4cm]
=& \ \frac{13}{9}
\end{align*}
}
\end{exemple} 

Il peut arriver qu'on retrouve une forme indéterminée à l'intérieur d'une évaluation de limite. Il peut donc s'avérer utile d'utiliser les \infobulle{propriétés des limites}{thmLimiteProp}.
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Évaluer  $\dlim{x \to 4}{\sqrt{\displaystyle \frac{x^2-4x}{x^2-3x-4}}}$
}{  
\begin{alignat*}{4}
& \ \dlim{x \to  4}{\sqrt{\displaystyle \frac{x^2-4x}{x^2-3x-4}}}   &&                                             &&   \text{Forme } \sqrt{\dfrac{0}{0}} \\[0.3cm]
=& \ \sqrt{ \dlim{x \to  4}{\displaystyle \frac{x^2-4x}{x^2-3x-4}} } \hspace{2cm} && \text{\infobulle{propriété 4 des limites}{thmLimiteProp}} \hspace{2cm}&& \text{F.I. } \dfrac{0}{0}\\[0.3cm]
=& \ \sqrt{ \dlim{x \to  4}{\displaystyle \frac{x(x-4)}{(x-4)(x+1)}} } \\[0.3cm]
=& \ \sqrt{ \dlim{x \to  4}{\displaystyle \frac{x}{(x+1)}} } \\[0.3cm]
=& \ \sqrt{\frac{4}{5}}  \\[0.3cm]
=& \ \frac{2}{\sqrt{5}}
\end{alignat*}
Notons qu'ici nous avons pu utiliser la propriété des limites puisque la fonction racine carrée est continue en $x=\frac{4}{5}$.
}
\end{exemple}
\begin{exemple}
\QS{Évaluer  $\dlim{x \to 3}{f(x)}$ où $f(x) =
\begin{cases}
\displaystyle \frac{x^2-9}{x^2-6x+9} & \text{si } x< 3 \\
10 & \text{si } x= 3 \\
\displaystyle \frac{3x-4}{3-x} & \text{si } x > 3 
\end{cases}$
}{

Comme il s'agit d'une fonction définie par parties et que l'on cherche la limite à la valeur charnière, il faut automatiquement évaluer les limites à gauche et à droite.
\begin{alignat*}{4}
 \dlim{x \to 3^-}{f(x)} =& \ \dlim{x \to 3^-}{\frac{x^2-9}{x^2-6x+9}} \hspace{2cm} && \text{F.I. } && \dfrac{0}{0}  \\[0.4cm]
=& \ \dlim{x \to 3^-}{\frac{(x-3)(x+3)}{(x-3)^2}}  \\[0.4cm]
=& \ \dlim{x \to 3^-}{\frac{(x+3)}{(x-3)}}   \hspace{2cm} && \text{F.D. } && \dfrac{6}{0^-}  \\[0.4cm]
=& \ {-\infty} \\[1cm]
\dlim{x \to 3^+}{f(x)} =& \ \dlim{x \to 3^+}{\frac{3x-4}{3-x}} \hspace{2cm} && \text{F.D. } && \dfrac{5}{0^-}  \\[0.4cm]
=& \ {-\infty}
\end{alignat*}
Ainsi, $\dlim{x \to 3}{f(x)}={-\infty}$.
}
\end{exemple}
\end{bloc}%m
%
%  
\begin{bloc}{bn}
\begin{exemple}

\Q{Évaluer les limites suivantes.}
\debutSousQ[beaOpt=<+- |alert@+>, itSep=0.3cm]  
\QR{$\dlim{x \to 1}{\sqrt{\frac{x-1}{x^2-1}}}$}{$\sqrt{\dfrac{1}{2}}$}[\vfill]
\QR{$\dlim{x \to 5}{\frac{3-\sqrt{4+x}}{5-x}}$}{$\dfrac{1}{6}$}[\vfill \newpage]
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\creerInfobulle[12cm]{
\dans{mn}{
\begin{table}[!h] 
\caption{Forme indéterminée $\frac{\infty}{\infty}$}  
\label{tabFormeinfsurinf}} 
\begin{cadre}[\dans{mn}{13cm}\dans{bi}{11cm}]{g} 
\begin{center}\textbf{Forme indéterminée $\dfrac{\infty}{\infty}$}\end{center}

\onslide<+>{Voici une astuce qui permet de lever l'indétermination dans le cas d'une forme $\frac{\infty}{\infty}$.}

\onslide<+>{Il est souvent possible de lever cette indétermination en faisant une mise en évidence d'un terme qui tend vers l'infini au numérateur et au dénominateur et en simplifiant ensuite l'expression.}

\onslide<+>{Dans le cas où la fonction serait un quotient de polynômes, on mettrait en évidence le terme de plus haut degré au numérateur et au dénominateur avant de simplifier.} 

\onslide<+>{À la partie \dans{bmn}{\ref{partie3}}\dans{i}{III}, nous verrons qu'il est aussi possible de mettre en évidence d'autres types de fonctions.}
\end{cadre}
\dans{mn}{\end{table}}
}{tabFormeinfsurinf}
\dans{ordi}{\newpage}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Évaluer $\dlim{x \to \infty}{\frac{3x^6+2x+1}{5x^6+4x^2}}$.}{
\begin{alignat*}{4}
& \ \dlim{x \to \infty}{\frac{3x^6+2x+1}{5x^6+4x^2}}  \hspace{4cm} && && \text{F.I. } &&\dfrac{\infty}{\infty} \\[0.4cm]
=& \ \dlim{x \to\infty}{\displaystyle \frac{x^6(3+\displaystyle \frac{2}{x^5}+\frac{1}{x^6})}{x^6(5+\displaystyle \frac{4}{x^4})}}   \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3+\displaystyle \frac{2}{x^5}+\frac{1}{x^6}}{5+\displaystyle \frac{4}{x^4}}}   && \text{plusieurs } && \text{F.D. } && \dfrac{k}{\infty}  \\[0.4cm]
=& \ \frac{3+0+0}{5+0} \\[0.4cm]
=& \ \frac{3}{5}
\end{alignat*}
Remarquons qu'ici il n'est pas utile de mettre en évidence les coefficients qui multiplient les termes de plus haut degré.
}
\end{exemple}

L'exemple précédent illustre bien que, dans le cas d'un quotient de polynômes, seulement les termes dominants déterminent le résultat d'une limite à l'infini. 
%%%\begin{bloc}{bimn}[Limite à l'infini d'un quotient de polynômes]
%%%\begin{thm}[Limite à l'infini d'un quotient de polynômes]{thmLimiteinfinirationnelle}
%%%Soit $p(x)=a_nx^n+a_{n-1}x^{n-1}+...+a_1x+a_0$ et \newline $q(x)=b_mx^m+b_{m-1}x^{m-1}+...+b_1x+b_0$, où $a_n$ et $b_m\neq 0$, deux polynômes. Alors on a%
%%%\[\dlim{x \to \pm \infty}{\frac{p(x)}{q(x)}}=\dlim{x \to \pm \infty}{\frac{a_nx^n}{b_mx^m}}\]
%%%\end{thm}
%%%\end{bloc}%bimn
\dans{ordi}{\newpage}
\begin{exemple}
\QS{Évaluer $\dlim{x \to {-\infty}}{\frac{4x^6-16x^2+7}{1-x^4}}$. }{
\begin{alignat*}{3}
& \ \dlim{x \to {-\infty}}{\frac{4x^6-16x^2+7}{1-x^4}} \hspace{4cm} && \text{F.I. }\dfrac{\infty}{\infty} \\[0.3cm]
=& \ \dlim{x \to {-\infty}}{\dfrac{4x^6\paren*{1-\frac{4}{x^4}+\frac{7}{4x^6}}}{-x^4\paren*{-\frac{1}{x^4}+1 }}} \hspace{2cm} \\[0.3cm]
=& \ \dlim{x \to {-\infty}}{\dfrac{-4x^2\paren*{1-\frac{4}{x^4}+\frac{7}{4x^6}}}{\paren*{-\frac{1}{x^4}+1 }}} \hspace{2cm} \\[0.3cm]
=& \ {-\infty}
\end{alignat*}
Notons qu'ici la forme indéterminée était plutôt de la forme $\dfrac{\infty - \infty}{{-\infty}}$. Nous aurions pu d'abord lever l'indétermination au numérateur, cependant l'astuce utilisée permet de lever toutes les indéterminations en même temps.
} 
\end{exemple}

\begin{exemple}
\QS{La concentration $C$ d'un médicament dans le sang $t$ heures après son injection est donnée par la fonction $C(t)=\displaystyle \frac{0,3t}{t^2+4}$. Évaluer $\dlim{t \to \infty}{C(t)}$ et interpréter le résultat selon le contexte.}{
\begin{alignat*}{3}
\dlim{t \to \infty}{C(t)} =& \ \dlim{x \to \infty}{\frac{0,3t}{t^2+4}} \hspace{4cm} && \text{F.I. }\dfrac{\infty}{\infty} \\[0.3cm]
=& \ \dlim{x \to \infty}{\frac{0,3t}{t^2\paren*{1+\frac{4}{t^2} }}}  &&  \\[0.3cm]
=& \ \dlim{x \to \infty}{\frac{0,3}{t\paren*{1+\frac{4}{t^2} }}}  &&  \\[0.25cm]
=& \ 0
\end{alignat*}
À très long terme, la concentration du médicament dans le sang sera nulle. Il n'y aura donc plus de trace du médicament dans le sang.
}
\end{exemple}
\dans{ordi}{\newpage}

Dans le cas de limite à l'infini des fonctions rationnelles, chaque fois que le degré du numérateur est supérieur au degré du dénominateur, le résultat est $\pm \infty$. Chaque fois que le degré du numérateur est inférieur au degré du dénominateur, le résultat est 0.

\begin{exemple}
\QS{Évaluer $\dlim{x \to {-\infty}}{\displaystyle \frac{3x+4}{\sqrt{4x^2-3x+1}}}$. }{
\setlength{\belowdisplayskip}{0pt}%
\begin{align*}
& \ \dlim{x \to {-\infty}}{\displaystyle \frac{3x+4}{\sqrt{4x^2-3x+1}}}  \hspace{2cm} & \text{F.I. }\dfrac{\infty}{\infty} \\[0.4cm]
=& \ \dlim{x \to {-\infty}}{\displaystyle \frac{x\paren*{3+ \frac{4}{x}}}{\sqrt{x^2\paren*{4-\displaystyle\frac{3}{x}+\frac{1}{x^2}}}}}  \\[0.4cm]
=& \ \dlim{x \to {-\infty}}{\displaystyle \frac{x\paren*{3+\displaystyle\frac{4}{x}}}{\sqrt{x^2}\sqrt{\paren*{4-\displaystyle\frac{3}{x}+\frac{1}{x^2}}}}}  \\[0.4cm]
=& \ \dlim{x \to {-\infty}}{\displaystyle \frac{x\paren*{3+\displaystyle\frac{4}{x}}}{-x\sqrt{\paren*{4-\displaystyle\frac{3}{x}+\frac{1}{x^2}}}}}  \hspace{2cm} & \sqrt{x^2}=-x \text{ car }x<0 ~ (x \text{ tend vers }{-\infty}) \\[0.4cm]
=& \ \dlim{x \to {-\infty}}{\displaystyle \frac{-\paren*{3+\displaystyle\frac{4}{x}}}{\sqrt{\paren*{4-\displaystyle \frac{3}{x}+\frac{1}{x^2}}}}}  \\[0.4cm]
=& \ -\frac{3}{\sqrt{4}} \\[0.4cm]
=& \ -\frac{3}{2}
\end{align*}
}
\end{exemple} 
\dans{ordi}{\newpage}
\end{bloc}%m
%
%
\begin{bloc}{bn}
\begin{exemple}

\Q{Évaluer les limites suivantes.}
\debutSousQ[beaOpt=<+- |alert@+>, itSep=0.3cm]
\QR{$\dlim{x \to \infty}{\frac{x^2-1}{x^3-x}}$}{$0$}[\vfill]
\QR{$\dlim{x \to {-\infty}}{\frac{\sqrt{x^2+x-1}}{5-x}}$}{$1$}[\vfill \vfill \newpage]
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\creerInfobulle[12cm]{
\dans{mn}{
\begin{table}[!h] 
\caption{Forme indéterminée $\infty - \infty$}  
\label{tabFormeinfmoinsinf}} 
\begin{cadre}[\dans{mn}{13cm}\dans{bi}{11cm}]{g} 
\begin{center}\textbf{Forme indéterminée $\infty - \infty$}\end{center} 
Voici quelques astuces permettant de lever l'indétermination dans le cas d'une forme $\infty - \infty$. 

\dans{m}{
Nous avons vu dans la première partie une première astuce qui consiste à effectuer une mise en évidence dans le cas d'un polynôme. Voici deux nouvelles astuces qui seront utiles pour les fonctions algébriques.} 
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]
\dans{bin}{\item S'il s'agit d'un polynôme, mettre en évidence la plus grande puissance de $x$.}
\item Multiplier par une fraction constituée du conjugué au numérateur et au dénominateur et simplifier. Par exemple, dans le cas où l'expression contient des radicaux. 
\item Dans le cas d'une somme ou d'une différence de fractions algébriques: mettre sur le même dénominateur, effectuer l'opération et simplifier.
\end{liste}  
\end{cadre}
\dans{mn}{\end{table}}
}{tabFormeinfmoinsinf}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Évaluer $\dlim{x \to \infty}{\paren*{\sqrt{x-2}-\sqrt{x}}}$.}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{4}
& \ \dlim{x \to \infty}{\paren*{\sqrt{x-2}-\sqrt{x}}} \hspace{7cm} && \text{F.I. } && \infty - \infty \\[0.4cm]
=& \ \dlim{x \to \infty}{\paren*{\paren*{\sqrt{x-2}-\sqrt{x}}\cdot \frac{\sqrt{x-2}+\sqrt{x}}{\sqrt{x-2}+\sqrt{x}}}}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{x-2-x}{\sqrt{x-2}+\sqrt{x}}}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{-2}{\sqrt{x-2}+\sqrt{x}}}  && \text{F.D. } && \dfrac{k}{ \infty}  \\[0.4cm]
=& \ 0
\end{alignat*}
}
\end{exemple}
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Évaluer $\dlim{x \to 2^-}{\paren*{\frac{1}{2-x}-\frac{2x}{4-x^2}}}$.}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{4}
& \ \dlim{x \to 2^-}{\paren*{\frac{1}{2-x}-\frac{2x}{(2-x)(2+x)}}} \hspace{4cm} && \text{F.I. } && \dfrac{1}{0^+}-\dfrac{4}{0^+}=\infty - \infty \\[0.4cm]
=& \ \dlim{x \to 2^-}{\paren*{\frac{1}{2-x}-\frac{2x}{(2-x)(2+x)}}} \\[0.4cm]
=& \ \dlim{x \to 2^-}{\frac{2+x-2x}{(2-x)(2+x)}}     \\[0.4cm]
=& \ \dlim{x \to 2^-}{\frac{2-x}{(2-x)(2+x)}} \\[0.4cm]
=& \ \dlim{x \to 2^-}{\frac{1}{(2+x)}} \\[0.4cm]
=& \ \frac{1}{4}
\end{alignat*}
}
\end{exemple}

Il arrive qu'après avoir effectué une manipulation algébrique pour lever une indétermination, on se retrouve devant une autre forme indéterminée. Dans ce cas, il suffit d'appliquer une nouvelle astuce jusqu'à ce qu'on obtienne une forme déterminée.

\begin{exemple}
\QS{Évaluer $\dlim{x \to \infty}{\paren*{\sqrt{x^2+3x}-x}}$.}{
\setlength{\belowdisplayskip}{0pt}%
\begingroup
\allowdisplaybreaks
\begin{alignat*}{4}
& \ \dlim{x \to \infty}{\paren*{\sqrt{x^2+3x}-x}} \hspace{6cm} && \text{F.I. } \infty - \infty \\[0.4cm]
=& \ \dlim{x \to \infty}{\paren*{\paren*{\sqrt{x^2+3x}-x}\cdot \frac{\sqrt{x^2+3x}+x}{\sqrt{x^2+3x}+x}}}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{x^2+3x-x^2}{\sqrt{x^2+3x}+x}}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3x}{\sqrt{x^2+3x}+x}}   && \text{F.I. } \dfrac{\infty}{\infty}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3x}{\sqrt{x^2}\sqrt{1+\frac{3}{x}}+x}}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3x}{x\sqrt{1+\frac{3}{x}}+x}} && \text{car } x>0 \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3x}{x\paren*{\sqrt{1+\frac{3}{x}}+1}}}  \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{3}{\sqrt{1+\frac{3}{x}}+1}}  \\[0.4cm]
=& \ \frac{3}{2}
\end{alignat*}
\endgroup
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Évaluer $\dlim{x \to 2}{\paren*{\frac{1}{x^2-4} - \dfrac{1}{x-2}}}$.}{$\nexists$}[\vspace*{4cm}]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}[allowframebreaks]{bn}
\begin{exercice}

\Q{Évaluer, si possible, les limites suivantes.}
\debutSousQ[itSep=0.3cm, nbCols=2]
\QR{$\dlim{x\to 3}{\frac{x+1}{x^2-1}}$}{$\frac{1}{2}$} 
\QR{$\dlim{x\to \infty}{\dfrac{\frac{1}{x^2-1}}{x+1}}$}{$0$}
\QR{$\dlim{x\to\infty}{\paren*{\sqrt{x+1} - \sqrt{x}}}$}{$0$} \framebreak
\QR{$\dlim{x\to 1}{\dfrac{x+2}{x^2-2x+1}}$}{$\infty$} 
\QR{$\dlim{x\to 1}{\dfrac{x+2}{x-1}}$}{$\nexists$} 
\QR{$\dlim{x\to \infty}{\sqrt[3]{\dfrac{x^3+2x^2-1}{27x^3-x+7}}}$}{$\frac{1}{3}$} \framebreak
\QR{$\dlim{x\to 2}{\dfrac{x^2-x-2}{x^3-4x^2+6x-4}}$}{$\frac{3}{2}$} 
\QR{$\dlim{x\to 4}{\dfrac{x-4}{\frac{7-x}{8-x} - \frac{3}{x}}}$}{$8$}
\finSousQ
\end{exercice}
\dans{n}{\newpage}
\end{bloc}%bn
%
%
\devoirs[message={}, commentaire1=Faire les exercices 1 à 6 de la section \thesection.]
%
%
\begin{bloc}{b}[Activité]
\begin{center}
Activité sur les limites
\end{center}
\end{bloc}%b
%
%
\subsection{Asymptotes horizontales et verticales}
%
%
\begin{bloc}{bimn}
\begin{defi}[Asymptote horizontale]{asympHor}
Une fonction $f$ admet une \defTerme{asymptote horizontale} d'équation $y=b$ si $\dlim{x \to {-\infty}}{f(x)}=b$ \textbf{ou} $\dlim{x \to \infty}{f(x)}=b$, où $b \in \mathds{R}$.\index{Asymptote!horizontale}
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bimn}
\begin{defi}[Asymptote verticale]{asympVer}
Une fonction $f$ admet une \defTerme{asymptote verticale} d'équation\newline $x=a$ si $\dlim{x \to a^-}{f(x)}=\pm \infty$ \textbf{ou} $\dlim{x \to a^+}{f(x)}=\pm \infty$, où $a \in \mathds{R}$. \index{Asymptote!verticale}
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bmn}
Les valeurs de $x$ où il risque d'y avoir des asymptotes verticales sont celles exclues du domaine.\pause{} Dans le cas des fonctions définies par parties, les valeurs charnières sont aussi des endroits où il est possible de trouver des asymptotes verticales.
\end{bloc}%bmn
%
%
\begin{bloc}{m}
\dans{ordi}{\newpage}
\begin{exemple}
\QS{Trouver les asymptotes horizontales et verticales de la fonction $f(x)=\displaystyle \frac{1-x}{x^2-1}$.
}{

Pour les asymptotes horizontales, il faut toujours évaluer les deux limites suivantes.

\begin{alignat*}{4}
& \ \dlim{x \to {-\infty}}{\frac{1-x}{x^2-1}} \hspace{6cm} && \text{F.I. } && \dfrac{\infty}{\infty} \\[0.4cm]
=& \ \dlim{x \to {-\infty}}{\frac{x\paren*{\frac{1}{x}-1}}{x^2\paren*{1-\frac{1}{x^2}}}} \\[0.4cm]
=& \ \dlim{x \to {-\infty}}{\frac{\paren*{\frac{1}{x}-1}}{x\paren*{1-\frac{1}{x^2}}}} && \text{F.D. } && \dfrac{k}{{-\infty}} \\[0.4cm]
=& \ 0 \\[1cm]
& \ \dlim{x \to \infty}{\frac{1-x}{x^2-1}} && \text{F.I. } && \dfrac{{-\infty}}{\infty} \\[0.4cm]
=& \ \dlim{x \to \infty}{\frac{\paren*{\frac{1}{x}-1}}{x\paren*{1-\frac{1}{x^2}}}} && \text{F.D. } && \dfrac{k}{\infty} \\[0.4cm]
=& \ 0
\end{alignat*}

On conclut qu'il y a une asymptote horizontale d'équation $y=0$. Attention, une seule des deux limites devait donner une valeur réelle pour qu'on soit en présence d'une asymptote horizontale. Toutefois, il faut toujours évaluer les deux limites parce qu'on pourrait obtenir deux asymptotes horizontales différentes.

Pour les asymptotes verticales, on étudie d'abord le domaine de la fonction.\newline
$\dom(f)=\mathds{R}\setminus \left\lbrace {-1}, 1 \right\rbrace$. Ainsi, $x={-1}$ et $x=1$ sont des candidats potentiels.

\begin{align*}
& \ \dlim{x \to {-1}^-}{\frac{1-x}{x^2-1}} \hspace{2cm} & \text{F.D. }\dfrac{2}{0^+} \\[0.4cm]
=& \ \dlim{x \to {-1}^-}{\frac{1-x}{(x-1)(x+1)}} \hspace{2cm} & \text{F.D. }\dfrac{2}{0^+} \\[0.4cm]
=& \ \infty
\end{align*}

On peut tout de suite conclure qu'il y a une asymptote verticale d'équation $x={-1}$. Lorsque nous serons à l'étape de la représentation graphique, nous nous intéresserons aussi à la limite quand $x$ tend vers ${-1}$ par la droite pour connaître le comportement de la fonction autour de l'asymptote. 

Vérifions maintenant la présence d'une deuxième asymptote.

\begin{align*}
& \ \dlim{x \to 1^-}{\frac{1-x}{x^2-1}} \hspace{3cm} & \text{F.I. }\dfrac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 1^-}{\frac{-(-1+x)}{(x-1)(x+1)}} \\[0.4cm]
=& \ \dlim{x \to 1^-}{\frac{-1}{x+1}}  \\[0.4cm]
=& \ -\frac{1}{2}
\end{align*}

\begin{align*}
& \ \dlim{x \to 1^+}{\frac{1-x}{x^2-1}} \hspace{3cm} & \text{F.I. }\frac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 1^+}{\frac{-(-1+x)}{(x-1)(x+1)}} \\[0.4cm]
=& \ \dlim{x \to 1^+}{\frac{-1}{x+1}}  \\[0.4cm]
=& \ -\frac{1}{2}
\end{align*}
On conclut ici qu'il n'y a pas d'asymptote verticale en $x=1$.
}
\end{exemple}
\end{bloc}%m
%
%
\devoirs[message={}, commentaire1=Faire l'exercice 7 de la section \thesection.]
%
%
\begin{bloc}{m}
\subsection{Ressources additionnelles}
\href{https://www.youtube.com/watch?time_continue=491&v=Q_6djie5Bp8}{Forme $k/0$.}

\href{https://www.youtube.com/watch?time_continue=438&v=vWmQAq-aKnw}{Forme indéterminée $0/0$ (fonction rationnelle).}

\href{https://www.youtube.com/watch?time_continue=375&v=TOCT107ofRI}{Forme indéterminée $0/0$ (fonction rationnelle avec division euclidienne).}

\href{https://www.youtube.com/watch?v=VbGCw7z-0O4}{Forme indéterminée $0/0$ (racine). }

\href{https://www.youtube.com/watch?v=SHsQ_NM8Fyo}{Forme indéterminée $0/0$ (fraction complexe).}

\href{https://www.youtube.com/watch?v=D0OIHmmirT8}{Forme infini/infini  (fonction rationnelle).}

\href{https://www.youtube.com/watch?v=e8IYj18iyio}{Forme infini/infini (avec racine carrée).}

\href{https://www.youtube.com/watch?v=r_hKgKgClGM&index=3&list=PLR4dIO3FEk8R8AkkeWu0pfmq2imwDbSKD}{Limite infini-infini (avec fractions). }

\href{https://www.youtube.com/watch?v=GdMYVzKcy_M}{Asymptotes verticales.}

\href{https://www.youtube.com/watch?v=ig-NDCYeoOA}{Asymptotes horizontales.}

\href{https://www.wolframalpha.com}{\textit{WolframAlpha}}

Voici deux exemples d'écriture de limites dans un langage que \textit{WolframAlpha} comprend. 

Pour calculer $\dlim{x \to \infty}{3x^2}$, on écrit \og limit 3x\^{}2, x=infinity \fg{}. 

Pour calculer $\dlim{x \to 2^+}{3x^2}$, on écrit \og limit 3x\^{}2, x=2 right \fg{}. 

\href{https://www.wolframalpha.com/examples/mathematics/calculus-and-analysis/}{Consulter cette page pour plus d'exemples d'utilisation de \textit{WolframAlpha}.}
\end{bloc}%m
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Évaluer les limites.}
\debutSousQ
\QR{$\dlim{x \to {-1}^+}{\dfrac{-2}{x+1}}$}{${-\infty}$ donc $\nexists$}
\QR{$\dlim{x \to {-1}^-}{\dfrac{-2}{x+1}}$}{$\infty$ donc $\nexists$} 
\QR{$\dlim{x \to {-1}}{\dfrac{-2}{x+1}}$}{$\nexists$}
\QR{$\dlim{x \to {-2}}{\dfrac{x+2}{x^2+5x+6}}$}{1}
\QR{$\dlim{x \to {-3}}{\dfrac{9-x^2}{x+3}}$}{6}
\QR{$\dlim{x \to 4}{\sqrt{2x+8}}$}{4}
\QR{$\dlim{x \to {-\infty}}{\dfrac{x}{|x|}}$}{${-1}$} 
\QR{$\dlim{x \to 16}{\dfrac{16-x}{\sqrt{x}-4}}$}{${-8}$}
\QR{$\dlim{x \to 1}{\dfrac{2x^3-10x^2-2x+10}{x-1}}$}{${-16}$} 
\QR{$\dlim{x \to 2}{\dfrac{4-x^2}{-x^3-3x^2+4x+12}}$}{$\dfrac{1}{5}$} 
\QR{$\dlim{x \to 1^+}{\dfrac{x-1}{x^3-2x^2+x}}$}{$\infty$ donc $\nexists$} 
\QR{$\dlim{x \to 0}{\paren*{\dfrac{1}{x}-\dfrac{1}{x^2}}}$}{${-\infty}$ donc $\nexists$} 
\QR{$\dlim{x \to 1}{\paren*{\dfrac{\sqrt{x}}{x-1}+\dfrac{1-x}{x^2-2x+1}}}$}{$\dfrac{1}{2}$}
\QR{$\dlim{x \to 5}{\dfrac{x^2-3x-10}{x^2-10x+25}}$}{$\nexists$}
\QR{$\dlim{x \to 1}{\dfrac{x^3+x^2-5x+3}{x^3-3x+2}}$}{$\dfrac{4}{3}$} 
\QR{$\dlim{x \to 4^+}{\dfrac{3-x}{x^2-2x-8}}$}{${-\infty}$ donc $\nexists$} 
\QR{$\dlim{x \to 0^-}{\paren*{\dfrac{1}{x}+\dfrac{1}{x^2}}}$}{$\infty$ donc $\nexists$} 
\QR{$\dlim{x \to 0}{\dfrac{\sqrt{x+4}-2}{x}}$}{$\dfrac{1}{4}$} 
\finSousQ

\Q{Évaluer les limites.}
\debutSousQ
\QR{$\dlim{x \to \infty}{\dfrac{x^2+3x}{x^2+1}}$}{$1$} 
\QR{$\dlim{x \to {-\infty}}{\dfrac{3x^2+2x-1}{-x^3+3x^2+\num{1000}}}$}{$0$} 
\QR{$\dlim{x \to \infty}{\dfrac{5x^3-2x^2}{1-3x}}$}{${-\infty}$ donc $\nexists$} 
\QR{$\dlim{x \to \infty}{\sqrt[5]{\dfrac{32x^2-4x+1}{x^2+x+12}}}$}{$2$} 
\QR{$\dlim{x \to {-\infty}}{\dfrac{\sqrt{x^2+2}}{3x-6}}$}{$-\dfrac{1}{3}$} 
\QR{$\dlim{x \to \infty}{\paren*{\sqrt{x^6+8}-x^3}}$}{$0$} 
\QR{$\dlim{x \to \infty}{\paren*{-x^4+x^2-12}}$}{${-\infty}$ donc $\nexists$} 
\QR{$\dlim{x \to {-\infty}}{\sqrt{4-x}}$}{$\infty$ donc $\nexists$} 
\QR{$\dlim{x \to \infty}{\dfrac{2-x}{\sqrt{7+9x^2}}}$}{$-\dfrac{1}{3}$} 
\finSousQ

\Q{Soit%
\[f(x) =
\begin{cases}
\dfrac{1}{x+2} & \text{si } x < {-2} \\
x^2-5 & \text{si } {-2} < x < 3 \\
\sqrt{x+13} & \text{si }  x > 3 \\
\end{cases}.\]%
Évaluer les limites suivantes.}
\debutSousQ
\QR{$\dlim{x \to {-2}^-}{f(x)}$}{${-\infty}$ donc $\nexists$.} 
\QR{$\dlim{x \to {-2}^+}{f(x)}$}{${-1}$} 
\QR{$\dlim{x \to {-2}}{f(x)}$}{$\nexists$} 
\QR{$\dlim{x \to 0}{f(x)}$}{${-5}$}  
\QR{$\dlim{x \to 3^-}{f(x)}$}{$4$}  
\QR{$\dlim{x \to 3^+}{f(x)}$}{$4$}  
\QR{$\dlim{x \to \infty}{f(x)}$}{$\infty$ donc $\nexists$.}  
\QR{$\dlim{x \to {-\infty}}{f(x)}$}{$0$}   
\finSousQ

\Q{Sachant que $\dlim{x \to a}{f(x)}=3$, $\dlim{x \to a}{g(x)}={-1}$ et $\dlim{x \to a}{h(x)}=0$, évaluer si possible les limites suivantes.}
\debutSousQ
\QR{$\dlim{x \to a}{\paren*{f(x)+3g(x)}}$}{$0$} 
\QR{$\dlim{x \to a}{\paren*{h(x)-4g(x)+1}}$}{$5$} 
\QR{$\dlim{x \to a}{f(x)g(x)}$}{${-3}$} 
\QR{$\dlim{x \to a}{(g(x))^2}$}{$1$} 
\QR{$\dlim{x \to a}{\sqrt[3]{5+f(x)}}$}{$2$} 
\QR{$\dlim{x \to a}{\dfrac{3}{g(x)}}$}{${-3}$} 
\QR{$\dlim{x \to a}{\dfrac{6f(x)}{f(x)+3g(x)}}$}{$\nexists$} 
\finSousQ

\QR{Sachant que%
\[\dfrac{2x^7+x^5-4x+10}{12+x^4+x^7} \leq g(x) \leq \dfrac{\sqrt{4x^2+2x+3}}{6+x}\textrm{,}\]% 
évaluer $\dlim{x \to \infty}{g(x)}$.}{$2$}

\QR{Sachant que%
\[\dfrac{10x^2-35x+30}{x^2+x-6} \leq h(x) \leq \dfrac{x-2}{\sqrt{x^2-3}-1}+\dfrac{1}{2}\textrm{,}\]%
évaluer $\dlim{x \to 2}{h(x)}$.}{$1$}

\dans{m}{\columnbreak}

\Q{Identifier, s'il y en a, les asymptotes horizontales et verticales des fonctions suivantes.}
\debutSousQ
\QR{$f(x)=(x+1)^2(2x-x^2)$}{Il n'y a pas d'asymptote.}
\QR{$f(x)=\dfrac{2x-6}{4-x}$}{Asymptote horizontale : $y={-2}$.\newline Asymptote verticale : $x=4$.} 
\QR{$f(x)=\dfrac{3x^2-8}{x^2-4}$}{Asymptote horizontale : $y=3$. Asymptotes verticales : $x=2$ et $x={-2}$.}[7cm] 
\QR{$f(x)=\dfrac{3(x+1)^2}{(x-1)^2}$}{Asymptote horizontale : $y=3$. Asymptote verticale : $x=1$.} 
\QR{$f(x)=\sqrt[3]{\dfrac{8x+5}{x-8}}$}{Asymptote horizontale : $y=2$. Asymptote verticale : $x=8$.} 
\QR{$f(x)=\dfrac{\sqrt{x^2+2}}{3x-6}$}{Asymptotes horizontales : $y=-\frac{1}{3}$\dans{m}{\newline} où $x<0$ et $y=\frac{1}{3}$ où $x>0$. Asymptote verticale : $x=2$.}[10cm] 
\finSousQ
\end{secExercices}
\end{bloc}%eim
%
%
\section{Continuité}
%
%
\begin{bloc}{bmn}[\dans{b}{Continuité}]
Nous avons vu au \infobulle{théorème \ref{thmContinuite}}{thmContinuite} que toutes les fonctions usuelles non définies par parties sont continues sur leur \infobulle{domaine}{tabCalculsImpossibles}. \pause

De plus, la \infobulle{définition de la continuité en un point}{defContinuiteEnUnPoint} est toujours la même.
\dans{n}{\vspace{1cm}}
\end{bloc}%bmn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Déterminer l'ensemble sur lequel la fonction $f(x)=\displaystyle\frac{x+2}{x^2+5x}$ est continue.}{

Il suffit de trouver le domaine de la fonction $f$.
Ici, la seule opération problème à considérer est la division par 0.%
\[x^2+5x=0 \Rightarrow x(x+5)=0 \Rightarrow x=0 \text{ ou } x={-5}\]%
On obtient donc $\dom (f) = \mathds{R} \setminus \{{-5}, 0\}$.
Ainsi, la fonction $f$ est continue $\forall x \in \mathds{R} \setminus \{{-5}, 0\}$.
}
\end{exemple}

\begin{exemple}
\QS{Déterminer l'ensemble sur lequel la fonction $g(x)=\displaystyle \frac{\sqrt{x}}{1+x^2}$ est continue.}{

Ici, il y a deux opérations problèmes à considérer. D'abord, il y aura une division par zéro si
$1+x^2=0 \Rightarrow x^2={-1}$, ce qui est impossible. De plus, comme il y a une racine paire, on doit avoir $x\geq 0$. La fonction $g$ est donc continue sur $\left[0, \infty\right[$.
}
\end{exemple}
\dans{ordi}{\newpage}
\end{bloc}%m
%                           

\begin{bloc}{bmn}[Continuité des fonctions définies par parties]

Lorsque la fonction est définie par parties, il faut étudier la continuité de chaque branche de la fonction et la continuité à chaque valeur charnière.
\dans{n}{\newpage}
\end{bloc}%bmn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Déterminer l'ensemble sur lequel la fonction suivante est continue.%
\[f(x) =
\begin{cases}
\sqrt{9-x^2} & \text{si } x\leq 2 \\
\displaystyle \frac{x^2-4}{x-2} & \text{si } x > 2 
\end{cases}\]
}{

Il faut d'abord étudier la continuité sur chaque branche.

Le domaine de la fonction $\sqrt{9-x^2}$ est $[{-3}, 3]$, donc la fonction $f$ est continue sur $[{-3}, 2[$.\newline
Le domaine de la fonction $\displaystyle \frac{x^2-4}{x-2}$ est $\mathds{R} \setminus \lbrace 2\rbrace$, donc la fonction $f$ est continue sur $]2, \infty[$.

On étudie ensuite la continuité à la valeur charnière $x=2$.

\begin{liste}
\item $f(2) = \sqrt{9-2^2}=\sqrt{5}$ donc $2 \in \dom(f)$. 

\item 
\begin{alignat*}{4}
\dlim{x \to 2^-}{f(x)} =& \dlim{x \to 2^-}{\sqrt{9-x^2}} = \sqrt{5}  \\[1cm]
\dlim{x \to 2^+}{f(x)} =& \ \dlim{x \to 2^+}{\frac{x^2-4}{x-2}} \hspace{4cm} && \text{F.I. } && 0/0 \\[0.4cm]
=& \ \dlim{x \to 2^+}{\frac{(x-2)(x+2)}{x-2}} \\[0.4cm]
=& \ \dlim{x \to 2^+}{(x+2)} \\[0.4cm]
=& \ 4 
\end{alignat*}
\end{liste}
Ainsi la limite n'existe pas. 

La fonction $f$ est donc discontinue en $x=2$. On en conclut qu'elle est continue sur $[{-3}, \infty[ \setminus \lbrace 2\rbrace$.
}
\end{exemple}
\end{bloc}%m
%

\begin{bloc}{bn}
\begin{exercice}

\Q{Déterminer l'ensemble sur lequel les fonctions suivantes sont continues.}
\debutSousQ[itSep=0.3cm]
\QR{$f(x) = \dfrac{\sqrt{x-2}}{x-5}$}{$[2, \infty[ \, \setminus \, \{5\}$}[\vspace*{3cm}]
\QR{$f(x) = \begin{cases}
3x+3 & \text{si } x < 0 \\
12 & \text{si } x=0 \\
\sqrt{9-x} & \text{si } 0< x \leq 5 \\
\dfrac{3}{x-10} & \text{si } x > 5 \\
\end{cases}$}{$\mathds{R}\setminus \{0, 5, 10\}$}[\vfill] 
\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Déterminer pour quelles valeurs de $x$ la fonction $f$ est continue.}
\debutSousQ
\QR{
$f(x) =
\begin{cases}
\sqrt{x^2-5} & \text{si } x \geq 3 \\
x-1 & \text{si } 1 < x < 3 \\
\dfrac{1}{x} & \text{si }  x \leq 1 \\
\end{cases}$}{$\mathrm{R} \setminus \{0, 1\}$}
\QR{$\displaystyle f(x)=\sqrt{\dfrac{(x+1)(x-2)}{({-3}-x)}}$}{$]{-\infty}, {-3}[\,\cup\, [{-1}, 2] $} 
\QR{$f(x)=\dfrac{-2x^2+x+1}{x^2-3x+2}$}{$\mathrm{R}\setminus\{1, 2\}$} 
\QR{$f(x)=\dfrac{5}{x}+\dfrac{2x}{x+4}$}{$\mathrm{R}\setminus\{{-4}, 0\}$}  
\finSousQ

\Q{Soit la fonction $f$ suivante
\[f(x) =
\begin{cases}
\sqrt[3]{x^2+4}+x & \text{si } x \leq 2 \\
&\\
\dfrac{2k^2x+3}{x-1} & \text{si } 2 < x < 3 \\
&\\
\dfrac{\sqrt{x^2-5}+2x+1}{2} & \text{si } x \geq 3
\end{cases}\]
où $k$ est une constante.}
\debutSousQ
\QR{Trouver une valeur de $k$ telle que la fonction $f$ soit continue en $x=2$.}{$k=\pm \frac{1}{2}$}
\QR{Déterminer si $f$ est alors continue en $x=3$.}{Non}
\QR{Déterminer la valeur qu'il faut donner à $k$ pour que la fonction $f$ soit continue en $x=3$.}{$k=\pm 1$}
\finSousQ
\end{secExercices}
\end{bloc}%eim
