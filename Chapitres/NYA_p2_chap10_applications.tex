%%% Copyright (C) 2020 Julie Gendron, Simon Paquette et Jean-Nicolas Pépin
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\chapter{Applications}


\section{Extremums absolus}


\begin{bloc}{m}
À la \autoref{secExtAbs} de la \autoref{partPoly}, nous avons défini les \infobulle{extremums absolus}{defExtremumsAbsolus}. Le \infobulle{théorème \ref{thmValExetreme}}{thmValExetreme} nous donne des conditions suffisantes pour qu'une fonction possède un maximum et un minimum absolu. Dans le cas où ces conditions sont respectées, le \infobulle{théorème \ref{thmPositionValExt}}{thmPositionValExt} nous indique où se situent les extremums absolus de la fonction. 

Ainsi, pour trouver les extremums absolus d'une fonction continue sur un intervalle $[a, b]$, il suffit de trouver toutes les valeurs critiques de $f$ qui sont dans $]a, b[$. Nous évaluons ensuite la fonction à chacune de ces valeurs critiques, en $x=a$ et en $x=b$. La plus grande valeur de $f$ ainsi calculée est le maximum absolu et la plus petite est le minimum absolu. 

Pour trouver les extremums absolus lorsque la fonction n'est pas continue ou que l'intervalle n'est pas fermé, nous cherchons à déterminer si la fonction peut prendre des valeurs plus grandes ou plus petites que celles trouvées aux valeurs critiques et aux extrémités incluses de l'intervalle. Il faut étudier le comportement de la fonction autour des discontinuités et des extrémités non incluses de l'intervalle. Pour étudier le comportement de la fonction, nous évaluons des limites.
\end{bloc}%m
%
%
\begin{bloc}{bn}[\dans{b}{Extremums absolus}]

Pour trouver les \infobulle{extremums absolus}{defExtremumsAbsolus} d'une fonction $f$ : \pause
\begin{liste}[itSep=\dans{b}{0.3cm}\dans{n}{0.1cm}, beaOpt=<+- |alert@+>]
\item On se demande s'ils existent (\infobulle{théorème \ref{thmValExetreme}}{thmValExetreme}).
\item Si le \infobulle{théorème \ref{thmValExetreme}}{thmValExetreme} nous confirme leur existence, nous les trouvons grâce au \infobulle{théorème \ref{thmPositionValExt}}{thmPositionValExt}, c'est-à-dire, en évaluant la fonction aux valeurs critiques ainsi qu'aux extrémités de l'intervalle. 
\item Sinon, après avoir évalué aux valeurs critiques et aux extrémités incluses, on étudie le comportement de la fonction aux extrémités non incluses de l'intervalle et aux discontinuités de la fonction.
\end{liste}
\end{bloc}%bn
%
%
\begin{bloc}{m}
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Déterminer les extremums absolus de la fonction $f(x) = 1+\dfrac{1}{x}+\dfrac{1}{x^2}$ sur l'intervalle $[{-4}, 1]$. 
}{

La fonction $f$ n'est pas continue en $x=0$ car $\dom(f) = \mathds{R}\setminus \{0\}$. Nous ne savons donc pas si les extremums absolus existent.  

Nous cherchons ensuite les valeurs critiques de $f$. 

Nous trouvons $f'(x) = -\dfrac{x+2}{x^3}$. 

Ainsi, $\dom(f') = \mathds{R}\setminus \{0\}$. Puisque $x=0$ ne fait pas partie du domaine de $f$, il ne s'agit pas d'une valeur critique. 

Ensuite, $f'(x) = 0 \Rightarrow x = -2$. Il y a donc une seule valeur critique et elle se situe dans l'intervalle qui nous intéresse. 

Il faut évaluer la fonction à cette valeur ainsi qu'aux bornes. 
\begin{align*}
f(-4) &= \frac{13}{16} \\
f(1) &= 3 \\
f(-2) &= \frac{3}{4} 
\end{align*}
Il faut ensuite étudier le comportement de $f$ autour de $x=0$ avant de conclure. 
\begin{alignat*}{3}
& \ \dlim{x \to 0^-}{\paren*{1+\frac{1}{x}+\frac{1}{x^2}}} && \hspace{4cm}   \text{F.I. } && {-\infty}+\infty \\[0.4cm]
=& \ \dlim{x \to 0^-}{\dfrac{x^2+x+1}{x^2}} && \hspace{4cm}   \text{F.D. } && \dfrac{1}{0^+} \\[0.4cm]
=& \ \infty\\
\intertext{La fonction n'a donc pas de maximum absolu sur cet intervalle. On peut aussi le vérifier avec la limite quand $x$ s'approche de $0$ par la droite.}
& \ \dlim{x \to 0^+}{\paren*{1+\frac{1}{x}+\frac{1}{x^2}}} && \hspace{4cm}   \text{F.D. } && \infty+\infty \\[0.4cm]
=& \ \infty
\end{alignat*}
Nous concluons donc que le minimum absolu de $f$ sur $[{-4}, 1]$ est $\frac{3}{4}$.
}
\end{exemple}

\begin{exemple}
\QS{Déterminer les extremums absolus de la fonction $f(x) = x-3x^{\frac{1}{3}}$ sur l'intervalle $[{-2}, 2[$. 
}{
Nous avons $\dom(f) = \mathds{R}$. Puisqu'il y a une borne de l'intervalle qui n'est pas incluse, nous ne savons pas si les extremums absolus existent. 

Nous cherchons ensuite les valeurs critiques de $f$. 

Nous trouvons $f'(x) = 1 - \dfrac{1}{x^{\frac{2}{3}}}$. 

Ainsi, $\dom(f') = \mathds{R}\setminus \{0\}$. Puisque $x=0$ fait partie du domaine de $f$, il s'agit d'une valeur critique de $f$. De plus, elle est dans l'intervalle qui nous intéresse. 

Ensuite, $f'(x) = 0 \Rightarrow x = \pm 1$. Ces deux valeurs font partie du domaine de $f$, donc elles sont des valeurs critiques. De plus, elles sont dans l'intervalle qui nous intéresse. 

Il faut évaluer la fonction à ces valeurs critiques ainsi qu'à la borne incluse. 
\begin{align*}
f({-2}) &= -2-3\sqrt[3]{-2} \approx 1,78 \text{ \imaCalcu}\\
f(1) &= -2 \\
f({-1}) &= 2 \\
f(0) &= 0
\end{align*}
Il faut ensuite étudier le comportement de $f$ quand $x$ se rapproche de $2$ par la gauche avant de conclure. 
\begin{alignat*}{3}
& \ \dlim{x \to 2^-}{\paren*{x-3x^{\frac{1}{3}}}} \\[0.4cm]  
=& \ 2-3\sqrt[3]{-2} \approx -1,78 \text{ \imaCalcu}
\end{alignat*}
Puisque ${-2} < 2-3\sqrt[3]{-2} < 2$, la fonction possède un maximum et un minimum absolu. 

Nous concluons donc que le minimum absolu de $f$ sur $[{-2}, 2[$ est ${-2}$ et son maximum absolu sur $[{-2}, 2[$ est $2$.
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}
\begin{exercice}

\Q{Déterminer les extremums absolus des fonctions suivantes sur l'intervalle donné.}
\debutSousQ[itSep=0.3cm]
\QR{$f(x) = x^{\frac{5}{3}} - 5x^{\frac{2}{3}}$ sur $]{-1}, 1]$}{Le maximum absolu de $f$ est $0$ et il est atteint en $x=0$. 

La fonction n'a pas de minimum absolu sur $]{-1}, 1]$. 
} 

\QR{$f(x) = x\sqrt{2-x^2}$ sur $[-\sqrt{2}, \sqrt{2}]$}{Le maximum absolu de $f$ est $1$ et il est atteint en $x=1$. 

Le minimum absolu de $f$ est ${-1}$ et il est atteint en $x={-1}$.
}
\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{eim}
\begin{secExercices}

\Q{Déterminer, s'il y a lieu, les valeurs du maximum et du minimum absolus de $f$ sur l'intervalle donné et situer ces valeurs.}
\debutSousQ
\QR{$f(x)=\sqrt[3]{x^3+1}$ sur $[{-2}, 0]$}{Minimum absolu de $-\sqrt[3]{7}$ en $x={-2}$ et maximum absolu de $1$ en $x = 0$.}[7cm]
\QR{$f(x)=\dfrac{x^2+4}{x-1}$ sur $[{-2}, 6]$}{Aucun extremum absolu.}
\QR{$f(x)=x\sqrt{4x^2-16}$ sur $[{-4}, 4]$}{Minimum absolu de ${-4}\sqrt{48}$ en $x={-4}$ et maximum absolu de $4\sqrt{48}$ en $x=4$.}[7cm]
\QR{$f(x)=\dfrac{(x+1)^2}{(x-1)^2}$ sur $[{-2}, 3]$}{Minimum absolu de 0 en $x={-1}$ et aucun maximum absolu.}
\QR{$f(x)=\dfrac{\sqrt{x^2+1}}{x-1}$ sur $]{-2}, 0[$}{Aucun minimum absolu et maximum absolu de $-\frac{1}{\sqrt{2}}$ en $x = {-1}$.}
\finSousQ

\Q{Déterminer, s'il y a lieu, les valeurs du maximum et du minimum absolus de $f(x)=\dfrac{2x^2}{x^2+x-6}$ sur les intervalles donnés.}
\debutSousQ
\QR{sur $]{-\infty}, \infty[$}{Aucun extremum absolu.}
\QR{sur $]{-3}, 2[$}{Maximum absolu de $0$ en $x=0$ et aucun minimum absolu.}
\QR{sur $[0, \infty[$}{Aucun extremum absolu.}
\QR{sur $]2, \infty[$}{Aucun maximum absolu et minimum absolu de $\frac{48}{25}$ en $x=12$.}
\QR{sur $[4, \infty[$}{Maximum absolu de $\frac{16}{7}$ en $x=4$ et minimum absolu de $\frac{48}{25}$ en $x=12$.}
\QR{sur $[6, \infty[$}{Maximum absolu de $2$ en $x=6$ et minimum absolu de $\frac{48}{25}$ en $x=12$.\dans{m}{\newpage}}
\finSousQ
\end{secExercices}
\end{bloc}%eim
%
%
\section{Optimisation}
%
%
\begin{bloc}{bmn}[Démarche]

À la\dans{b}{ section} \autoref{chapOpti}, nous avons vu une \infobulle{démarche}{tabMethodeOpti} pour résoudre les problèmes d'optimisation. \dans{m}{Revoyons avec un problème en contexte ces différentes étapes de résolution.}
\end{bloc}%bmn
%
%
\begin{bloc}{m}
\begin{exemple} 
\QS{On veut fabriquer une boîte de \qty{108}{dm^3} sans couvercle ayant la forme d'un prisme à base carrée. Quelles sont les dimensions de la boîte permettant d'employer le moins de matériaux possible? 
}{

\textbf{Étape 1: Lire attentivement le problème. Relire au besoin.} \\

\textbf{Étape 2: Nommer les variables. Utiliser un schéma, s'il y a lieu.}  

$A$: aire totale de la boîte (dm\up{2}) \\
$x$: longueur d'un côté du carré formant la base (dm)\\
$h$: hauteur de la boîte (dm)
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_10_ex_boite_man}
\end{center}

\textbf{Étape 3: Établir une formule pour la quantité à optimiser.}  

Nous avons $A(x)=x^2+4xh$. 

\textbf{Étape 4: Au besoin, trouver une relation entre les variables afin que la fonction à optimiser ne dépende que d'une seule variable.}  

Comme le volume de la boîte doit être de \qty{108}{dm^3}, nous trouvons $108=x^2\cdot h \Rightarrow h=\displaystyle \frac{108}{x^2}$.

Donc, en substituant $h$, nous obtenons $A(x)=x^2+\displaystyle \frac{432}{x}$.\\\dans{ordi}{\newpage}

\textbf{Étape 5: Déterminer le domaine de la fonction à optimiser, selon la variable choisie.}  

Puisque $x$ représente une mesure, nous avons $x\geq 0$.

Or, la fonction $A$ n'est pas définie si $x=0$ puisque cela entraînerait une division par zéro.

Donc, nous obtenons $\dom(A) = ]0, \infty[$.\\

\textbf{Étape 6: Utiliser le calcul différentiel pour trouver les extremums cherchés.}  

Nous cherchons les valeurs critiques de $A(x)=x^2+\displaystyle \frac{432}{x}$. 

Nous avons $A'(x)=2x-432x^{-2}=2x-\displaystyle \frac{432}{x^2}=\displaystyle\frac{2x^3-432}{x^2}$. 

Ensuite, $A'(x)=0$ si $x=6$. Comme cette valeur fait partie du domaine de $A$, il s'agit d'une valeur critique.

Aussi, $A'(x) ~\nexists$ si $x=0$. Comme cette valeur ne fait pas partie du domaine de $A$, il ne s'agit pas d'une valeur critique.

Nous calculons $A(6)=108$.

De plus, $\dlim{x \to 0^+}{A(x)}=\infty$ et $\dlim{x \to \infty}{A(x)}=\infty$, alors cela confirme que le minimum est atteint en $x=6$.\\

\textbf{Étape 7: Répondre à la question formulée dans le problème, en tenant compte du contexte. S'assurer que la réponse est plausible.} 

Il faut fabriquer une boîte avec une base carrée de \qty{6}{dm} de côté et une hauteur de \qty{3}{dm} pour obtenir un volume de \qty{32}{dm^3} et une aire totale minimale de \qty{108}{dm^2}. 
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}

\begin{exercice}

\QR{L'administration d'un espace naturel protégé souhaite construire une promenade de bois pour relier le chalet d'interprétation de la nature et l'accueil. Le chalet d'interprétation et l'accueil sont situés directement sur les rivages opposés d'un cours d'eau d'une largeur de 200 mètres, mais ils ne sont pas vis-à-vis. En effet, l'accueil se situe à \qty{2}{km} du point de son rivage qui est le plus près du chalet situé sur le rivage opposé. Si le coût d'installation de la promenade est de \qty{30}{\$} par mètre sur la terre et \qty{90}{\$} par mètre sur l'eau, donner les longueurs de promenade à installer sur l'eau et sur la terre qui permettraient de minimiser les coûts d'installation de la promenade de bois.\ \imaCalcu{}}{Il faut installer $\num{2000}-50\sqrt{2}$ mètres de promenade sur la terre et $\sqrt{\num{45000}}$ mètres de promenade sur l'eau.}
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}{eim}
\begin{secExercices}

\QR{Une antenne de télécommunications de 20 mètres de haut est située à 100 mètres d'un pylône électrique de 40 mètres de haut. Afin de stabiliser ces deux structures, on souhaite attacher le sommet de chacune des structures en un point au sol situé entre elles à l'aide d'un solide fil de fer torsadé. Déterminer la fonction à optimiser, et son domaine, si l'on cherche l'endroit du point d'ancrage qui engendre la plus petite longueur de fil à utiliser.}{$f(x) = \sqrt{20^2+x^2}+\sqrt{40^2+(100-x)^2}$ \newline $\dom(f) = [0, 100]$}

\QR{On désire construire une boîte rectangulaire à base carrée sans couvercle de coût minimal dont le volume est de \qty{864}{cm^3}. Le matériau utilisé coûte \qty{6}{\$/dm^2}. Déterminer les dimensions que devrait avoir cette boîte ainsi que le coût de construction. \imaCalcu{}}{$x$: mesure d'un côté de la base en cm\newline $A$: aire de la boîte en cm$^2$\newline $A(x)=x^2+\frac{\num{3456}}{x}$\newline $\dom(A)=]0, \infty[$\newline La boîte devrait être de \qty{12}{cm} de côté sur \qty{6}{cm} de hauteur et le coût serait alors de \qty{25,92}{\$}.}[10cm]

\QR{Soit la parabole d'équation $f(x)=4-x^2$. Donner les coordonnées des points de cette parabole qui sont les plus proches du point $(0, 2)$.}{$d$ : distance entre un point sur la parabole et le point $(0, 2)$ \newline $d(x) = \sqrt{(4-x^2-2)^2+(x-0)^2}$ \newline $\dom(d) = ]{-\infty}, \infty[$ \newline Les deux points les plus proches du point $(0, 2)$ sont $\paren*{\sqrt{\frac{3}{2}}, \frac{5}{2}}$ et $\paren*{-\sqrt{\frac{3}{2}}, \frac{5}{2}}$.}[10cm]

\QR{Les fonctions $f(x)=\sqrt{2x-3}+1$ et $g(x)=\frac{2x}{3}$ se croisent en deux points. Trouver à quel endroit entre ces deux points la distance verticale entre les deux fonctions est maximale.}{La distance est maximale en $x=\frac{21}{8}$.}[10cm]

\QR{Carl désire construire une piscine creusée rectangulaire d'aire maximale dont un des côtés sera adjacent au terrain du voisin. La ville exige un trottoir d'au moins \qty{80}{cm} de largeur sur les côtés adjacents au terrain du propriétaire et de \qty{120}{cm} de largeur sur les côtés adjacents aux voisins. Sachant que l'aire totale de l'espace piscine de Carl (incluant les trottoirs) est de \qty{30}{m^2}, déterminer les dimensions de la piscine de plus grande aire qu'il pourrait construire.  \imaCalcu \columnbreak}{$x$ : longueur du côté de l'espace piscine non adjacent au terrain du voisin en cm \newline $A$ : aire de la piscine en cm$^2$ \newline $A(x) = (x-200)\paren*{\frac{\num{300000}}{x}-160}$ \newline $\dom(A) = [200, \num{1875}]$ \newline Le côté de la piscine adjacent au terrain du voisin devrait mesurer  \qty[parse-numbers=false]{(200\sqrt{6}-160)}{cm} et l'autre côté \qty[parse-numbers=false]{(250\sqrt{6}-200)}{cm}.}[12cm]

\QR{Une compagnie de location de bateaux souhaite inciter ses clients à ralentir sur l'eau. Elle ajoute donc des frais de \qty[parse-numbers=false]{\frac{v}{50}}{\$/km}, où $v$ est la vitesse moyenne du bateau durant la période de location, à son tarif actuel qui est constitué des frais fixes de \qty{50}{\$} et de frais de \qty{30}{\$} par heure de location. Déterminer la vitesse moyenne qui permettrait d'obtenir les coûts les plus faibles pour un trajet en bateau de \qty{25}{km}.}{$v$: la vitesse moyenne du bateau en km/h \newline $c$: le coût de la location pour une balade de \qty{25}{km} \newline $C(v)=50 + 30 \cdot \frac{25}{v}+ 25 \cdot \frac{v}{50}$ \newline $\dom(C)=]0, \infty[$\newline Pour les coûts de la location les plus bas, la vitesse moyenne du bateau devrait être de \qty[parse-numbers=false]{10\sqrt{15}}{km/h}, soit environ \qty{39}{km/h}. Le coût total serait alors d'environ \qty{88,73}{\$}.}[10cm]

\QR{Un arbre est planté au centre d'une boîte ayant la forme d'un prisme à base carrée de \qty{2}{m} de côté et \qty{1}{m} de hauteur. On veut placer une échelle qui partira du sol et qui sera appuyée à la fois sur l'arête de la boîte et sur le tronc de l'arbre. Déterminer la longueur minimale que devra avoir cette échelle. On néglige ici le diamètre du tronc de l'arbre.}{$x$: la distance entre le pied de l'échelle et le point de contact sur la boîte en m \newline $L$: la longueur de l'échelle en m \newline $L(x)=x+\frac{x}{\sqrt{x^2-1}}$ \newline $\dom(L)=]1, \infty[$ \newline La longueur minimale de l'échelle est de \qty[parse-numbers=false]{2\sqrt{2}}{m}.}[10cm]
\end{secExercices}
\end{bloc}%eim
%
%
\section{Taux liés}
%
%
\begin{bloc}{m}
Tout comme à la \autoref{secTauxLies}, nous cherchons maintenant à déterminer un taux de variation instantané à partir d'une équation qui lie des variables dépendantes d'une même variable indépendante.

\begin{exemple}
\QS{Supposons que $x$ et $y$ sont des fonctions de $t$ liées par l'équation $2y=4-\sqrt{x+y^2}$.

Sachant que $\displaystyle \frac{\textrm{d}x}{\textrm{d}t}=1$, évaluer $\displaystyle \frac{\textrm{d}y}{\textrm{d}t}$ lorsque $y=0$.
}{

\setlength{\belowdisplayskip}{0pt}%
Il faut d'abord dériver les deux côtés de l'équation par rapport à la variable $t$.
\begin{align*}
2\frac{\textrm{d}y}{\dd t}=& \ \frac{-1}{2\sqrt{x+y^2}}\frac{\dd }{\dd t}\paren*{x+y^2}\\[0.4cm]
2\frac{\dd y}{\dd t}=& \ \frac{-1}{2\sqrt{x+y^2}}\paren*{\frac{\dd x}{\dd t}+2y\frac{\dd y}{\dd t}}\\[0.3cm]
\intertext{Ensuite, lorsque $y=0$, on a $0=4-\sqrt{x}\Rightarrow x=16$. Donc, en remplaçant $y$ par 0, $x$ par 16 et $\displaystyle \frac{\dd x}{\dd t}$ par 1, on obtient}
2\left. \frac{\dd y}{\dd t}\right|_{y=0} =& \ \frac{-1}{2\cdot 4}\paren*{1+0} \\[0.4cm]
\Rightarrow \left. \frac{\dd y}{\dd t}\right|_{y=0}=& \  -\frac{1}{16} \text{ .}
\end{align*}
}

\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bmn}[Démarche]

La \infobulle{démarche}{tabMethodeTauxLies} pour la résolution de problèmes de taux liés en contexte est la même que celle vue dans la\dans{b}{ section} \autoref{secTauxLies}.

\dans{n}{\vspace*{1cm}}
\end{bloc}%bmn
%
%
\begin{bloc}{m}
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Un certain type de conifère pousse en forme de cône. La relation entre le rayon de la base du cône et la hauteur du cône est donnée par $r = 0,3h^{\frac{4}{3}}$. De plus, la hauteur du cône en fonction du temps est donnée par $h(t) = \frac{8t^2}{100+t^2}$. À quel rythme le diamètre du cône d'un de ces conifères âgé de six ans croît-il? \imaCalcu{}
}{

\textbf{Étape 1:  Lire attentivement le problème. Relire au besoin.} \\

\textbf{Étape 2:  Nommer les variables et déterminer tous les taux connus, ainsi que le taux cherché.}

$d$: le diamètre du cône formé par l'arbre (m) \\
$h$: la hauteur du cône (m)\\
$t$: temps écoulé (années)

On cherche $\left. \displaystyle \frac{\dd d}{\dd t}\right|_{t=6}$. 

Ici, $\left. \displaystyle \frac{\dd h}{\dd t}\right|_{t=6}$ n'est pas donné directement, mais on peut le trouver à partir de l'équation de $h(t)$.

$\displaystyle \frac{\dd h}{\dd t}=\frac{16t(100+t^2)-2t(8t^2)}{(100+t^2)^2}$ ~~~(\infobulle{théorème \ref{thmDeriveeQuotient}}{thmDeriveeQuotient})

Et donc $\left. \displaystyle \frac{\dd h}{\dd t}\right|_{t=6}=\displaystyle \frac{96\cdot 136-12\cdot 288}{136^2}=\displaystyle \frac{\num{9600}}{\num{18496}}=\frac{150}{289}$. \\

\textbf{Étape 3:  Tracer un schéma (pour les problèmes de nature géométrique).} 
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_10_ex_arbre_man}
\end{center}

\newpage

\textbf{Étape 4:  Trouver le(s) lien(s) entre les variables.} 

On a $d=2r=0,6h^{\frac{4}{3}}$.   \\

\textbf{Étape 5:  Utiliser le calcul différentiel pour obtenir les taux de variation décrits à l'étape 2.}  

On dérive de chaque côté par rapport à la variable $t$.
\begin{align*}
 & \frac{\dd d}{\dd t} = \ 0,6\cdot\frac{4}{3}h^{\frac{1}{3}}\cdot\frac{\dd h}{\dd t} \\[0.4cm]
\Rightarrow& \frac{\dd d}{\dd t}  = \ 0,8\cdot h^{\frac{1}{3}}\cdot\frac{\dd h}{\dd t} \\
\intertext{On veut évaluer cette dérivée en $t=6$. Or, $h(6)=\frac{36}{7}$.}
\left. \frac{\dd d}{\dd t}\right|_{t=6} =& \ \ 0,8\cdot \frac{36}{7}^{\frac{1}{3}}\cdot\frac{150}{289} \\[0.4cm]
\approx& \ \qty{0,7167}{\text{m/année}} 
\end{align*}
\textbf{Étape 6:  Conclure et interpréter le résultat en tenant compte du contexte.}  

Lorsque l'arbre est âgé de 6 ans, son diamètre augmente d'environ \num{0,7167} mètre par année.
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}
\begin{exercice}

\QR{Pour simplifier l'étude d'un circuit électrique en parallèle, on peut modéliser l'ensemble des résistances à l'aide d'une seule résistance équivalente à la résistance de l'ensemble du circuit. Pour un circuit parallèle à trois résistances $R_1$, $R_2$ et $R_3$, la résistance $R_e$ équivalente est donnée par 
\[\frac{1}{R_e}=\frac{1}{R_1}+\frac{1}{R_2}+\frac{1}{R_3} \text{ .}\]
Si la résistance $R_1$ augmente de \qty{2}{\Omega /min}, la résistance $R_2$ augmente de \qty{3}{\Omega /min} et la résistance $R_3$ diminue de \qty{1}{\Omega /min}, à quel rythme varie la résistance $R_e$ lorsque $R_1=15$, $R_2=30$ et $R_3=30$?}{$R_e$ varie au rythme de \qty[parse-numbers=false]{\frac{5}{8}}{\Omega /min}.}
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{eim}
\begin{secExercices}
\QR{Soit $\sqrt{x^3+8}=y^2$. Déterminer $\dfrac{\textrm{d}x}{\textrm{d}t}$ quand $(x, y)=(2, 2)$ sachant qu'à ce moment $\dfrac{\textrm{d}y}{\textrm{d}t}=12$.}{On obtient $\left. \dfrac{\textrm{d}x}{\textrm{d}t} \right|_{(x, y)=(2, 2)}=32$.}

\QR{Une solution concentrée à \qty{0,25}{g/L} contient \qty{5}{g} de soluté. Si on augmente la quantité de liquide à un rythme de \qty{0,2}{L/s}, déterminer de quelle façon variera la concentration de la solution au moment précis où elle sera de \qty{25}{L}.}{À ce moment précis, la concentration diminuera de \qty[parse-numbers=false]{\frac{1}{25^2}=0,0016}{g/L} par seconde.}[12cm]

\QR{Hélène se dirige vers un édifice à une vitesse de \qty{1}{m/s}. Elle passe devant une lumière au sol qui projète son ombre sur l'édifice. Cette lumière est située à dix mètres de l'édifice. Sachant qu'Hélène mesure 1,5 mètre, déterminer à quelle vitesse variera la longueur de son ombre deux secondes après être passée vis-à-vis de la lumière.}{Deux secondes après être passée vis-à-vis de la lumière, la longueur de son ombre diminue de \qty[parse-numbers=false]{\frac{15}{4}}{m/s}.}[10cm]

\QR{La loi des gaz parfaits est donnée par $PV=nRT$ où $P$ représente la pression en kPa, $V$ représente le volume en L, $n$ représente la quantité de gaz en mol, $R$ représente la constante universelle des gaz parfaits, $T$ représente la température absolue en K.
Déterminer de quelle façon varie le volume de \qty{4}{mol} de gaz à \qty{291}{^{\circ} K} lorsque la pression atteint \qty{140}{kPa} si on diminue la pression de \qty{2}{kPa} par seconde.}{Lorsque la pression atteint \qty{140}{kPa}, le volume du gaz augmente de \qty[parse-numbers=false]{\frac{582\cdot R}{70^2}}{L/s}.}[10cm]
\end{secExercices}
\end{bloc}%eim
%
%
\section{Approximation linéaire}
%
%
\begin{bloc}{m}
Les nombres rationnels et les racines de nombres qui ne sont pas une puissance de nombre entier sont difficiles à évaluer sans outil. On peut toutefois les approximer en utilisant la droite tangente ou la \infobulle{différentielle}{defDiff}, comme nous l'avons fait à la \autoref{secApproxLin}.

\begin{exemple}
\QS{Approximer la valeur de $\sqrt{37}$.
}{

Si nous utilisons la \textbf{droite tangente} pour faire l'approximation : 

Soit la fonction $f(x)=\sqrt{x}$. Nous savons que $\sqrt{36}=6$. Nous pouvons donc utiliser la tangente à la courbe en $x=36$ pour approximer la valeur de $f(37)$.

Nous trouvons la dérivée pour obtenir la pente de la tangente. Nous trouvons $f'(x) = \dfrac{1}{2\sqrt{x}}$. Donc, la pente de la tangente en $x=36$ est $f'(36) = \dfrac{1}{2\sqrt{36}} = \dfrac{1}{12}$. 

À l'aide du point de tangence $(36, 6)$, nous pouvons trouver l'ordonnée à l'origine de la droite tangente en $x=36$. 

$6 = \dfrac{1}{12}\cdot 36 + b \Rightarrow b=3$. 

Nous avons donc $g(x) = \dfrac{x}{12} + 3$ comme équation de droite tangente. Ainsi, nous pouvons utiliser cette équation pour faire l'approximation.%
\[f(37) \approx g(37) = \frac{37}{12}+3 = \frac{73}{12}.\]
Si nous utilisons la \textbf{différentielle} pour faire l'approximation : 

Soit la fonction $y=f(x) = \sqrt{x}$. Nous savons que $\sqrt{36}=6$, alors nous utilisons $x=36$ et $\dd x=1$.

Nous trouvons la dérivée pour obtenir la différentielle de $y$. Nous trouvons $f'(x) = \dfrac{1}{2\sqrt{x}}$. Donc, $\dd y = \dfrac{1}{2\sqrt{x}} \, \dd x$. 

Nous pouvons maintenant faire l'approximation.% 
\[f(37)=f(36+1) \approx f(36) + \dd y = f(36)+f'(36)\cdot 1=6+\displaystyle \frac{1}{12}=\frac{73}{12}.\]
}
\end{exemple}
\dans{ordi}{\newpage}
\end{bloc}%m
%
%
\begin{bloc}{bn}[\dans{b}{Approximation linéaire}]
Comme nous avons vu à la section \ref{secApproxLin}, pour faire une approximation linéaire, nous pouvons utiliser : \pause

\begin{liste}[beaOpt=<+- |alert@+>, itSep=0.3cm]
\item Une droite tangente. 
\item La \infobulle{différentielle}{defDiff}.
\end{liste}
\end{bloc}%bn


\begin{bloc}{bn}
\begin{exercice}

\QR{Approximer la valeur de $\dfrac{2}{1,1} + \dfrac{3}{1,1^2}$ à l'aide du calcul différentiel.}{$\dfrac{2}{1,1} + \dfrac{3}{1,1^2} \approx 4,2$}
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Approximer linéairement, à l'aide d'une droite tangente, la valeur de chacune des expressions suivantes.}
\debutSousQ
\QR{$(8,03)^{\frac{2}{3}}$}{$4,01$}
\QR{$\dfrac{(2,99)^2-4}{(2,99)^2+1}$}{$0,497$}
\finSousQ

\Q{Utiliser la différentielle pour approximer la valeur de chacune des expressions suivantes.}
\debutSousQ
\QR{$(8,03)^{\frac{2}{3}}$}{$4,01$}
\QR{$\dfrac{(2,99)^2-4}{(2,99)^2+1}$}{$0,497$}
\finSousQ
\end{secExercices}
\end{bloc}%eim

