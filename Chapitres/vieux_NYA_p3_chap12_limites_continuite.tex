

\chapter{Limites et continuité}



\section{Limites}

\begin{bframe}{Notation et signification}
\begin{center}
Que signifie $\dlim{x \to a}{f(x)} = b$?
\end{center}
\end{bframe}

\begin{bloc}{b}[Propriétés]
Nous avons vu les propriétés des limites aux \infobulle{théorème \ref{thmLimiteProp}}{thmLimiteProp}.
\end{bloc}%b

\begin{manuel}
Commençons par l'évaluation de limites de fonctions exponentielles et logarithmiques. On se rappelle que ces fonctions sont continues sur leur domaine.

\begin{ex}
\Q{Évaluer les limites suivantes.}
\debutSousQ
\QS{$\dlim{x \to 2}{2^x}$}{

D'abord, le domaine de cette fonction exponentielle est $\mathds{R}$ et comme toute fonction non définie par parties est \infobulle{continue sur son domaine}{thmContinuite}, cette fonction est continue en $x=2$. Nous pouvons donc automatiquement utiliser le fait que
$\dlim{x \to a}{f(x)} = f(a)$, ce qui découle du point 3 de la \infobulle{définition de la continuité}{defContinuiteEnUnPoint}.

Ainsi, $\dlim{x \to 2}{2^x} = 2^2 = 4$.}
\QS{$\dlim{x \to 1}{\displaystyle \frac{\log{(x)}}{2(x+5)}}$}{

De la même façon, il suffit ici d'évaluer l'image de la fonction lorsque $x=1$.

Ainsi, $\dlim{x \to 1}{\displaystyle \dfrac{\log{(x)}}{2(x+5)}}= \dfrac{\log{(1)}}{2(1+5)} = \displaystyle \frac{0}{12}=0$.
}
\finSousQ
\end{ex}
\ordi{\newpage}

Pour l'évaluation de limites de fonctions exponentielles et logarithmiques, la connaissance de l'allure du graphe nous sera très utile. De plus, toutes les astuces vues précédemment pour les formes indéterminées peuvent s'appliquer.
\mob{\newpage}
\begin{ex}
\Q{Évaluer les limites suivantes.}
\debutSousQ
\QS{$\dlim{x \to \infty}{e^x}$}{

On trace d'abord une esquisse du graphe de la fonction $e^x$. Comme $e>1$, le graphe a la forme suivante.
\begin{center}
\renewcommand{\minX}[0]{-3} 
\renewcommand{\maxX}[0]{3}
\renewcommand{\minY}[0]{-1}
\renewcommand{\maxY}[0]{10}
\begin{tik}[p3LimitesExExpoMan]
\begin{axis}
[
    grid=major,
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$f(x)$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={\minY,3,...,\maxY},
    xtick={\minX,...,\maxX},
    ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.2,
    xmax=\maxX + 0.2,
    axis line style={thick},
    height=6cm,
    width=8cm
]

\addplot [very thick, smooth, draw=blue, domain=-3.2:3.2] {e^x+0.05};

%%% asymptote
\addplot [very thick, dashed, draw=red] 
coordinates {
		(-3.2,0)
		(-1,0)
};
\end{axis}
\end{tik}
\end{center}
Comme les valeurs de $y$ ne font qu'augmenter lorsque les valeurs de $x$ augmentent, on a $\dlim{x \to \infty}{e^x}= \infty$.

On peut aussi y réfléchir en se disant qu'en multipliant $e$ avec lui-même un nombre infini de fois, le résultat sera infiniment grand puisque $e>1$.
}
\QS{$\dlim{x \to \infty}{\displaystyle \left(\frac{1}{2}\right)^x}$}{

Ici, $\frac{1}{2}<1$ et le graphe a la forme suivante.
\begin{center}
\renewcommand{\minX}[0]{-3} 
\renewcommand{\maxX}[0]{3}
\renewcommand{\minY}[0]{-1}
\renewcommand{\maxY}[0]{10}
\begin{tik}[p3LimitesExExpo2Man]

\begin{axis}
[
    grid=major,
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$f(x)$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={\minY,3,...,\maxY},
    xtick={\minX,...,\maxX},
    ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.2,
    xmax=\maxX + 0.2,
    axis line style={thick},
    height=6cm,
    width=8cm
]

\addplot [very thick, smooth, draw=blue, domain=-3.2:3.2] {e^(-x)+0.05};

%%% asymptote
\addplot [very thick, dashed, draw=red] 
coordinates {
		(3.2,0)
		(1,0)
};
\end{axis}
\end{tik}
\end{center}
Ainsi, $\dlim{x \to \infty}{\left(\frac{1}{2}\right)^x}= 0$.
}
\QS{$\dlim{x \to \infty}{\log{(x^2-x)}}$}{

\begin{minipage}[c]{0.5\textwidth}
\begin{align*}
& \ \dlim{x \to \infty}{\log{(x^2-x)}}   \hspace{1cm} & \text{F.I. } \infty - \infty \\[0.4cm]
=& \ \dlim{x \to \infty}{\log{\left(x^2\left(1-\frac{1}{x}\right)\right)}} \hspace{1cm} & \text{Forme } \log{(\infty)} \\[0.4cm]
=& \ \infty
\end{align*}
\end{minipage}  
\hspace*{1cm}
\begin{minipage}[c]{0.5\textwidth}
\renewcommand{\minX}[0]{-1} 
\renewcommand{\maxX}[0]{5}
\renewcommand{\minY}[0]{-5}
\renewcommand{\maxY}[0]{5}
\begin{tik}[p3LimitesExLogMan]

\begin{axis}
[
    grid=major,
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$f(x)$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={\minY,-3,...,\maxY},
    xtick={\minX,...,\maxX},
    restrict y to domain=-7:6,
    ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.2,
    xmax=\maxX + 0.2,
    axis line style={thick},
    height=6cm,
    width=7cm
]

\addplot [very thick, smooth, draw=blue, samples=100, domain=0.001:5.2] {ln(x-0.05)+0.05};

%%% asymptote
\addplot [very thick, dashed, draw=red] 
coordinates {
		(0,-5.2)
		(0,0)
};
\end{axis}
\end{tik}
\end{minipage}
}
\QS{$\dlim{x \to \infty}{\displaystyle \frac{e^{2x}+2e^{3x}}{e^{2x}-e^x}}$}{
\begin{align*}
& \ \dlim{x \to \infty}{\displaystyle \frac{e^{2x}+2e^{3x}}{e^{2x}-e^x}}   \hspace{2cm} & \text{F.I. } \displaystyle \frac{\infty}{\infty - \infty} \\[0.4cm]
=& \ \dlim{x \to \infty}{\displaystyle \frac{e^{3x}\left(\frac{1}{e^x}+2\right)}{e^{2x}\left(1-\frac{1}{e^x}\right)} } \\[0.4cm]
=& \ \dlim{x \to \infty}{\displaystyle \frac{e^{x}\left(\frac{1}{e^x}+2\right)}{1-\frac{1}{e^x}} } \\[0.4cm]
=& \ \infty
\end{align*}
Dans cet exemple, nous aurions pu tenter de déterminer plus précisément la nature de la forme en étudiant le comportement du dénominateur.% 
$$\dlim{x \to \infty}{\left(e^{2x}-e^x\right)} = \dlim{x \to \infty}{e^x(e^x-1)} = \infty$$
Ainsi, la forme exacte de la limite est $\frac{\infty}{\infty}$.
}
\finSousQ
\end{ex}
\end{manuel}



\begin{bnframe}[allowframebreaks]{Exemple \exe}
\Q{Évaluer les limites suivantes}
\debutSousQ[itSep=0.3cm]
\QR{$\dlim{x \to 4}{\displaystyle \frac{3^{\sqrt{x}}}{e^{x-4}+3}}$}{$\displaystyle \frac{9}{4}$}[\vfill]
\QR{$\dlim{x \to \infty}{\log{(x^2-5)}}$}{$\infty$}[\vfill \newpage]
\QR{$\dlim{x \to \infty}{\ln{\left(\displaystyle \frac{1}{x}\right)}}$}{$-\infty$}[\vfill]
\QR{$\dlim{x \to 0}{\ln{\paren*{\dfrac{1}{x^2}}}}$}{$\infty$}[\vfill]
\QR{$\dlim{x \to 0}{\dfrac{1-e^{2x}}{1-e^{x}}}$}{$2$}[\vfill]
\finSousQ
\end{bnframe} 


\begin{manuel}
Pour les fonctions trigonométriques, le plus avantageux sera de transformer les fonctions pour utiliser seulement les fonctions sinus et cosinus, puisque celles-ci sont continues sur les réels. Pour ce faire, on utilisera les \tip{définitions}{defFctTrigo}. 

\begin{ex}
\Q{Évaluer les limites suivantes.}
\debutSousQ
\QS{$\dlim{x \to \pi}{\sec{(x)}}$}{

$\dlim{x \to \pi}{\sec{(x)}} = \dlim{x \to \pi}{\dfrac{1}{\cos{(x)}}} = \dfrac{1}{\cos{(\pi)}}=\dfrac{1}{-1}={-1}$.
}
\QS{$\dlim{x \to \frac{\pi}{2}^+}{\paren[big]{\tan{(x)}-2\sec{(x)}}}$}{
\begin{align*}
& \ \dlim{x \to \frac{\pi}{2}^+}{\paren[big]{\tan{(x)}-2\sec{(x)}}}   \hspace{2cm}  \\[0.4cm]
=& \ \dlim{x \to \frac{\pi}{2}^+}{\paren*{\frac{\sin(x)}{\cos(x)}-\frac{2}{\cos{(x)}}}}   \hspace{2cm} & \text{F.I. } \infty - \infty \\[0.4cm]
=& \ \dlim{x \to \frac{\pi}{2}^+}{\dfrac{\sin(x)-2}{\cos(x)}} \hspace{2cm} & \text{F.D.} \dfrac{-1}{0^-} \\[0.4cm] %Il faudrait réussir à centrer les termes dans la fraction
=& \ \infty
\end{align*}
}
\QS{$\dlim{x \to 0^+}{\ln{\paren*{\sin(x)}}}$}{

\begin{minipage}[c]{0.5\textwidth}
\begin{align*}
& \ \dlim{x \to 0^+}{\ln{\paren*{\sin(x)}}} \hspace{1cm} & \text{Forme } \ln{\paren*{\sin(0^+)}=\ln(0^+)} \\[0.4cm]
=& \ -\infty  
\end{align*}
\end{minipage}  
\hspace*{1cm}
\begin{minipage}[c]{0.5\textwidth}
\renewcommand{\minX}[0]{-1} 
\renewcommand{\maxX}[0]{5}
\renewcommand{\minY}[0]{-5}
\renewcommand{\maxY}[0]{5}
\begin{tik}[p3LimitesExLog2Man]

\begin{axis}
[
    grid=major,
    axis lines=middle,
    inner axis line style={->},
    xlabel={$x$},
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$f(x)$},
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ytick={\minY,-3,...,\maxY},
    xtick={\minX,...,\maxX},
    restrict y to domain=-7:6,
    ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.2,
    xmax=\maxX + 0.2,
    axis line style={thick},
    height=5cm,
    width=6cm
]

\addplot [very thick, smooth, draw=blue, samples=100, domain=0.001:5.2] {ln(x-0.05)+0.05};

%%% asymptote
\addplot [very thick, dashed, draw=red] 
coordinates {
		(0,-5.2)
		(0,0)
};
\end{axis}
\end{tik}
\end{minipage}

Ici, pour évaluer $\dlim{x \to 0^+}{\sin(x)}$ il faut visualiser un angle juste un peu plus grand que $0$ radian dans le graphique de la fonction sinus. On constate que ce sinus sera positif et qu'il va tendre vers $0$.
}
\finSousQ
\end{ex}



\rem Notons que $\dlim{x \to \pm\infty}{\sin(x)}$ et $\dlim{x \to \pm\infty}{\cos(x)}$ n'existent pas puisque les fonctions sinus et cosinus sont périodiques (elles oscillent à l'infini).
\mob{\newpage}

\begin{ex}
\QS{Évaluer $\dlim{x \to \infty}{\cotan(x)}$.}{

$\dlim{x \to \infty}{\cotan{x}} ~~ \nexists$

On peut s'en convaincre en étudiant le graphe de la fonction cotangente, qui est périodique.
\begin{center}
\begin{minipage}[c]{0.5\textwidth}
\renewcommand{\minX}[0]{-5} 
\renewcommand{\maxX}[0]{5}
\renewcommand{\minY}[0]{-4}
\renewcommand{\maxY}[0]{4}
\begin{tik}[p3CotanMan]

\begin{axis}
[
    grid=major,
	height=5cm,
    width=7cm,
    grid=major,
    axis lines=middle,
    inner axis line style={->},
    restrict y to domain=-5:5,
    xlabel={$x$},    
    xlabel style={at={(ticklabel* cs:1)}, anchor=west},
    ylabel={$y$}, 
    ylabel style={at={(ticklabel* cs:1)}, anchor=south},
    ymin=\minY - 0.2,
    ymax=\maxY + 0.2,
    xmin=\minX - 0.2,
    xmax=\maxX + 0.2,
    ytick={-3,-1,...,\maxY},
    axis line style={thick},
    xtick={
        -3.14159, 0, 3.14159
    },
    xticklabels={
        -$\pi$, $0$, $\pi$
    }
]
\addplot [smooth, draw=blue, very thick, samples=100, domain=-5.2:-3.15] {cot(deg(x))};

\addplot [smooth, draw=blue, very thick, samples=100, domain=-3.14:0] {cot(deg(x))};

\addplot [smooth, draw=blue, very thick, samples=100, domain=0:3.14] {cot(deg(x))};

\addplot [smooth, draw=blue, very thick, samples=100, domain=3.15:5.2] {cot(deg(x))};

\addplot [dashed, very thick, draw=red] coordinates {
(3.1415,\minY) 
(3.1415,\maxY)
};

\addplot [dashed, very thick, draw=red] coordinates {
(-3.1415,\minY) 
(-3.1415,\maxY)
};

\addplot [dashed, very thick, draw=red] coordinates {
(0,\minY) 
(0,\maxY)
};

\end{axis}
\end{tik}
\end{minipage}
\end{center}
}
\end{ex}
\end{manuel}


\begin{bnframe}[allowframebreaks]{Exemple \exe}
\Q{Évaluer les limites suivantes.}
\debutSousQ[itSep=0.3cm]
\QR{$\dlim{x \to \frac{\pi}{2}}{\sec(x)}$}{$\nexists$}[\vfill \newpage]
\QR{$\dlim{x \to 2\pi}{\log{\paren*{\cos(x)}}}$}{$0$}[\vspace*{5cm}]
\QR{$\dlim{x \to \frac{\pi}{4}}{\dfrac{\cos(x)-\sin(x)}{1-\tan(x)}}$}{$\dfrac{\sqrt{2}}{2}$}[\vspace*{8cm}]
\QR{$\dlim{x \to \infty}{\cosec(x)}$}{$\nexists$}[\vspace*{2cm}]
\QR{$\dlim{x\to \frac{\pi}{2}}{\dfrac{\sin(2x)}{\cos(x)}}$}{$2$}[\vspace*{3cm}]
\finSousQ
\end{bnframe} 


\begin{manuel}
\begin{ex}
\QS{Montrer que $\dlim{x \to \infty}{\dfrac{\sin{(x)}}{x}}=0$ en utilisant le \infobulle{théorème du sandwich}{thmSandwich}.}{

La valeur du sinus d'un angle est toujours comprise entre ${-1}$ et $1$. On peut donc utiliser  l'inéquation suivante.%
$${-1}\leq \sin(x)\leq 1$$%
En divisant chaque terme par $x$, où $x>0$ puisque $x$ tend vers l'infini, on obtient%
$$-\dfrac{1}{x}\leq \dfrac{\sin(x)}{x}\leq \dfrac{1}{x}\text{ .}$$%
Or, $\dlim{x \to \infty}{-\dfrac{1}{x}}=0$ et $\dlim{x \to \infty}{\dfrac{1}{x}}=0$. Ainsi, selon le théorème du sandwich, $\dlim{x \to \infty}{\dfrac{\sin{(x)}}{x}}=0$.
}
\end{ex}
\end{manuel}


\begin{bnframe}{Exemple \exe}
\QR{Évaluer la limite suivante à l'aide du théorème du sandwich.%
$$\dlim{x \to \infty}{\paren*{\frac{\sin(x)}{e^x}+3}}$$}{$3$}[\vfill]
\end{bnframe} 

\begin{manuel}
Le théorème suivant nous sera utile pour trouver les règles de dérivation pour les fonctions trigonométriques.
\end{manuel}

\begin{bmntframe}
\begin{thm}[Limite de $\displaystyle \frac{\sin(x)}{x}$ quand $x$ tend vers $0$]{thmLimiteSinxSurx}
Si $x$ est un angle mesuré en radians, alors\index{Théorème!limite $\frac{\sin(x)}{x}$}%
$$\dlim{x \to 0}{\displaystyle \frac{\sin{(x)}}{x}}=1\text{ .}$$

\preuve{

Il s'agit d'une forme indéterminée $\frac{0}{0}$, mais les astuces vues jusqu'à présent ne permettent pas de lever l'indétermination. Nous utiliserons le théorème du sandwich. Pour construire notre sandwich, observons la figure suivante. Il s'agit d'un triangle rectangle dont la base mesure $1$. L'arc en pointillé est un arc de cercle de rayon $1$ et le segment droit en pointillé relie les points d'intersection de l'arc de cercle avec le triangle rectangle.
\begin{center}
%%%\tikzexternalenable
%%%\tikzsetnextfilename{p3LimitesDemoSinXsurX}
\begin{tikzpicture}[scale=5]
\draw
    (0.7071,0.7071) coordinate (a) node {}
    -- node[midway, above]{} (0,0) coordinate (b) node {}
    -- node[midway, below]{$1$} (1,0) coordinate (c) node {}
    pic["$x$", draw=black, angle eccentricity=1.2, angle radius=1cm]
    {angle=c--b--a};
    
\draw
    (0.7071,0.7071) coordinate (a) node {}
    -- node[midway, above]{} (0,0) coordinate (b) node {}
    -- node[midway, below]{} (1,0) coordinate (c) node {}
    pic[draw=black, dashed, angle eccentricity=1.2, angle radius=5cm]
    {angle=c--b--a};
    
\draw (0.95,0) --(0.95,0.05);
\draw (0.95,0.05) -- (1,0.05); 
\draw (1,0) -- node[midway, right]{$\tan(x)$} (1,1);
\draw (1,1) -- (0.7071,0.7071);     
\draw[dashed] (1,0) -- (0.7071,0.7071);   
\end{tikzpicture}
%%%\tikzexternaldisable
\end{center}
Dans la figure, nous remarquons deux triangles et un secteur de cercle.
\begin{center}
\hspace*{0.65cm}
\begin{minipage}[t]{0.33\textwidth}
%%%\tikzexternalenable
%%%\tikzsetnextfilename{p3LimitesDemoSinXsurX2}
\begin{tikzpicture}[scale=3,every node/.style={scale=1}]
\filldraw[fill=green!20] (0,0) -- (1,0) -- (0.7071,0.7071) -- cycle;  
\draw
    (0.7071,0.7071) coordinate (a) node {}
    -- node[midway, above]{} (0,0) coordinate (b) node {}
    -- node[midway, below]{$1$} (1,0) coordinate (c) node {}
    pic["$x$", draw=black, angle eccentricity=1.2, angle radius=1cm]
    {angle=c--b--a};
    
\draw
    (0.7071,0.7071) coordinate (a) node {}
    -- node[midway, above]{} (0,0) coordinate (b) node {}
    -- node[midway, below]{} (1,0) coordinate (c) node {}
    pic[draw=black, dashed, angle eccentricity=1.2, angle radius=3cm]
    {angle=c--b--a};
        
\draw[dashed] (0.7071,0) -- (0.7071,0.7071);
\draw[|<->|] (1.1,0) -- node[midway,right]{$\sin(x)$} (1.1, 0.7071); 
\end{tikzpicture}
%%%\tikzexternaldisable
\end{minipage}
\hspace*{-0.4cm}
\begin{minipage}[t]{0.33\textwidth}
%%%\tikzexternalenable
%%%\tikzsetnextfilename{p3LimitesDemoSinXsurX3}
\begin{tikzpicture}[scale=3]

\filldraw[fill=blue!20,draw=black] (0,0) -- (1,0) arc (0:45:1) -- node[midway, above]{$1$} cycle;

\draw
    (0.7071,0.7071) coordinate (a) node {}
    -- node[midway, above]{} (0,0) coordinate (b) node {}
    -- node[midway, below]{$1$} (1,0) coordinate (c) node {}
    pic["$x$", draw=black, angle eccentricity=1.2, angle radius=1cm]
    {angle=c--b--a};
\draw[dashed] (1,0) -- (0.7071,0.7071);
\end{tikzpicture}
%%%\tikzexternaldisable
\end{minipage}
\hspace*{-0.7cm}
\begin{minipage}[t]{0.33\textwidth}
%%%\tikzexternalenable
%%%\tikzsetnextfilename{p3LimitesDemoSinXsurX4}
\begin{tikzpicture}[scale=3]

\filldraw[fill=red!20] (0,0) -- (1,0) -- (1,1) -- cycle;  
\draw
    (0.7071,0.7071) coordinate (a) node {}
    -- node[midway, above]{} (0,0) coordinate (b) node {}
    -- node[midway, below]{$1$} (1,0) coordinate (c) node {}
    pic["$x$", draw=black, angle eccentricity=1.2, angle radius=1cm]
    {angle=c--b--a};
    
\draw
    (0.7071,0.7071) coordinate (a) node {}
    -- node[midway, above]{} (0,0) coordinate (b) node {}
    -- node[midway, below]{} (1,0) coordinate (c) node {}
    pic[draw=black, dashed, angle eccentricity=1.2, angle radius=3cm]
    {angle=c--b--a};
    
\draw (0.95,0) --(0.95,0.05);
\draw (0.95,0.05) -- (1,0.05); 
\draw (1,0) -- node[midway, right]{$\tan(x)$} (1,1);
\draw (1,1) -- (0.7071,0.7071);     
\draw[dashed] (1,0) -- (0.7071,0.7071);    
\end{tikzpicture}
%%%\tikzexternaldisable
\end{minipage}
\end{center}

\begin{center}
{\color{green}Aire triangle vert} $\leq$ {\color{blue}Aire secteur bleu} $\leq$ {\color{red} Aire triangle rouge}
\end{center}
Calculons chacune des aires.
\ordi{\newpage}

Pour calculer l'aire des triangles, on multiplie la longueur de leur base par leur hauteur et on divise par 2.

{\color{green}Aire triangle vert} $=\displaystyle \frac{\sin(x)}{2}$. 

Pour le triangle rouge, la base mesure 1 et la hauteur représente la tangente de l'angle $x$.

{\color{red} Aire triangle rouge} $=\displaystyle \frac{1 \cdot \tan(x)}{2}=\frac{\sin(x)}{2\cos(x)}$.

Finalement, pour l'aire du secteur bleu, on utilise le produit croisé. C'est à cette étape qu'on exige que l'angle soit calculé en radians.%
$$\dfrac{\text{Aire cercle}}{2\pi}=\dfrac{\text{Aire du secteur}}{\text{Angle}}\Rightarrow\dfrac{\pi\cdot 1^2}{2\pi}=\dfrac{\text{Aire du secteur}}{x}$$%
{\color{blue}Aire secteur bleu} $=\displaystyle \frac{\pi \cdot x}{2\pi}=\frac{x}{2}$.

On obtient donc l'inéquation%
$$\dfrac{\sin(x)}{2} \leq \dfrac{x}{2} \leq \dfrac{\sin(x)}{2\cos(x)} ~\Rightarrow ~ \sin(x) \leq x \leq \dfrac{\sin(x)}{\cos(x)}.$$%
Comme on cherche à évaluer la limite lorsque $x$ tend vers 0, nous allons étudier deux cas deux cas.

\textbf{Cas 1:} Si $x$ tend vers 0 par la gauche, alors $\sin(x)$ tend vers 0 par la gauche, c'est-à-dire que $\sin(x)<0$. Ainsi, si on divise chaque membre de l'inéquation par $\sin(x)$, on obtient:%
$$\displaystyle 1 \geq \frac{x}{\sin(x)} \geq \frac{1}{\cos(x)}.$$% 
Puis, en inversant chaque terme, on obtient :%
$$1\leq \frac{\sin(x)}{x}\leq \cos(x) \text{ .}$$%
La dernière égalité se justifie de la même façon que $2<3 ~\Rightarrow \dfrac{1}{2}>\dfrac{1}{3}$. 

C'est le sandwich que nous recherchions. En effet, $\dlim{x \to 0^-}{1}=1$ et $\dlim{x \to 0^-}{\dfrac{1}{\cos(x)}}=1$ et donc, selon le théorème du sandwich, $\dlim{x \to 0^-}{\dfrac{\sin(x)}{x}}=1$.

\textbf{Cas 2:} Si $x$ tend vers 0 par la droite, alors $\sin(x)$ tend vers 0 par la droite, c'est-à-dire que $\sin(x)>0$. Ainsi, si on divise chaque membre de l'inéquation par $\sin(x)$, on obtient:%
$$1 \leq \dfrac{x}{\sin(x)} \leq \dfrac{1}{\cos(x)} \Rightarrow 1\geq \dfrac{\sin(x)}{x}\geq \cos(x).$$%
De la même façon, comme $\dlim{x \to 0^+}{1}=1$ et $\dlim{x \to 0^+}{\dfrac{1}{\cos(x)}}=1$, on obtient, selon le théorème du sandwich, $\dlim{x \to 0^+}{\displaystyle \frac{\sin(x)}{x}}=1$.

On peut donc conclure que $\dlim{x \to 0}{\dfrac{\sin(x)}{x}}=1$.
\findemo 
}
\end{thm}
\ndc{\newpage}
\end{bmntframe}



\begin{bnframe}{Preuve}
\begin{center}
gros triangle
\end{center}
\end{bnframe}


\begin{bnframe}{Preuve}

\begin{minipage}[t]{0.33\textwidth}
petit triangle
\end{minipage}
\begin{minipage}[t]{0.33\textwidth}
petit triangle
\end{minipage}
\hspace*{-0.3cm}
\begin{minipage}[t]{0.33\textwidth}
petit triangle
\end{minipage}
\ndc{\newpage}
\end{bnframe}

\begin{manuel}
\begin{ex}
\QS{Évaluer $\dlim{x \to 0}{\frac{\tan(x)}{x}}$.}{
\begin{align*}
& \ \dlim{x \to 0}{\frac{\tan(x)}{x}} \\[0.4cm]
=& \ \dlim{x \to 0}{\frac{\sin(x)}{x \cdot \cos(x)}} \hspace{2cm} & \text{F.I. } \frac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 0}{\left(\frac{\sin(x)}{x} \cdot \frac{1}{\cos(x)}\right)}\\[0.4cm]
=& \ \dlim{x \to 0}{\frac{\sin(x)}{x}}\cdot \dlim{x \to 0} {\frac{1}{\cos(x)}}\\[0.4cm]
=& \ 1 \cdot 1 \hspace{2cm} & \text{\tip{théorème \ref{thmLimiteSinxSurx}}{thmLimiteSinxSurx}}\\[0.4cm]
=& \ 1 
\end{align*}
}
\end{ex}
\ordi{\newpage}
\end{manuel}



\begin{bmntframe}{Théorème}
\begin{thm}[Limite de $\dfrac{1-\cos(x)}{x}$ quand $x$ tend vers $0$]{thmLimiteUnMoinsCosxSurx}
Si $x$ est un angle mesuré en radian, alors%
$$\dlim{x\to 0}{\dfrac{1-\cos(x)}{x}}=0\text{ .}$$

\preuve{ 
\begin{alignat*}{3}
\dlim{x\to 0}{\dfrac{1-\cos(x)}{x}} &= \dlim{x\to 0}{\dfrac{\big(1-\cos(x)\big)\big(1+\cos(x)\big)}{x\big(1+\cos(x)\big)}} \hspace{2cm} && \text{multiplication par le conjugué} \\[0.3cm]
 &= \dlim{x\to 0}{\dfrac{1-\cos^2(x)}{x\big(1+\cos(x)\big)}} \hspace{2cm} && \\[0.3cm]
 &= \dlim{x\to 0}{\dfrac{\sin^2(x)}{x\big(1+\cos(x)\big)}} \hspace{2cm} && \text{\infobulle{identité trigonométrique}{propIdentitesTrigoBase}}\\[0.3cm]
 &= \dlim{x\to 0}{\dfrac{\sin(x)}{x}} \cdot \dlim{x \to 0}{\dfrac{\sin(x)}{1+\cos(x)}} \hspace{2cm} && \text{\tip{propriété des limites}{thmLimiteProp}}\\[0.3cm]
 &= 1 \cdot 0 && \text{\tip{théorème \ref{thmLimiteSinxSurx}}{thmLimiteSinxSurx}} \\[0.3cm]
 &= 0
\end{alignat*}

\findemo
}
\end{thm}
\ndc{\vfill}
\end{bmntframe}




\begin{bnframe}{Exemple \exe}
\QR{Évaluer la limite suivante.%
$$\dlim{x \to 0}{2x\cotan(x)}$$}{$2$}[\vfill]
\end{bnframe} 




\devoirs
\ndc{\newpage}


\begin{manuel}

\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?v=xhhupTKsX1A}{Limites avec des fonctions exponentielles}  

\href{https://www.youtube.com/watch?v=MJf0BCB_tHg}{Limites avec des fonctions logarithmiques} 

\href{https://www.youtube.com/watch?time_continue=272&v=DCpaN5c-DbY}{Limites avec des fonctions trigonométriques} 

\href{https://www.youtube.com/watch?time_continue=17&v=7dnoW-mWlAo}{Démonstration $\dlim{x\to 0}{\dfrac{\sin(x)}{x}}$}
\end{manuel}



\begin{sectionEx}
\Q{Évaluer les limites.}
\debutSousQ
\QR{$\dlim{x \to \infty}{e^{\frac{1}{x}}}$}{$1$}
\QR{$\dlim{x \to 0^-}{e^{\frac{1}{x}}}$}{$0$}
\QR{$\dlim{x \to \infty}{\dfrac{1-e^x}{1+e^x}}$}{${-1}$}
\QR{$\dlim{x \to 2^-}{\ln(2-x)}$}{$-\infty$ donc $\nexists$}
\QR{$\dlim{x \to 0^+}{e^{\frac{1}{x}}}$}{$\infty$ donc $\nexists$}
\QR{$\dlim{x \to 2}{\dfrac{x3^x}{4^x-x}}$}{$\dfrac{9}{7}$}
\QR{$\dlim{x \to \infty}\paren*{{\dfrac{1}{2}}}^{\sqrt{x}}$}{$0$}
\QR{$\dlim{x \to 0}{\log_3(x^2+1)}$}{$0$}
\QR{$\dlim{x \to 4}{\log_3(7x-1)}$}{$3$}
\QR{$\dlim{x \to 0}{\log_2\paren*{\dfrac{1}{x^2}}}$}{$\infty$ donc $\nexists$}
\QR{$\dlim{x \to 1}{\paren*{(\ln(x))^3-2}^5}$}{${-32}$}
\QR{$\dlim{x \to 4}{\dfrac{e^{x^2}}{\sqrt{x}-2}}$}{$\nexists$}
\QR{$\dlim{x \to 0}{\sqrt{3^x-1}}$}{$0$}
\finSousQ

\Q{Évaluer les limites.}
\debutSousQ
\QR{$\dlim{x \to \frac{\pi}{4}}{\dfrac{x \sin(x)}{\pi}}$}{$\dfrac{\sqrt{2}}{8}$}
\QR{$\dlim{x \to \frac{\pi}{3}}{\cotan(x)}$}{$\dfrac{\sqrt{3}}{3}$}
\QR{$\dlim{x \to 0^-}{e^{\cosec(x)}}$}{$0$}
\QR{$\dlim{x \to \frac{\pi}{2}}{\dfrac{\cos(x)+1}{3\sin(x)}}$}{$\dfrac{1}{3}$}
\QR{$\dlim{x \to 1}{\dfrac{4^{2x+1}+\cotan\paren*{\dfrac{\pi x}{2}}}{2^{3x}+\ln(x)}}$}{$8$}
\QR{$\dlim{x \to 0}{\dfrac{x}{\sin(x)}}$}{$1$}
\QR{$\dlim{x \to 0}{\dfrac{2\sin(x)}{x^2+x}}$}{$2$}
\QR{$\dlim{x \to 0}{\dfrac{\cos(x)}{x}}$}{$\nexists$}
\QR{$\dlim{x \to \frac{\pi}{4}}{\dfrac{\cos(x)-\sin(x)}{1-\tan(x)}}$}{$\dfrac{\sqrt{2}}{2}$}
\QR{$\dlim{x \to 1}{\cos\paren*{\dfrac{x^2-1}{x-1}}}$}{$\cos(2)$}
\QR{$\dlim{x \to 0}{\sin\paren*{\dfrac{1}{x}}}$}{$\nexists$}
\QR{$\dlim{x \to 0}{\sin\paren*{\dfrac{x^2-\pi x}{x}}}$}{$0$}
\QR{$\dlim{x \to 0}{\dfrac{\sin(3x)}{\tan(3x)}}$}{$1$}
\QR{$\dlim{x \to \frac{\pi}{2}^+}{e^{\tan(x)}}$}{$0$}
\QR{$\dlim{x \to 0^+}{\paren[Big]{\ln\paren[big]{\sin(2x)}-\paren[\big]{\tan(x)}}$}{$\ln(2)$}
\QR{$\dlim{x \to \infty}{\arctan(x)}$ \hfill} \rep{p}{$\dfrac{\pi}{2}$}
\finSousQ

\Q{Utiliser le théorème du sandwich pour évaluer les limites suivantes.}
\debutSousQ
\QR{$\dlim{x \to \infty}{\dfrac{\cos(2x)}{x^2}}$}{$0$}
\QR{$\dlim{x \to 0^+}{\sqrt{x}e^{\sin\paren*{\dfrac{\pi}{x}}}}$}{$0$}
\QR{$\dlim{x \to \infty}{\dfrac{\sin(x)+2}{3x+2}}$}{$0$}
\QR{$\dlim{x \to 0}{x\sin\paren*{\dfrac{1}{x}}}$}{$0$}
\finSousQ
\end{sectionEx}





\section{Continuité}

\begin{manuel}
Les fonctions qui ne sont pas définies par parties sont continues sur leur domaine. Pour étudier la continuité aux valeurs charnières des fonctions définies par parties, les limites étudiées dans la section précédente seront utiles. 

\begin{ex}
\QS{Déterminer l'ensemble sur lequel la fonction suivante est continue.%
$$f(x) =
\begin{fctpp}
$2^x$ & si $x\leq 0$ \\[0.2cm]
$\cosec(x)$ & si $0<x<2\pi$ \\[0.2cm]
$\displaystyle \frac{\ln(x)}{\sqrt{10-x}}$ & si $x>2\pi$ 
\end{fctpp}$$}{

Il faut d'abord étudier la continuité sur chaque branche. Le domaine de la fonction $y=2^x$ est $\mathds{R}$, donc la fonction $f$ est continue sur $]-\infty,\,0[$.

Comme $\cosec(x)=\dfrac{1}{\sin(x)}$, le domaine de la fonction $y=\cosec(x)$ est $\mathds{R} \setminus \{x \vert \sin(x)=0\}=\mathds{R} \setminus \{k\pi ~\vert~ k \in \mathds{Z}\}$, donc la fonction $f$ est continue sur $]0, \,2\pi[ \setminus \{\pi\}$.

Le domaine de la fonction $\dfrac{\ln(x)}{\sqrt{10-x}}$ est $]0, \, 10[$, donc la fonction $f$ est continue sur $]2\pi, \, 10[$.

On étudie ensuite la continuité à la valeur charnière $x=0$.

\begin{liste}
\item $f(0) = 2^0=1$ donc $0 \in \dom(f)$. 

\item
\begin{minipage}[t]{0.35\textwidth}
\begin{align*}
\dlim{x \to 0^-}{f(x)} =& \ \dlim{x \to 0^-}{2^x}\\[0.4cm]
 =& \ 1 
\end{align*} 
\end{minipage}
\begin{minipage}[t]{0.65\textwidth}
\begin{align*}
\dlim{x \to 0^+}{f(x)} =& \ \dlim{x \to 0^+}{\cosec(x)} \\[0.3cm]
=& \ \dlim{x \to 0^+}{\frac{1}{\sin(x)}}  \hspace{1cm} & \text{Forme }\frac{1}{0^+} \\[0.3cm]
=& \ \infty 
\end{align*}
\end{minipage}
\end{liste}
Comme la limite n'existe pas, la fonction $f$ n'est pas continue en $x=0$.

De plus, comme $f(2\pi) ~\nexists$, la fonction $f$ est  discontinue en $x=2\pi$.

En conclusion, la fonction $f$ est continue $\forall ~ x~ \in ]-\infty,\, 10[  \setminus \{0, \, \pi, \, 2\pi\}$.
}
\end{ex}
\end{manuel}


\begin{bloc}{bn}[\dans{b}{Continuité}]
Nous avons vu au \infobulle{théorème \ref{thmContinuite}}{thmContinuite} que toutes les fonctions usuelles non définies par parties sont continues sur leur \infobulle{domaine}{tabCalculsImpossiblesFinal}. \pause

De plus, la \infobulle{définition de la continuité en un point}{defContinuiteEnUnPoint} est toujours la même.
\end{bloc}%bn


\begin{bnframe}{Exemple \exe}
\QR{Déterminer l'ensemble sur lequel la fonction suivante est continue.
$$f(x) =
\begin{fctpp}
$2^x\sqrt{x+16}$ & si $x\leq 0$ \\
$\tan(x)$ & si $0<x\leq 2\pi$ \\
$\ln(x^2-9)$ & si $x > 2\pi$\\
\end{fctpp}$$}{$[{-16}, \,0[ \cup ]0, \, 2\pi[ \setminus \acco*{\frac{\pi}{2}, \frac{3\pi}{2}} \cup ]2\pi, \, \infty[$}[\vfill]
\end{bnframe} 


\devoirs
\ndc{\newpage}



\begin{sectionEx}
\Q{Déterminer pour quelles valeurs de $x$ la fonction $f$ est continue.}
\debutSousQ
\QR{
$f(x) =
\begin{fctpp}
$\cos(x)$ & si $x \leq \frac{\pi}{2}$ \\
$\sin(2x)$ & si $\frac{\pi}{2} < x < 2\pi$ \\
$\sqrt{x}$ & si $ x > 2\pi$ 
\end{fctpp}$}{$\mathds{R}\setminus\{2\pi\}$}
\QR{$f(x)=\sin\paren*{\dfrac{x+3}{x^2-1}}$}{$\mathds{R}\setminus\{{-1},\,1\}$} 
\QR{$f(x)=\dfrac{\sqrt{x}+\cos(x)}{\sin(x)-1}$}{$[0,\,\infty[\setminus \acco*{\dfrac{\pi}{2}+2k\pi|k \, \in \, \mathds{Z}}$} 
\QR{$f(x)=\ln\paren*{\dfrac{3+x}{x-2}}$}{$]-\infty,\,{-3}[ \, \cup \, ]2,\,\infty[$}
\QR{$f(x)=\dfrac{\arctan(x)+\ln(x)}{x^2-4}$}{$]0,\,2[ \, \cup \, ]2,\,\infty[$}
\QR{$f(x)=e^{\frac{\sin(x)}{x}}$}{$\mathds{R}\setminus\{0\}$}
\QR{$f(x)=\dfrac{\arcsin\left(\dfrac{1}{x}\right)}{x}$}{$\mathds{R}\setminus]{-1},\,1[$}  
\finSousQ
\end{sectionEx}





