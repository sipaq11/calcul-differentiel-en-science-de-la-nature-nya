

\chapter{Applications}


\section{Extremums absolus}


\begin{manuel}
À la section \ref{secExtAbs} de la partie \ref{partPoly}, nous avons défini les \tip{extremums absolus}{defExtremumsAbsolus}. Le \tip{théorème \ref{thmValExetreme}}{thmValExetreme} nous donne des conditions suffisantes pour qu'une fonction possède un maximum et un minimum absolu. Dans le cas où ces conditions sont respectées, le \tip{théorème \ref{thmPositionValExt}}{thmPositionValExt} nous indique où se situent les extremums absolus de la fonction. 

Ainsi, pour trouver les extremums absolus d'une fonction continue sur un intervalle $[a,\, b]$, il suffit de trouver toutes les valeurs critiques de $f$ qui sont dans $]a,\, b[$. Nous évaluons ensuite la fonction à chacune de ces valeurs critiques, en $x=a$ et en $x=b$. La plus grande valeur de $f$ ainsi calculée est le maximum absolu et la plus petite est le minimum absolu. 

Pour trouver les extremums absolus lorsque la fonction n'est pas continue ou que l'intervalle n'est pas fermé, nous cherchons à déterminer si la fonction peut prendre des valeurs plus grandes ou plus petites que celles trouvées aux valeurs critiques et aux extrémités incluses de l'intervalle. Il faut étudier le comportement de la fonction autour des discontinuités et des extrémités non incluses de l'intervalle. Pour étudier le comportement de la fonction, nous évaluons des limites.
\end{manuel}


\begin{bnframe}{Extremums absolus}

Pour trouver les \tip{extremums absolus}{defExtremumsAbsolus} d'une fonction $f$ : \pause
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]
\item On se demande s'ils existent (\tip{théorème \ref{thmValExetreme}}{thmValExetreme}).
\item Si le \tip{théorème \ref{thmValExetreme}}{thmValExetreme} nous confirme leur existence, nous les trouvons grâce au \tip{théorème \ref{thmPositionValExt}}{thmPositionValExt}. 
C'est-à-dire, en évaluant la fonction aux valeurs critiques ainsi qu'aux extrémités de l'intervalle. 
\item Sinon, après avoir évalué aux valeurs critiques et aux extrémités incluses, on étudie le comportement de la fonction aux extrémités non incluses de l'intervalle et aux discontinuités de la fonction.
\end{liste}
\end{bnframe}


\begin{manuel}
\ordi{\newpage}

\begin{ex}
\QS{Déterminer les extremums absolus de la fonction $f(x) = 1+\dfrac{1}{x}+\dfrac{1}{x^2}$ sur l'intervalle $[{-4} ,\, 1]$. 
}{

La fonction $f$ n'est pas continue en $x=0$ car $\dom(f) = \mathds{R}\setminus \{0\}$. Nous ne savons donc pas si les extremums absolus existent.  

Nous cherchons ensuite les valeurs critiques de $f$. 

Nous trouvons $f'(x) = -\dfrac{x+2}{x^3}$. 

Ainsi, $\dom(f') = \mathds{R}\setminus \{0\}$. Puisque $x=0$ ne fait pas partie du domaine de $f$, il ne s'agit pas d'une valeur critique. 

Ensuite, $f'(x) = 0 \Rightarrow x = -2$. Il y a donc une seule valeur critique et elle se situe dans l'intervalle qui nous intéresse. 

Il faut évaluer la fonction à cette valeur ainsi qu'aux bornes. 
\begin{align*}
f(-4) &= \frac{13}{16} \\
f(1) &= 3 \\
f(-2) &= \frac{3}{4} 
\end{align*}
Il faut ensuite étudier le comportement de $f$ autour de $x=0$ avant de conclure. 
\begin{alignat*}{3}
& \ \dlim{x \to 0^-}{\left(1+\frac{1}{x}+\frac{1}{x^2}\right)} && \hspace{4cm}   \text{F.I. } && -\infty+\infty \\[0.4cm]
=& \ \dlim{x \to 0^-}{\dfrac{x^2+x+1}{x^2}} && \hspace{4cm}   \text{F.D. } && \dfrac{1}{0^+} \\[0.4cm]
=& \ \infty
\end{alignat*}
La fonction n'a donc pas de maximum absolu sur cet intervalle.
\begin{alignat*}{3}
& \ \dlim{x \to 0^+}{\left(1+\frac{1}{x}+\frac{1}{x^2}\right)} && \hspace{4cm}   \text{F.D. } && \infty+\infty \\[0.4cm]
=& \ \infty
\end{alignat*}
Nous trouvons donc que le minimum absolu de $f$ sur $[{-4} ,\, 1]$ est $\frac{3}{4}$.
}
\end{ex}



\begin{ex}
\QS{Déterminer les extremums absolus de la fonction $f(x) = x-3x^{\frac{1}{3}}$ sur l'intervalle $[{-2} ,\, 2[$. 
}{

Nous avons $\dom(f) = \mathds{R}$. Puisqu'il y a une borne de l'intervalle qui n'est pas incluse, nous ne savons pas si les extremums absolus existent. 

Nous cherchons ensuite les valeurs critiques de $f$. 

Nous trouvons $f'(x) = 1 - \dfrac{1}{x^{\frac{2}{3}}}$. 

Ainsi, $\dom(f') = \mathds{R}\setminus \{0\}$. Puisque $x=0$ fait partie du domaine de $f$, il s'agit d'une valeur critique $f$. De plus, elle est dans l'intervalle qui nous intéresse. 

Ensuite, $f'(x) = 0 \Rightarrow x = \pm 1$. Ces deux valeurs font partie du domaine de $f$, donc elles sont des valeurs critiques. De plus, elles sont dans l'intervalle qui nous intéresse. 

Il faut évaluer la fonction à ces valeurs critiques ainsi qu'à la borne incluse. 
\begin{align*}
f({-2}) &= -2-3\sqrt[3]{-2} \approx 1,78 \text{ \imaCalcu}\\
f(1) &= -2 \\
f({-1}) &= 2 \\
f(0) &= 0
\end{align*}
Il faut ensuite étudier le comportement de $f$ quand $x$ se rapproche de $2$ par la gauche avant de conclure. 
\begin{alignat*}{3}
& \ \dlim{x \to 2^-}{\left(x-3x^{\frac{1}{3}}\right)} \\[0.4cm]  
=& \ 2-3\sqrt[3]{-2} \approx -1,78 \text{ \imaCalcu}
\end{alignat*}
Puisque ${-2} < 2-3\sqrt[3]{-2} < 2$, la fonction possède un maximum et un minimum absolu. 

Nous trouvons donc que le minimum absolu de $f$ sur $[{-2} ,\, 2[$ est ${-2}$ et son maximum absolu sur $[{-2} ,\, 2[$ est $2$.
}
\end{ex}
\end{manuel}



\begin{bnframe}{Exercice \exe}
\Q{Déterminer les extremums absolus des fonctions suivantes sur l'intervalle donné.}
\debutSousQ[itSep=0.3cm]
\QR{$f(x) = x^{\frac{5}{3}} - 5x^{\frac{2}{3}}$ sur $]{-1},\, 1]$}{Le maximum absolu de $f$ est $0$ et il est atteint en $x=0$. 

La fonction n'a pas de minimum absolu sur $]{-1},\, 1]$. 
} 

\QR{$f(x) = x\sqrt{2-x^2}$ sur $[-\sqrt{2},\, \sqrt{2}]$}{Le maximum absolu de $f$ est $1$ et il est atteint en $x=1$. 

Le minimum absolu de $f$ est ${-1}$ et il est atteint en $x={-1}$.
}
\finSousQ
\end{bnframe}




\devoirs
\ndc{\newpage}




\begin{sectionEx}

\Q{Déterminer, s'il y a lieu, les valeurs du maximum et du minimum absolus de $f$ sur l'intervalle donné et situer ces valeurs.}
\debutSousQ
\QR{$f(x)=\sqrt[3]{x^3+1}$ sur $[{-2},\,0]$}{Minimum absolu de $-\sqrt[3]{7}$ en $x={-2}$ et maximum absolu de $1$ en $x = 0$.}
\QR{$f(x)=\dfrac{x^2+4}{x-1}$ sur $[{-2},\,6]$}{Aucun extremum absolu.}
\QR{$f(x)=x\sqrt{4x^2-16}$ sur $[{-4},\,4]$}{Minimum absolu de ${-4}\sqrt{48}$ en $x={-4}$ et maximum absolu de $4\sqrt{48}$ en $x=4$.}
\QR{$f(x)=\dfrac{(x+1)^2}{(x-1)^2}$ sur $[{-2},\,3]$}{Minimum absolu de 0 en $x={-1}$ et aucun maximum absolu.}
\QR{$f(x)=\dfrac{\sqrt{x^2+1}}{x-1}$ sur $]{-2},\,0[$}{Aucun minimum absolu et maximum absolu de $-\frac{1}{\sqrt{2}}$ en $x = {-1}$.}
\finSousQ

\Q{Déterminer, s'il y a lieu, les valeurs du maximum et du minimum absolus de $f(x)=\dfrac{2x^2}{x^2+x-6}$ sur les intervalles donnés.}
\debutSousQ
\QR{sur $]-\infty,\,\infty[$}{Aucun extremum absolu.}
\QR{sur $]{-3},\,2[$}{Maximum absolu de $0$ en $x=0$ et aucun minimum absolu.}
\QR{sur $[0,\,\infty[$}{Aucun extremum absolu.}
\QR{sur $]2,\,\infty[$}{Aucun maximum absolu et minimum absolu de $\frac{48}{25}$ en $x=12$.}
\QR{sur $[4,\,\infty[$}{Maximum absolu de $\frac{16}{7}$ en $x=4$ et minimum absolu de $\frac{48}{25}$ en $x=12$.}
\QR{sur $[6,\,\infty[$}{Maximum absolu de $2$ en $x=6$ et minimum absolu de $\frac{48}{25}$ en $x=12$.}
\finSousQ
\end{sectionEx}

\section{Optimisation}

\begin{bmframe}{Démarche}
Au chapitre \man{\ref{chapOpti}}\bea{6}, nous avons vu une \tip{démarche}{tabMethodeOpti} pour résoudre les problèmes d'optimisation. \man{Revoyons avec un problème en contexte ces différentes étapes de résolution.}
\end{bmframe}

\begin{manuel}
\begin{ex} 
\QS{On veut fabriquer une boîte de 32 dm\up{3} sans couvercle ayant la forme d'un prisme à base carrée. Quelles sont les dimensions de la boîte permettant de minimiser la quantité de matériaux utilisée? 
}{

\textbf{Étape 1: Lire attentivement le problème. Relire au besoin.} \\

\textbf{Étape 2: Nommer les variables. Utiliser un schéma, s'il y a lieu.}  

$A$: aire totale de la boîte (dm\up{2}) \\
$x$: longueur d'un côté du carré formant la base (dm)\\
$h$: hauteur de la boîte (dm)
\begin{center}
\tikzexternalenable
\tikzsetnextfilename{p2ApplicationEx1Man}
\begin{tikzpicture}[x  = {(-0.5cm,-0.5cm)},
                    y  = {(0.95cm,-0.05cm)},
                    z  = {(0cm,1cm)}]

\begin{scope}[canvas is yz plane at x=-1]
  \draw[black] (-1,-1) -- (-1,1) -- (1,1) node[above, midway]{$x$} -- (1,-1) -- cycle ;
\end{scope}
\begin{scope}[canvas is yz plane at x=1]
  \draw[black] (-1,-1) -- (-1,1) node[left, midway]{$h$} -- (1,1) -- (1,-1) -- cycle;
\end{scope}
\begin{scope}[canvas is xz plane at y=1]
  \draw[black] (-1,-1) -- (-1,1) -- (1,1) -- (1,-1) -- cycle;
\end{scope}
\begin{scope}[canvas is xz plane at y=-1]
  \draw[black] (-1,-1) -- (-1,1) -- (1,1) node[above, midway]{$x$} -- (1,-1) -- cycle;
\end{scope}

\end{tikzpicture}
\tikzexternaldisable
\end{center}

\textbf{Étape 3: Établir une formule pour la quantité à optimiser.}  

Nous avons $A(x)=x^2+4xh$. \newpage

\textbf{Étape 4: Au besoin, trouver une relation entre les variables afin que la fonction à optimiser ne dépende que d'une seule variable.}  

Comme le volume de la boîte doit être de 32 dm\up{3}, nous trouvons $32=x^2\cdot h \Rightarrow h=\displaystyle \frac{32}{x^2}$.

Donc, en substituant $h$, nous obtenons $A(x)=x^2+\displaystyle \frac{128}{x}$.\\

\textbf{Étape 5: Déterminer le domaine de la fonction à optimiser, selon la variable choisie.}  

Puisque $x$ représente une mesure, nous avons $x\geq 0$.

Or, la fonction $A$ n'est pas définie si $x=0$ puisque cela entraînerait une division par zéro.

Donc, nous obtenons $\dom(A) = ]0,\,\infty[$.\\

\textbf{Étape 6: Utiliser le calcul différentiel pour trouver les extremums cherchés.}  

Nous cherchons les valeurs critiques de $A(x)=x^2+\displaystyle \frac{128}{x}$. 

Nous avons $A'(x)=2x-128x^{-2}=2x-\displaystyle \frac{128}{x^2}=\displaystyle\frac{2x^3-128}{x^2}$. 

Ensuite, $A'(x)=0$ si $x=4$. Comme cette valeur fait partie du domaine de $A$, il s'agit d'une valeur critique.

Aussi, $A'(x) ~\nexists$ si $x=0$. Comme cette valeur ne fait pas partie du domaine de $A$, il ne s'agit pas d'une valeur critique.

Nous calculons $A(4)=48$.

De plus, $\dlim{x \to 0^+}{A(x)}=\infty$ et $\dlim{x \to \infty}{A(x)}=\infty$, alors cela confirme que le minimum est atteint en $x=4$.\\

\textbf{Étape 7: Répondre à la question formulée dans le problème, en tenant compte du contexte. S'assurer que la réponse est plausible.} 

Il faut fabriquer une boîte avec une base carrée de 4 dm de côté et une hauteur de 2 dm pour obtenir un volume de 32 dm\up{3} et une aire totale minimale de 48 dm\up{2}. 
}
\end{ex}
\end{manuel}



\begin{bnframe}{Exercice \exe}
\QR{On veut alimenter en électricité une habitation sur une île située à 3 km d'un rivage rectiligne. Pour ce faire, on doit relier l'île par câble électrique à un point d'alimentation se situant sur le rivage, à 10 km du point du rivage le plus près de l'île. Si le coût d'installation du câble est de 5 \$ par mètre sur la terre et de 9 \$ par mètre sous l'eau, donner les longueurs de câble à installer sous l'eau et sur la terre ferme qui permettraient de minimiser les coûts d'installation. \ \imaCalcu{}}{
Il faut installer $10-\dfrac{15}{2\sqrt{14}}$ kilomètres de câble sur la terre et $\dfrac{27}{2\sqrt{14}}$ kilomètres de câble sous l'eau. 
}
\end{bnframe}



\devoirs



\begin{sectionEx}

\QR{Une antenne de télécommunications de 20 mètres de haut est située à 100 mètres d'un pylône électrique de 40 mètres de haut. Afin de stabiliser ces deux structures, on souhaite attacher le sommet de chacune des structures en un point au sol situé entre elles à l'aide d'un solide fil de fer torsadé. Déterminer la fonction à optimiser, et son domaine, si l'on cherche l'endroit du point d'ancrage qui minimise la quantité de fil à utiliser.}{$f(x) = \sqrt{20^2+x^2}+\sqrt{40^2+(100-x)^2}$ \newline $\dom(f) = [0,\, 100]$}

\QR{Déterminer la quantité minimale de carton nécessaire pour fabriquer une boîte rectangulaire à base carrée ouverte sur le dessus et dont le volume est de 500~cm\up{3}. Déterminer quelles seront alors les dimensions de la boîte. \textit{Problème tiré de Calcul différentiel, de Luc Amyotte}}{$x$ : mesure d'un côté de la base \newline $A$: aire de la boîte \newline $A(x) = x^2 + \frac{2000}{x}$ \newline $\dom(A) = ]0,\, \infty[$ \newline On obtient une quantité minimale de 300~cm\up{2} de carton pour fabriquer une boîte de 500~cm\up{3} dont les dimensions sont de \newline 10~cm $\times$ 10~cm $\times$ 5~cm.  }

\QR{Déterminer le ou les points de la parabole $y=2x^2$ les plus proches du point $(0,\,10)$. \textit{Problème tiré de Calcul différentiel, de Luc Amyotte}}{$d$ : distance entre un point sur la parabole et le point $(0,\,10)$ \newline $d(x) = \sqrt{x^2+(10-2x^2)^2}$ \newline $\dom(d) = ]-\infty,\, \infty[$ \newline On obtient une distance minimale de $\frac{\sqrt{79}}{4}\approx 2,22$ aux points $\left(-\sqrt{\frac{39}{8}},\,\frac{39}{4}\right)$ et $\left(\sqrt{\frac{39}{8}},\,\frac{39}{4}\right)$.}

\QR{Soit une feuille de papier dont l'aire est de 2~m\up{2}. Déterminer ses dimensions pour que la surface d'impression soit maximale, sachant que la feuille comporte des marges non imprimées de 8 cm de chaque côté et de 10 cm en haut et en bas. \textit{Problème tiré de Calcul différentiel, de Luc Amyotte}  \imaCalcu}{$x$ : mesure de la largeur de la feuille \newline $A$ : surface d'impression \newline $A(x) = (x-16)\left(\frac{20000}{x}-20\right)$ \newline $\dom(A) = [16,\, 1000]$ \newline Les dimensions sont $40\sqrt{10}$ cm par $\frac{500}{\sqrt{10}}$~cm.}

\QR{Si on exclut le salaire du camionneur, les frais d'exploitation d'un camion sont de $\left(0,6 + \frac{v}{500} \right)$~\$/km, où $v$ est la vitesse du camion (en kilomètres par heure). Le salaire du camionneur est de 16 \$/h. Déterminer la vitesse à laquelle le camionneur doit conduire pour qu'un trajet de 250~km soit le plus économique possible. \textit{Problème tiré de Calcul différentiel, de Luc Amyotte}}{$C$ : coût d'un trajet de 250~km \newline $C(x) = 250\left(0,6+\frac{v}{500}\right)+16\cdot \frac{250}{v}$ \newline $\dom(C) = ]0,\, \infty[$ \newline Le trajet est le plus économique (environ 239,44~\$) lorsque le camionneur conduit à une vitesse de $40\sqrt{5}\approx 89,4$~km/h. \man{\newpage}}
\man{\columnbreak}
\QR{Une clôture de 3~m de haut est située à 1~m d'un immeuble. Déterminer la longueur $L$ de la plus courte échelle qui touche le sol d'un côté de la clôture, prend appui sur le sommet de la clôture et touche l'immeuble de l'autre côté. \textit{Problème tiré de Calcul différentiel, de Luc Amyotte}}{$L$ : longueur de l'échelle \newline $x$ : distance entre le pied de l'échelle au sol et le point de contact avec le haut de la clôture \newline $L(x) = x + \frac{x}{\sqrt{x^2-9}}$ \newline $\dom(L) = ]3,\, \infty[$ \newline La longueur de l'échelle la plus courte est d'environ 5,41 m lorsque le pied de l'échelle est en contact avec le sol à $\sqrt[3]{9}\approx 2,1$~m de la clôture, soit à environ 3,1~m du mur de l'immeuble.}

\end{sectionEx}





\section{Taux liés}

\begin{manuel}
Tout comme à la section \ref{secTauxLies}, nous cherchons maintenant à déterminer un taux de variation instantané à partir d'une équation qui lie des variables dépendantes d'une même variable indépendante.

\begin{ex}
\QS{Supposons que $x$ et $y$ sont des fonctions de $t$ liées par l'équation $2y=4-\sqrt{x+y^2}$.

Sachant que $\displaystyle \frac{\dd x}{\dd t}=1$, évaluer $\displaystyle \frac{\dd y}{\dd t}$ lorsque $y=0$.
}{

Il faut d'abord dériver les deux côtés de l'équation par rapport à la variable $t$.
\begin{align*}
&2\frac{\dd y}{\dd t}= \ \frac{-1}{2\sqrt{x+y^2}}\frac{\dd }{\dd t}\left(x+y^2\right)\\[0.4cm]
&2\frac{\dd y}{\dd t}= \ \frac{-1}{2\sqrt{x+y^2}}\left(\frac{\dd x}{\dd t}+2y\frac{\dd y}{\dd t}\right)
\end{align*}
Ensuite, lorsque $y=0$, on a $0=4-\sqrt{x}\Rightarrow x=16$. Donc, en remplaçant $y$ par 0, $x$ par 16 et $\displaystyle \frac{\dd x}{\dd t}$ par 1, on obtient
\begin{align*}
2\left. \frac{\dd y}{\dd t}\right|_{y=0} =& \ \frac{-1}{2\cdot 4}\left(1+0\right) \\[0.4cm]
\Rightarrow \left. \frac{\dd y}{\dd t}\right|_{y=0}=& \  -\frac{1}{16} \text{ .}
\end{align*}
}

\end{ex}
\end{manuel}

\begin{bmframe}{Démarche}
La \tip{démarche}{tabMethodeTauxLies} pour la résolution de problèmes de taux liés en contexte est la même que celle vue dans la section \ref{secTauxLies}.
\end{bmframe}

\begin{manuel}
\ordi{\newpage}

\begin{ex}
\QS{Le rayon $r$ du tronc de la souche d'un certain type d'arbre est fonction de la hauteur $h$ de celui-ci, et la relation entre le rayon et la hauteur de l'arbre est donnée par $r=0,002h^{3/2}$, où $r$ et $h$ sont mesurés en mètres. De plus, la taille d'un de ces arbres âgé de $t$ années est donnée par $h(t)=\displaystyle\frac{10t^2}{100+t^2}$. À quel rythme le diamètre du tronc de la souche d'un de ces arbres âgé de 5 ans croît-il?\textit{Tiré de Calcul différentiel 2$^e$ édition par Hamel et Amyotte.} \imaCalcu{}
}{

\textbf{Étape 1:  Lire attentivement le problème. Relire au besoin.} \\


\textbf{Étape 2:  Nommer les variables et déterminer le ou les taux connus ainsi que le taux cherché.}

$d$: le diamètre du tronc de l'arbre (m) \\
$h$: la hauteur de l'arbre (m)\\
$t$: temps écoulé (années)

On cherche $\left. \displaystyle \frac{\dd d}{\dd t}\right|_{t=5}$. 

Ici, $\left. \displaystyle \frac{\dd h}{\dd t}\right|_{t=5}$ n'est pas donné directement, mais on peut le trouver à partir de l'équation de $h(t)$.

$\displaystyle \frac{\dd h}{\dd t}=\frac{20t(100+t^2)-2t(10t^2)}{(100+t^2)^2}$ ~~~(\tip{théorème \ref{thmDeriveeQuotient}}{thmDeriveeQuotient})

Et donc $\left. \displaystyle \frac{\dd h}{\dd t}\right|_{t=5}=\displaystyle \frac{100\cdot 125-10\cdot 250}{125^2}=\displaystyle \frac{10 000}{15625}=\frac{16}{25}$. \\


\textbf{Étape 3:  Tracer un schéma (pour les problèmes de nature géométrique).} 
\begin{center}
\tikzexternalenable
\tikzsetnextfilename{p2ApplicationsEx2Man}
\begin{tikzpicture}[scale=0.5, every node/.style={scale=0.6}]
\draw (-2,6) -- (-2,0) arc (180:360:2cm and 0.5cm) -- (2,6) ++ (-2,0) circle (2cm and 0.5cm);
\draw[densely dashed] (-2,0) arc (180:0:2cm and 0.5cm);
\draw (0,6) -- (2,6);
\draw (1,5.9) node[above]{$r$};
\draw (2,3) node[right]{$h$};
\end{tikzpicture}
\tikzexternaldisable
\end{center}

\newpage

\textbf{Étape 4:  Trouver le(s) lien(s) entre les variables.} 

On a $d=2r=0,004h^{3/2}$.   \\

\textbf{Étape 5:  Utiliser le calcul différentiel pour obtenir les taux de variation décrits à l'étape 2.}  

On dérive de chaque côté par rapport à la variable $t$.
\begin{align*}
 & \frac{\dd d}{\dd t} = \ 0,004\cdot\frac{3}{2}h^{1/2}\cdot\frac{\dd h}{\dd t} \\[0.4cm]
\Rightarrow& \frac{\dd d}{\dd t}  = \ 0,006~h^{1/2}\cdot\frac{\dd h}{\dd t} 
\end{align*}
On veut évaluer cette dérivée en $t=5$. Or, $h(5)=\displaystyle \frac{250}{125}=2$.
\begin{align*}
\left. \frac{\dd d}{\dd t}\right|_{t=5} =& \ \ 0,006\cdot 2^{1/2}\cdot\frac{16}{25} \\[0.4cm]
=& \ 0,0054 \text{~m/année} 
\end{align*}
\textbf{Étape 6:}  Conclure et interpréter le résultat en tenant compte du contexte.  

Lorsque l'arbre est âgé de 5 ans, son diamètre augmente de 0,0086 mètres par année, ou d'environ 0,86 centimètres par année.
}
\end{ex}
\end{manuel}



\begin{bnframe}{Exercice \exe}
\QR{Pour simplifier l'étude d'un circuit électrique en parallèle, on peut modéliser l'ensemble des résistances à l'aide d'une seule résistance équivalente à la résistance de l'ensemble du circuit. Pour un circuit parallèle à trois résistances $R_1$, $R_2$ et $R_3$, la résistance $R_e$ équivalente est donnée par 
$$\frac{1}{R_e}=\frac{1}{R_1}+\frac{1}{R_2}+\frac{1}{R_3} \text{ .}$$
Si la résistance $R_1$ augmente de 2 $\Omega /\text{min}$, la résistance $R_2$ augmente de 3 $\Omega /\text{min}$ et la résistance $R_3$ diminue de 1 $\Omega /\text{min}$, à quel rythme varie la résistance $R_e$ lorsque $R_1=15$, $R_2=30$ et $R_3=30$?}{$R_e$ varie au rythme de $\dfrac{5}{8} \Omega /\text{min}$.}
\end{bnframe}




\devoirs



\begin{sectionEx}
\QR{Soit $\sqrt{x^3+8}=y^2$. Déterminer $\dfrac{\textrm{d}x}{\textrm{d}t}$ quand $(x,\,y)=(2,\,2)$ sachant qu'à ce moment $\dfrac{\textrm{d}y}{\textrm{d}t}=12$.}{On obtient $\left. \dfrac{\textrm{d}x}{\textrm{d}t} \right|_{(x,\,y)=(2,\,2)}=32$.}

\QR{On calcule l'indice de masse corporelle (IMC) d'un individu  en divisant la masse ($M$) de celui-ci mesurée en kilogrammes par le carré de sa taille ($T$) mesurée en mètres. Si une personne adulte mesurant 1,7~m et pesant 85~kg perd 500 g/semaine grâce à un régime alimentaire, déterminer le rythme auquel l'IMC de cette personne change. \textit{Problème tiré de Calcul différentiel, de Luc Amyotte}}{$IMC = \frac{M}{T^2}$ \newline L'IMC diminue à raison de $\dfrac{1}{2(1,7)^2}$~kg/m\up{2} par semaine.}

\QR{Une lumière installée au sol éclaire le mur vertical d'un édifice situé à 30~m de la source lumineuse. Une femme mesurant 1,6~m se déplace de la source lumineuse vers le mur à une vitesse constante de 3~m/s, de sorte que son ombre est projetée sur le mur. Déterminer le rythme auquel la longueur de l'ombre projetée sur le mur change après 5 secondes. \textit{Problème tiré de Calcul différentiel, de Luc Amyotte}}{$x$ : distance entre la source lumineuse et la femme \newline $y$ : longueur de l'ombre \newline $\frac{x}{1,6} = \frac{30}{y}$ \newline Après 5 secondes, la longueur de l'ombre projetée sur le mur diminue à raison de $\frac{16}{25}$~m/s. }

\QR{Un réservoir contient 15 litres d'alcool. On y verse de l'eau à raison de $2,5$~L/min. Déterminer le rythme auquel la concentration d'alcool change lorsqu'elle est de $0,6$, c'est-à-dire lorsque le mélange contient 60~$\%$ d'alcool. \textit{Problème tiré de Calcul différentiel, de Luc Amyotte} \hfill}}{$C$ : concentration d'alcool \newline $V$ : volume total \newline $C = \frac{15}{V}$ \newline Lorsque la concentration en alcool du mélange est de 60~$\%$, la concentration diminue à raison de $\frac{3}{50}$~/min, soit de 6 points de pourcentage par minute. }
\end{sectionEx}



\section{Approximation linéaire}


\begin{manuel}
Les nombres rationnels et les racines de nombres qui ne sont pas une puissance de nombre entier sont difficiles à évaluer sans outil. On peut toutefois les approximer en utilisant la droite tangente ou la \tip{différentielle}{defDiff}, comme nous l'avons fait à la section \ref{secApproxLin}.

\begin{ex}
\QS{Approximer la valeur de $\sqrt{37}$.
}{

Si nous utilisons la \g{droite tangente} pour faire l'approximation : 

Soit la fonction $f(x)=\sqrt{x}$. Nous savons que $\sqrt{36}=6$. Nous pouvons donc utiliser la tangente à la courbe en $x=36$ pour approximer la valeur de $f(37)$.

Nous trouvons la dérivée pour obtenir la pente de la tangente. Nous trouvons $f'(x) = \dfrac{1}{2\sqrt{x}}$. Donc, la pente de la tangente en $x=36$ est $f'(36) = \dfrac{1}{2\sqrt{36}} = \dfrac{1}{12}$. 

À l'aide du point de tangence $(36,\, 6)$, nous pouvons trouver l'ordonnée à l'origine de la droite tangente en $x=36$. 

$6 = \dfrac{1}{12}\cdot 36 + b \Rightarrow b=3$. 

Nous avons donc $g(x) = \dfrac{x}{12} + 3$ comme équation de droite tangente. Ainsi, nous pouvons utiliser cette équation pour faire l'approximation.%
$$f(37) \approx g(37) = \frac{37}{12}+3 = \frac{73}{12}.$$
Si nous utilisons la \g{différentielle} pour faire l'approximation : 

Soit la fonction $y=f(x) = \sqrt{x}$. Nous savons que $\sqrt{36}=6$ alors nous utilisons $x=36$ et $\dd x=1$.

Nous trouvons la dérivée pour obtenir la différentielle de $y$. Nous trouvons $f'(x) = \dfrac{1}{2\sqrt{x}}$. Donc, $\dd y = \dfrac{1}{2\sqrt{x}} \, \dd x$. 

Nous pouvons maintenant faire l'approximation.% 
$$f(37)=f(36+1) \approx f(36) + \dd y = f(36)+f'(36)\cdot 1=6+\displaystyle \frac{1}{12}=\frac{73}{12}.$$
}
\end{ex}
\ordi{\newpage}
\end{manuel}



\begin{bframe}{Approximation linéaire}
Pour faire une approximation linéaire, nous pouvons utiliser : 

\begin{liste}[beaOpt=<+- |alert@+>, itSep=0.3cm]
\item Une droite tangente. 
\item La \tip{différentielle}{defDiff}.
\end{liste}
\end{bframe}





\begin{bnframe}{Exercice \exe}
\QR{Approximer la valeur de $\dfrac{2}{1,1} + \dfrac{3}{1,1^2}$ à l'aide du calcul différentiel.}{$\dfrac{2}{1,1} + \dfrac{3}{1,1^2} \approx 4,2$}
\end{bnframe}



\devoirs
\ndc{\newpage}



\begin{sectionEx}
\Q{Approximer linéairement, à l'aide d'une droite tangente, la valeur de chacune des expressions suivantes.}
\debutSousQ
\QR{$(8,03)^{\frac{2}{3}}$}{$4,01$}
\QR{$\dfrac{(2,99)^2-4}{(2,99)^2+1}$}{$0,497$}
\finSousQ

\Q{Utiliser la différentielle pour approximer la valeur de chacune des expressions suivantes.}
\debutSousQ
\QR{$(8,03)^{\frac{2}{3}}$}{$4,01$}
\QR{$\dfrac{(2,99)^2-4}{(2,99)^2+1}$}{$0,497$}
\finSousQ
\end{sectionEx}

